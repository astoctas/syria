<?php

class Cart_custom extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
		if(!isset($_SESSION['cart'])) $_SESSION['cart'] = array(); 
    }
	
	public function add() {
		$uid = $this->input->get('uid');
		$success = false;
		if(!$uid) {
			echo json_encode((object)array(
				'success' => $success,
				'cart' => array_keys($_SESSION['cart']), 
				'items' => count($_SESSION['cart'])
			));
			return;
		}
		if(!in_array($uid, array_keys($_SESSION['cart']))) {
			$_SESSION['cart'][$uid] = array('q'=>1);
			$success = true;
		}
		echo json_encode((object)array(
			'success' => $success,
			'cart' => array_keys($_SESSION['cart']), 
			'items' => count($_SESSION['cart'])
		));
	}

	public function get() {
		echo json_encode((object)array(
			'success' => true,
			'cart' => array_keys($_SESSION['cart']), 
			'items' => count($_SESSION['cart'])
		));
	}
	
	public function update() {
		$uid = $this->input->get('uid');
		$q = $this->input->get('q');
		if(isset($_SESSION['cart'][$uid]))
			$_SESSION['cart'][$uid]['q'] = $q; 
		echo json_encode((object)array(
			'success' => true,
			'cart' => array_keys($_SESSION['cart']), 
			'items' => count($_SESSION['cart'])
		));
	}
	
	public function delete() {
		$uid = $this->input->get('uid');
		if(isset($_SESSION['cart'][$uid]))
			unset($_SESSION['cart'][$uid]); 
		echo json_encode((object)array(
			'success' => true,
			'cart' => array_keys($_SESSION['cart']), 
			'items' => count($_SESSION['cart'])
		));
	}
	
	public function send() {
		/* TODO
		/* ENVIAR MAIL
		*/
		$prods = $this->products();
		$this->smarty->assign('URL', $this->config->item('base_url'));
		$this->smarty->assign('_contents', $prods);
		$this->smarty->assign('_post', $_POST);
		$body = utf8_decode($this->smarty->fetch('cart-mail.tpl'));
		//pr($body); exit; 
		
		$this->load->library('email');
		$this->email->to(Auth::getConfig('mailing_from'));
		$this->email->reply_to(Auth::getConfig('mailing_reply_to'));
		$this->email->subject('Cotización web');
		$this->email->message($body);
		$ok = $this->email->send();
		//echo $this->email->print_debugger();
		//$this->smarty->assign('_sendok', $ok);
		
		if($ok) $_SESSION['cart'] = array();

		echo json_encode((object)array(
			'success' => true
		));
	}

	public function products() {
		if(count($_SESSION['cart'])) {
			$this->db->select('*, uid as content_uid');
			$this->db->where_in('uid',  array_keys($_SESSION['cart'])); 
			$contents = $this->db->get('contents')->result_array();
			$this->load->model('frontend/datastore');
			$res = $this->datastore->getData($contents);
			foreach($res as &$c){
				$c['_data']['cantidad'] = $_SESSION['cart'][$c['_data']['uid']]['q'];
			}
			$this->smarty->assign('_sendok', false);
			/*
			if(isset($_POST['cart'])){
				$this->smarty->assign('_contents', $res);
				$this->smarty->assign('_post', $_POST);
				$body = utf8_decode($this->smarty->fetch('cart-mail.tpl'));
				//pr($body); exit; 
				$this->load->library('email');
				$this->email->to(Auth::getConfig('mailing_from'));
				$this->email->reply_to($_POST['email']);
				$this->email->subject('Cotización web');
				$this->email->message($body);
				$ok = $this->email->send();
				$this->smarty->assign('_sendok', $ok);
				if($ok) $_SESSION['cart'] = array();
			}
			 */
			return $res;
		} else
			return array();
	}

}