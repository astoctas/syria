<?php

class Contents_custom extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
	}
	
	public function searchProducts($sec, $data) {
		$xpag = ($this->input->get('xpag')) ?: $sec['paging'];
		$pag = ($this->input->get('pag')) ? (int)$this->input->get('pag') - 1: 0;
		$limitFrom = (int)$pag * (int)$xpag;
		$limitTo = (int)$xpag;
		$text = $this->input->get('q');
		// separar en palabras y agregar * a cada una
		$text = implode("* ", preg_split("/[\s]+/", $text))."*";
		$values = array($text, $text);

		$query = "SELECT SQL_CALC_FOUND_ROWS c.*,  c.uid as content_uid
		FROM `contents` c
		INNER JOIN objects o ON c.object = o.uid AND o.name = 'Productos'
		LEFT JOIN data_uid du ON du.uid = c.uid
		LEFT JOIN `fields` f ON f.schema_uid = o.`schema` AND f.name = 'Marca'
		LEFT JOIN `contents` marca ON marca.uid = du.data AND marca.active = 1 AND marca.deleted = 0
		LEFT JOIN objects o2 ON marca.object = o2.uid AND o2.name = 'Marcas'
		WHERE c.active = 1 AND c.deleted = 0
		AND (MATCH(c.name) AGAINST(? IN BOOLEAN MODE) OR MATCH(marca.name) AGAINST (? IN BOOLEAN MODE)) 
		GROUP BY c.uid ";
		$query .= " LIMIT $limitFrom,$limitTo ";

		$res = $this->db->query($query, $values);
		if(!$res) return $data;
		$res = $res->result_array();
		$data['data'] = array();
		$data['paginfo'] = array();
		$this->load->model('frontend/sections');
		$params = array(
			'xpag' => $xpag,
			'pag' => $pag
		);
		$this->sections->getPaging($params, $pagInfo);
		if($pagInfo && $pag  > $pagInfo['pages']) {
			show_404();
			exit;
		}		
		$data['paginfo'] = $pagInfo;


		$this->load->model('frontend/datastore');
		$res = $this->datastore->getData($res);
		
		//print_r($res);
		$data['data'] = $res;
		return $data;
	}
	
	public function search($sec, $data) {
		//pr($sec, $data);
		// TRAER CORTES
		$data['corte'] = $this->db->get_where('categories', array('parent'=>'686d42d2-8ea8-4b93-ac72-8af583b2f93a'))->result_array();
		// TRAER TRANSITOS
		$data['transito'] = $this->db->get_where('categories', array('parent'=>'216d6c12-6e16-4148-8e25-5333f6ec312d'))->result_array();
		// TRAER MARCAS
		$query = "SELECT *
			FROM contents_active ca
			WHERE uid IN (
				SELECT DATA
				FROM data_uid d
				WHERE d.field = '5d310e4b-95c7-11e3-85ee-000ea6e13d9f' # MARCA
				AND d.uid IN (
					SELECT c.uid
					FROM contents_active c
					INNER JOIN data_uid u ON u.uid = c.uid AND FIELD = '15cc6243-95cc-11e3-85ee-000ea6e13d9f' # CATEGORIAS
					AND DATA = '{$sec['auto']['category']['uid']}' #CATEGORIA
					WHERE c.object = '1b7053dc-95bf-11e3-85ee-000ea6e13d9f' # PRODUCTOS
				)
			)";
		$data['marcas'] = $this->db->query($query)->result_array();
		$xpag = ($this->input->get('xpag')) ?: $sec['paging'];
		$pag = ($this->input->get('pag')) ? (int)$this->input->get('pag') - 1: 0;
		$limitFrom = (int)$pag * (int)$xpag;
		$limitTo = (int)$xpag;
		$values = array();
		$query = "SELECT SQL_CALC_FOUND_ROWS c.*, n.data, c.uid as content_uid FROM (
				SELECT d.uid FROM data_all d
				INNER JOIN data_all d0 ON d0.uid = d.uid AND d0.field = '15cc6243-95cc-11e3-85ee-000ea6e13d9f'  "; // TIPO
				if($this->input->get('corte')) $query .= " LEFT JOIN data_all d1 ON d1.uid = d.uid AND d1.field = '0fe603c7-95bf-11e3-85ee-000ea6e13d9f' "; // CORTE
				if($this->input->get('transito')) $query .= " LEFT JOIN data_all d2 ON d2.uid = d.uid AND d2.field = '0fe67040-95bf-11e3-85ee-000ea6e13d9f' "; //TRANSITO 
				if($this->input->get('marca')) $query .= " LEFT JOIN data_all d3 ON d3.uid = d.uid AND d3.field = '5d310e4b-95c7-11e3-85ee-000ea6e13d9f' "; //MARCA 
				$query .= " WHERE	d0.data = '{$sec['auto']['category']['uid']}' ";
				if($this->input->get('corte')) {
					$query .=" and d1.data = ? ";
					$values[] = $this->input->get('corte');
				}
				if($this->input->get('transito')) {
					$query .= "and d2.data = ? ";
					$values[] = $this->input->get('transito');
				}	
				if($this->input->get('marca')) {
					$query .= "and d3.data = ? ";
					$values[] = $this->input->get('marca');
				}				
				$query .= "GROUP BY d.uid
				) as d
				INNER JOIN contents_active c ON c.uid = d.uid
				LEFT JOIN data_numeric n ON n.uid = d.uid AND n.`field` = 'fe904022-95c4-11e3-85ee-000ea6e13d9f'
				ORDER BY n.data ";
				if($this->input->get('orden')) $query .= $this->input->get('orden');
				$query .= " LIMIT $limitFrom,$limitTo ";
		$res = $this->db->query($query, $values);
		$data['data'] = array();
		$data['paginfo'] = array();
		if(!$res) return $data;
		$res = $res->result_array();
		$this->load->model('frontend/sections');
		$params = array(
			'xpag' => $xpag,
			'pag' => $pag
		);
		$this->sections->getPaging($params, $pagInfo);
		if($pagInfo && $pag  > $pagInfo['pages']) {
			show_404();
			exit;
		}		
		$data['paginfo'] = $pagInfo;
		$this->load->model('frontend/datastore');
		$res = $this->datastore->getData($res);
		$data['data'] = $res;
		return $data;
		
	}

}