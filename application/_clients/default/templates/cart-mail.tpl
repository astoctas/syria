 <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
    <head>
    </head>
    <body style="font-family: Arial; font-size: 15px;">
        <h2 class="th">Cotizaci&oacute;n web</h2>
        <div class="content">
            <strong>Nombre: </strong>{$_post.nombre}
            <br/>
            <strong>Email: </strong>{$_post.email}
            <br/>
            <strong>Teléfono: </strong>{$_post.telefono}
            <br/>
            <strong>Comentarios: </strong>{$_post.comentarios}
            <br>
            <h2 class="th">Productos a cotizar</h2>
            <div class="listing" style="margin-top: 20px;">
               {foreach $_contents as $item}

                <div class="item left" style="margin-left: 0px !important; width: 475px; height: 220px; margin-bottom: 15px; border: 1px solid #f68121;">
 					{if isset($item.Imagenes)}
                    <a class="" href="{$URL}/productos/{$item._data.slug}"><img class="photo pull-left" src="{$item.Imagenes.0.src.Thumbnail}" style="width: 220px; height: 220px; float: left;" align="left"></a>
                    {else}
					<a class="" href="{$URL}/productos/{$item._data.slug}"><img class="photo pull-left" src="{$ASSETS}/css/images/no-image.jpg" style="width: 220px; height: 220px; float: left;" align="left"></a>
					{/if}
                    <div class="info pull-left" style="color: #777; float: left; padding: 5px 10px;">
                        <div class="title" style="color: #F58220; font-size: 16px;">
                            {$item._data.name}
                        </div>
                        <strong style="color: #333;">Tipo: </strong>{$item.Tipo.0.name|default:"N/D"}
                        <br>
                        <strong style="color: #333;">Marca: </strong>{$item.Marca.0.name|default:"N/D"}
                        <br>
                        <strong style="color: #333;">Tr&aacute;nsito: </strong>{$item.Transito.0.name|default:"N/D"}
                        <br>
                        <strong style="color: #333;">Corte: </strong>{$item.Corte.0.name|default:"N/D"}
                        <br>
                        <strong style="color: #333;">Medida: </strong>{$item.Medida|default:"N/D"}
                        <br>
                        <strong style="color: #333;">Precio 1ra calidad: </strong>
                        <span class="price" style="color: #F58220;">$ {$item.Precio_primera|default:"N/D"} /{if isset($item.Precio_por_Unidad)&& $item.Precio_por_Unidad eq 1}unidad{else}m<sup>2</sup>{/if}
                        </span>
                        <br>
                        <strong style="color: #333;">Precio 2da calidad: </strong>
                        <span class="price" style="color: #F58220;">$ {$item.Precio_segunda|default:"N/D"} /{if isset($item.Precio_por_Unidad)&& $item.Precio_por_Unidad eq 1}unidad{else}m<sup>2</sup>{/if}
                        </span>
                        <br>
                        <strong style="color: #333;">Cantidad: </strong>{$item._data.cantidad}
                        <br>
                    </div>
                </div>
				{/foreach}
            </div>
            <div class="clearfix">
            </div>
        </div>
    </body>
</html>
