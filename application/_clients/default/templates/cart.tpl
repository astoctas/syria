	<div class="th">{$page.Titulo|default}</div>
	{if isset($_sendok) && $_sendok}
	<div class="alert alert-warning " id="msg-sent"><h3>El carrito ha sido enviado para su cotización.  Le responderemos a la brevedad.</h3></div>

	{literal}
<!-- Google Code for syria-contacto Conversion Page
In your html page, add the snippet and call
goog_report_conversion when someone clicks on the
chosen link or button. -->
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 970864604;
    w.google_conversion_label = "QpoUCOLlgWoQ3O_4zgM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script>
	{/literal}

	{else}
		<div class="text">
			{$page.Texto|default}
		</div>
		<div id="cart-content">
		<div class="listing row">

					{foreach $contents as $item}
					<div class= " col-xs-12 col-md-6 " >

					<div class="item {if $item@iteration % 2}left{/if}">
						{if isset($item.Imagenes)}
						<a class="" href="{$URL}/productos/{$item._data.slug}"><img class="photo {if isset($item.Marca_de_Agua) && $item.Marca_de_Agua eq 1} watermark{/if}" src="{$item.Imagenes.0.src.Thumbnail}" /></a>
						{else}
						<a class="" href="{$URL}/productos/{$item._data.slug}"><img class="photo" src="{$ASSETS}/css/images/no-image.jpg" /></a>
						{/if}
						<div class="info">
							<div class="title">{$item._data.name}</div>
							<strong>Tipo: </strong>{$item.Tipo.0.name|default:"N/D"}<br/>
							<strong>Marca: </strong>{$item.Marca.0.name|default:"N/D"}<br/>
							<strong>Tránsito: </strong>{$item.Transito.0.name|default:"N/D"}<br/>
							<strong>Corte: </strong>{$item.Corte.0.name|default:"N/D"}<br/>
							<strong>Medida: </strong>{$item.Medida|default:"N/D"}<br/>
							<strong>Precio 1ra calidad: </strong><span class="price">$ {$item.Precio_primera|default:"N/D"} m<sup>2</sup></span><br/>
							<strong>Precio 2da calidad: </strong><span class="price">$ {$item.Precio_segunda|default:"N/D"} m<sup>2</sup></span><br/>
							<div class="btns">
							<div class="cantidad pull-left"><label>Cantidad</label>&nbsp;<input type="text" data-uid="{$item._data.uid}" class="cantidad" value="{$item._data.cantidad}" />&nbsp;</div>
							<a class="btn btn-success pull-left btn-narrow btn-cart-update" data-uid="{$item._data.uid}"><span class="fa fa-check"></span></a>
							<a class="btn btn-danger btn-cart-delete pull-left btn-narrow" data-uid="{$item._data.uid}" data-toggle="modal" data-target="#modal-{$item._data.uid}"><span class="fa fa-trash"></span></a>
							</div>
						</div>
					</div>
					</div>
						<div id="modal-{$item._data.uid}" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-lg">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">Quitar producto</h4>
						      </div>
							   <div class="modal-body">
						      ¿Desea quitar el producto <strong>{$item._data.name}</strong> del carrito?
								</div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						        <button type="button" class="btn btn-orange btn-cart-confirm" data-uid="{$item._data.uid}">Quitar</button>
						      </div>
						    </div>
						  </div>
						</div>
					{foreachelse}
					 <div class="alert alert-warning"><h3>No hay productos en el carrito.</h3></div>
					{/foreach}
			</div>
	        <div class="clearfix"></div>
			{if $contents}
			<a class="btn btn-orange btn-lg"  data-toggle="modal" data-target="#modal-ok"><span class="fa fa-shopping-cart"></span> Enviar</a>

						<div id="modal-ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-lg">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">Enviar carrito</h4>
						      </div>
							   <div class="modal-body">

				              		<div class="alert alert-danger hidden-xs-up" id="alert-failure">
				              			<h4>Error al enviar la cotización.</h4>  Por favor, intente nuevamente.
				              		</div>
				              		<div class="alert alert-success hidden-xs-up" id="alert-success">
				              			<h4>La cotización ha sido enviada.</h4>  Nos pondremos en contacto a la brevedad.
				              		</div>


							   	<div id="form-content">
						      ¿Desea enviar todos los productos del carrito para cotizar?  Complete el formulario.<br/><br/>

								<form class="form-horizontal" role="form" method="post" id="cart-send-form">
									<input type="hidden" name="cart" value="1" />
								  <div class="form-group">
								    <label for="inputPassword3" class="col-sm-2 control-label">Nombre</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control " id="inputPassword3" placeholder="Nombre" name="nombre" required>
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
								    <div class="col-sm-10">
								      <input type="email" class="form-control " id="inputEmail3" placeholder="Email" name="email" required>
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputTel3" class="col-sm-2 control-label">Teléfono</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control " id="inputTel3" placeholder="Teléfono" name="telefono">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputText3" class="col-sm-2 control-label">Comentarios</label>
								    <div class="col-sm-10">
								      <textarea class="form-control" id="inputEmail3" placeholder="Comentarios" rows="10" name="comentarios"></textarea>
								    </div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <button   onclick="goog_report_conversion ('http://syriaceramicos.com.ar/cotizar')" type="submit" id="cart-submit-btn" class="btn btn-orange btn-cart-send">Enviar</button>
								      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								    </div>
								  </div>
								</form>
							   	</div>
								</div>
						    </div>
						  </div>
						</div>
			{/if}
		</div>
		{/if}
