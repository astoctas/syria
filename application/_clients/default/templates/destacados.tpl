				<div class="col-md-6 col-sm-12" data-aos="fade-up">
					<div class="colheader img-rounded">{$section.params->title|default}</div>
					{foreach $contents as $item}
					<div class="destacado" >
						{if isset($item.Imagen)}
						<a href="{$item.Link}"><img class=" img-rounded img-fluid watermark" src="{$item.Imagen.0.src.Producto}" /></a>
						{/if}
						<div class="caption img-rounded">
							<div class="title"><a href="{$item.Link}">{$item.Titulo|default}</a></div>
							<div class="desc">{$item.Descripcion|default}</div>
							<div class="price">{$item.Precio|default}</div>
						</div>
					</div>
					{/foreach}

				</div>