	<div class="col3">
			<ul class="top list-inline">
				{foreach $contents as $uid => $item}
					<li><a href="{URL}{$item.slug}">{$item._data.name}</a>
						<ul class="sub list-unstyled">
							{foreach $item._children as $child}
								<li><a href="{URL}{$item._data.slug}/{$child._data.slug}">{$child._data.name}</a>
									<ul class="sub list-unstyled">
										{foreach $child._children as $child2}
											<li><a href="{URL}{$item._data.slug}/{$child._data.slug}/{$child2._data.slug}">{$child2._data.name}</a></li>
										{/foreach}				
									</ul>
								</li>
							{/foreach}				
						</ul>
					</li>
				{/foreach}				
            </ul>
	</div>