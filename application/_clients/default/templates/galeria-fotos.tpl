    {foreach $contents as $item}
    {if isset($item.Imagen[0])}
        {assign value=$item.Imagen[0] var=img}
            <div style="display: none;" id="div{$item@iteration}">
                <div class="container">
                    <div class="row item-galeria">
                        <div class="col-xs-12 col-md-6 col-lg-8 imagen">
                            <img  class="img-fluid" src="{$img.src.Original}"  alt="{$item.Texto}"/>
                        </div>
                        <div class="col-xs-12 col-md-6 col-lg-4 texto">
                            <h2 class="title">{$item.Titulo}</h2>
                            <div>{$item.Texto}</div>
                        </div>
                    </div>
                </div>
            </div>
    {/if}
    {/foreach}
   <div class="th">{$page.Titulo|default}</div>

     <div class="">
        <div class="row">
            <div class="col-md-12">
                <div id="gallery" class="imgs">
                {foreach $contents as $item}
                {if isset($item.Imagen[0])}
                    {assign value=$item.Imagen[0] var=img}
                        <div class="item" style="background-color: white; color: black; padding-bottom: 1em; border: 1px solid #999999;">
                        <a data-fancybox   href="javascript:;" class="gallery {if isset($item.Marca_de_Agua) && $item.Marca_de_Agua eq 1} watermark{/if} " data-src="#div{$item@iteration}" >
                               <img class="photo {if $img@iteration %2}left{/if} pull-left {if isset($item.Marca_de_Agua)&& $item.Marca_de_Agua eq 1} watermark{/if}" src="{$img.src.Thumbnail}" alt="{$item.Texto}" />
                        <div style="color: black !important; position: relative; top: 0.5em !important; padding-left: 0.5em !important">{$item.Titulo}</div>
                        </a>
                        </div>
                {/if}
                {/foreach}
                </div>
            </div>
        </div>
        <p></p>
        {if $paginfo}
            {if $paginfo.pages gt 1}
                <div class="paging pull-right">
                    <nav aria-label="">
                    <ul class="pagination">
                    {if $paginfo.prev} <li class="page-item"><a  class="page-link" href="{QS key="pag" value=$paginfo.prev}">&laquo;</a></li> {/if}
                        {section pag $paginfo.pages}
                        <li class="{if $paginfo.pag eq $smarty.section.pag.iteration}active{/if} page-item"><a  class="page-link" href="{QS key="pag" value=$smarty.section.pag.iteration}">{$smarty.section.pag.iteration}</a></li>
                        {/section}
                    {if $paginfo.next} <li class="page-item"><a  class="page-link" href="{QS key="pag" value=$paginfo.next}">&raquo;</a></li> {/if}
                    </ul>
                    </nav>
                </div>
            {/if}
        {/if}
    </div>


{*templateJS  src="$ASSETS/js/templates/colorbox.js" *}

<script type="text/javascript">
    $(document).ready(function() {
        $('[data-fancybox]').fancybox(); 
    });
</script>