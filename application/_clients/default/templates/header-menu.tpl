<div class="row">
    <div class="logo col">
        <a href="{URL}">
        <img src="{$ASSETS}/css/images/Syria_Logo_Header2.png?{$VERSION}" alt="Logo" class="hvr-grow"/> 
        </a>
    </div>    
    <div class="col">
        <nav class="navbar navbar-orange navbar-toggleable-md navbar-light" >
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="true" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                    {foreach $contents as $uid => $item}
                        <li class="nav-item {act s=$item._data.slug} {if $item._children}dropdown{/if}">
                            <a class="nav-link" href="{if $item._children}#{else}{URL}{$item._data.slug}{/if}" {if $item._children} data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"{/if} id="{$item._data.slug}">{$item._data.name}</a>
                         {if $item._children}
                            <div class="dropdown-menu list-unstyled"  aria-labelledby="{$item._data.slug}">
                                {foreach $item._children as $child}
                                    <a  class="dropdown-item" href="{URL}{$item._data.slug}/{$child._data.slug}">{$child._data.name}</a>
                                {/foreach}
                            </div>
                            {/if}
                        </li>
                    {/foreach}
            </ul>

            <div class="sub-nav">
                <ul class="nav navbar-nav">
                </ul>
            </div>       

          </div>
        </nav>
    <div class="clearfix"></div>
    </div>
</div>
