{assign var=item value=$contents|array_shift}
<div class="search">
    <form action="{$URL}/resultados" method="get">
    <input type="text" class="search_input" name="q" placeholder="Buscar productos" {literal}pattern=".{3,}"{/literal}   required title="Introduce al menos 3 caracteres" />
    <input type="image" src="{$ASSETS}/css/images/search-button.png" />
    </form>
</div>
<div class="row">
    <div class="col-sm-12 pull-right">
        <!--div class="address text-right ">                {*$item.Texto*}                </div-->
        <div class="pos-market">
            <div class="market" id="cart-q">
                <a href="{$URL}/cotizar"><span id="text">COTIZAR</span> <span id="cart-items" class="number">0</span> </a>
            </div>
        </div>
    </div>
</div>