{assign var=item value=$contents|array_shift}
<div id="fixed-header" class="header_top_part bg_grey_light_2">
    <div class="container main-container t_align_c">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 fs_small color_light fw_light t_xs_align_c">
            	{$item.Texto}
            </div>
        </div>
    </div>
</div>
