	<div class="galeria"  data-aos="fade-up">
		<div class="galeria-fotos">

			    <div id="" class="carousel slide">

			      <div class="owl-carousel owl-theme owl-carousel-galeria">
				{foreach $contents as $item}
			        <div class="item ">
			          <a href="{$item.Link}" target="_blank"><img class=" img-rounded img-fluid watermark " src="{$item.Imagen.0.src.Galeria}" alt=""></a>
			            <div class="carousel-caption"> </div>
			        </div>
				{/foreach}

			      </div>

			 <!-- Controls -->
			  <a class="carousel-control-prev" href="#" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>

		    </div><!-- /.carousel -->


		</div>
	</div><!-- /.galeria -->
	<script type="text/javascript">
		$(document).ready(function(){
		  var owl = $(".owl-carousel-galeria").owlCarousel({
		  	items:1,
		  	dots: true,
		  	loop: true,
		  	mouseDrag: false
		  });
		  $('.carousel-control-prev').on('click', function(){
			owl.trigger('prev.owl.carousel');
		  })
		  $('.carousel-control-next').on('click', function(){
			owl.trigger('next.owl.carousel');
		  })
		});		
	</script>
