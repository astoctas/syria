{extends "./base.tpl"}

{block name="body"}
            {$REGION_6|default}

        <div class="container">

            <header>
                <div class="header row">
                    <div class="col">
                        {$REGION_1|default}
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </header>



            <div class="">
                        {$REGION_2|default}

                <div class="promos row">
                            {$REGION_3|default}
                            {$REGION_4|default}
                        <div class="clearfix"> </div>
                </div>

            </div>
            
            </div> <!-- container -->
            {$REGION_7|default}


            <div class="container-fluid">
            <footer>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="footer">
                            <div class="container main-container">
                                <div class="">
                                    <div class="logo"><img class="hvr-grow" src="{$ASSETS}/css/images/Syria_Logo_Header2.png?{$VERSION}" alt="Logo"/></div>
                                        {$REGION_5|default}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            </div>

{/block}

        {block name="js" append}
<script>
            var config = {
                "path": "{$ASSETS}/css/images/Syria_Watermark_Home2.png",
                "gravity": "sw",
                "opacity": 0
            };
            $(document).ready(function(){
                $('img.watermark').watermark(config);
            });
</script>

        {/block}