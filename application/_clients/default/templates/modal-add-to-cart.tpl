	<div id="modal-add-to-cart" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h4 class="modal-title" id="myModalLabel">Producto agregado</h4>
	      </div>	
		   <div class="modal-body">
			      El producto se agregó al carrito.  ¿Qué desea hacer?<br/><br/>
			</div>			
		    <div class="modal-footer">
		        <a type="button" class="btn btn-default" data-dismiss="modal">Cerrar</a>
		        <a href="{$URL}/cotizar" type="" class="btn btn-orange ">Ir al carrito</a>
		    </div>
	    </div>
	  </div>
	</div>