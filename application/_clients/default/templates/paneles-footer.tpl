<div class="container-fluid">
    <div class="paneles row">
        <div class="container">
        <div class="row">
        {foreach $contents as $uid => $item}
        <div class="{if $item@iteration == 1}col-lg-4 col-md-12 col-sm-12{else}col-lg-4 col-md-12 col-sm-12{/if} m_bottom_13 m_sm_bottom_30 ">
            <h5 class=" tt_uppercase second_font fw_light m_bottom_13">{$item.Titulo|default}</h5>
            <hr class="divider_bg m_bottom_15">
            {$item.Texto|default}
        </div>
        {/foreach}
        </div>
        </div>
    </div>
</div>
</section>