{assign var=item value=$content}


<script type="application/ld+json">
{
  "@context":"https://schema.org",
  "@type":"Product",
  "productID":"uid-{$item._data.uid}",
  "name":"{$item._data.name}",
  "description":"{$item.Mas_Info|default:$item._data.name}",
  "url":"{$URL}/productos/{$item._data.slug}",
	
	{if isset($item.Imagenes)}
	  "image":"{$item.Imagenes[0].src.Original}",
	{/if}
  "brand":"facebook",
  "offers": [
    {
      "@type": "Offer",
      "price": "{$item.Precio_primera|default:"N/D"}",
      "priceCurrency": "ARS",
      "availability": "in stock"
      
    }
  ]
}
</script>


	<div class="th no-border"><span class="bc1">Productos :: </span> <span class="bc2">{$item._data.name}</span></div>

	<div class="product">
		<div class="row">
			<div class="col-md-6">
				<div class="title orange">{$item._data.name}</div>
				<div class="info">
					<strong>Tipo: </strong>{$item.Tipo.0.name|default:"N/D"}<br/>
					<strong>Marca: </strong>{$item.Marca.0.name|default:"N/D"}<br/>
					<strong>Tránsito: </strong>{$item.Transito.0.name|default:"N/D"}<br/>
					<strong>Corte: </strong>{$item.Corte.0.name|default:"N/D"}<br/>
					<strong>Medida: </strong>{$item.Medida|default:"N/D"}<br/>
					<strong>Colores: </strong>{$item.Colores|default:"N/D"}<br/>
					<strong>Precio primera calidad: </strong><span class="price">$ {$item.Precio_primera|default:"N/D"} /{if isset($item.Precio_por_Unidad)&& $item.Precio_por_Unidad eq 1}unidad{else}m<sup>2</sup>{/if}<br/>
				{if $item.Precio_segunda gt 0}
					<strong>Precio segunda calidad: </strong><span class="price">$ {$item.Precio_segunda|default:"N/D"} /{if isset($item.Precio_por_Unidad)&& $item.Precio_por_Unidad eq 1}unidad{else}m<sup>2</sup>{/if}</span><br/>
				{/if}
					{if $item.Mas_Info}<strong>Más Info:</strong><br/>{$item.Mas_Info}<br/>{/if}
					<!--div class="pull-left"><strong>Colores: </strong></div><div class="color img-rounded" style="background-color: #FFFFFF">&nbsp;</div><div class="color img-rounded" style="background-color: #d3c2a6">&nbsp;</div-->
		            <div class="clearfix"></div>
{*
					{if $item.Mercado_Libre}
					<p><br/><a href="{$item.Mercado_Libre}" target="_blank">ver publicación en<img src="{$ASSETS}/css/images/logo_tiendanube.png" width="200" /></a></p>
					{/if}
*}
		            <div class="clearfix"></div><br/>
					<a class="btn btn-gray btn-cart" data-uid="{$item._data.uid}"><img src="{$ASSETS}/css/images/carrito-blanco.png" /> Cotizar</a>
				</div>
				</div>
			<div class="col-md-6">
				{if isset($item.Imagenes)}
				<div class="title">Galería de imágenes</div>
				<div class="imgs">
					{foreach $item.Imagenes as $img}
						<a class="gallery {if isset($item.Marca_de_Agua) && $item.Marca_de_Agua eq 1} watermark{/if} " href="{$img.src.Original}" title="{$img.caption}" ><img class="photo {if $img@iteration %2}left{/if} pull-left {if isset($item.Marca_de_Agua)&& $item.Marca_de_Agua eq 1} watermark{/if}" src="{$img.src.Thumbnail}" /></a>
					{/foreach}
				</div>
				{/if}
			</div>
		</div>
	</div>
	<script>
	document.addEventListener("DOMContentLoaded", function() {
		fbq('track', 'ViewContent');
	})
	</script>
{include file="./modal-add-to-cart.tpl"}

{templateJS  src="$ASSETS/js/templates/colorbox.js" }