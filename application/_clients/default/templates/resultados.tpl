	<section class="resultados">
				<div class="row">
					<div class="col-xs-12 col-md-7 col-lg-6 th no-border f_left" ><span class="bc2">Productos ::</span> <span class="bc1">"{$smarty.request.q}"</span> <span class="total" >({$contents.paginfo.records} encontrados)</span></div>
					<div class="col-xs-12 col-md-5 col-lg-6 f_right p_relative "><div class="leyenda b_10 r_0">* Todos nuestros productos cuentan con garantía de fábrica</div></div>
				</div>
				<div class="row">
				<div class="col-sm-12">
				
				</div>
				</div>
				<div class="listing row">

					{foreach $contents.data as $item}
					<div class= " col-xs-12 col-md-6 "  data-aos="fade-up" >
					<div class="item {if $item@iteration % 2}left{/if}">
						{if isset($item.Imagenes)}
						<a class="" href="{$URL}/productos/{$item._data.slug}"><img class="photo {if isset($item.Marca_de_Agua) && $item.Marca_de_Agua eq 1} watermark{/if}" src="{$item.Imagenes.0.src.Thumbnail}" /></a>
						{else}
						<a class="" href="{$URL}/productos/{$item._data.slug}"><img class="photo" src="{$ASSETS}/css/images/no-image.jpg" /></a>
						{/if}
						<div class="info ">
							<div class="title"><a class="" href="{$URL}/productos/{$item._data.slug}">{$item._data.name}</a></div>
							<strong>Tipo: </strong>{$item.Tipo.0.name|default:"N/D"}<br/>
							<strong>Marca: </strong>{$item.Marca.0.name|default:"N/D"}<br/>
							<strong>Tránsito: </strong>{$item.Transito.0.name|default:"N/D"}<br/>
							<strong>Corte: </strong>{$item.Corte.0.name|default:"N/D"}<br/>
							<strong>Medida: </strong>{$item.Medida|default:"N/D"}<br/>
							{if $item.Precio_primera gt 0}
							<strong>Precio 1ra calidad: </strong><br/><span class="price">$ {$item.Precio_primera|default:"N/D"} /{if isset($item.Precio_por_Unidad)&& $item.Precio_por_Unidad eq 1}unidad{else}m<sup>2</sup>{/if}</span><br/>
							{/if}
							{if $item.Precio_segunda gt 0}
							<strong>Precio 2da calidad: </strong><br/><span class="price">$ {$item.Precio_segunda|default:"N/D"} /{if isset($item.Precio_por_Unidad)&& $item.Precio_por_Unidad eq 1}unidad{else}m<sup>2</sup>{/if}</span><br/>
							{/if}
							<div class="botones">
								<a class="btn btn-gray btn-cart" data-uid="{$item._data.uid}"><span class="glyphicon glyphicon-shopping-cart"></span> Cotizar</a>
								<a class="btn btn-orange btn-info" href="{$URL}/productos/{$item._data.slug}">+ info</a>
							</div>
						</div>
					</div>
					</div>
					{foreachelse}
					 <div class="alert alert-warning"><h3>No se encuentran productos con estas palabras.</h3></div>
					{/foreach}

				</div>
	            <div class="clearfix"></div>

				{assign var=paginfo value=$contents.paginfo}
				{if $paginfo}
					{if $paginfo.pages gt 1}
						<div class="paging pull-right">
							<nav aria-label="">
							<ul class="pagination">
							{if $paginfo.prev} <li class="page-item"><a  class="page-link" href="{QS key="pag" value=$paginfo.prev}">&laquo;</a></li> {/if}
							 {section pag $paginfo.pages}
							  	<li class="{if $paginfo.pag eq $smarty.section.pag.iteration}active{/if} page-item"><a  class="page-link" href="{QS key="pag" value=$smarty.section.pag.iteration}">{$smarty.section.pag.iteration}</a></li>
							  {/section}
							{if $paginfo.next} <li class="page-item"><a  class="page-link" href="{QS key="pag" value=$paginfo.next}">&raquo;</a></li> {/if}
							</ul>
							</nav>
						</div>
					{/if}
				{/if}
</section>
{include file="./modal-add-to-cart.tpl"}
