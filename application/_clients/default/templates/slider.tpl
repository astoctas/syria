{if $contents}
    <div class="container">
    <div class="row">
            <!--featured products-->
            <div class="section-marcas p_bottom_0 m_bottom_5 col-sm-12"  data-aos="fade-up">
                    <div class="d_table m_bottom_5 w_full animated " data-animation="fadeInDown">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 v_align_m d_table_cell f_none">
                            <h3 class=" m_top_0 color_dark tt_uppercase fw_light d_inline_m m_bottom_4">Nuestras Marcas</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 t_align_r d_table_cell f_none">
                            <!--carousel navigation-->
                            <div class="clearfix d_inline_b buttons">
                                <button class="{$section.uid}_prev black_hover button_type_4  state_2 tr_all d_block f_left vc_child m_right_5"> <i class="fa fa-chevron-left  d_inline_m"></i></button>
                                <button class="{$section.uid}_next black_hover button_type_4  state_2 tr_all d_block f_left vc_child"><i class="fa fa-chevron-right  d_inline_m"></i></button>
                            </div>
                        </div>
                    </div>
                    <hr class="divider_bg m_bottom_15 animated hidden" data-animation="fadeInDown" data-animation-delay="100">
                    <div class="row">
                        <div class="owl-carousel owl-theme owl-carousel-marcas" data-nav="{$section.uid}_">

                        {foreach $contents as $item}

                                <a href="{$item.Link|default:'javascript:;'}" class="">
                                	{if isset($item.Logo[0])}
                                    {assign "img" $item.Logo[0]}
                                    <img class="marca" src="{$img.src.Original}" alt="" />
                                    {/if}
                                </a>

                        {/foreach}

                        </div>
                    </div>
            </div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){
          var owlMarcas = $(".owl-carousel-marcas").owlCarousel(
                            {
                                    "stagePadding" : 15,
                                    "margin" : 30,
                                    dots: false,
                                    loop: true,
                                    mouseDrag: false,
                                "responsive" : {
                                        "0" : {
                                            "items" : 6
                                        },
                                        "320" : {
                                            "items" : 2
                                        },
                                        "550" : {
                                            "items" : 2
                                        },
                                        "768" : {
                                            "items" : 4
                                        },
                                        "992" : {
                                            "items" : 6
                                        },
                                        "1200" : {
                                            "items" : 6
                                        }
                                    }
                                })
          $('.{$section.uid}_prev').on('click', function(){
            owlMarcas.trigger('prev.owl.carousel');
          })
          $('.{$section.uid}_next').on('click', function(){
            owlMarcas.trigger('next.owl.carousel');
          })
        });     
</script>
{/if}