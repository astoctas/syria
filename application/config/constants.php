<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*
|--------------------------------------------------------------------------
| APPLICATION DIRS
|--------------------------------------------------------------------------
|
*/

define('MAX_LOGIN_ATTEMPS', 5);
define('DEFAULT_CLIENT', 'default');
define('JS_PATH', 'assets');
define('JS_CONTROLLERS_PATH', 'controllers');
define('CSS_PATH', 'assets');
define('TMP_PATH', '/tmp');
define('UPLOAD_PATH', 'uploads');
define('CACHE_PATH', 'uploads/cache');
define('CACHE_THUMB_W', 130);
define('CACHE_THUMB_H', 130);
define('CACHE_GENERIC_IMG', '../assets/backend/css/img/ftype_presentation-svg.png');

define('DEFAULT_LANGUAGE', 'ES');
define('VERSION', '100');

/* End of file constants.php */
/* Location: ./application/config/constants.php */