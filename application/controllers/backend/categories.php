<?php

class Categories extends CI_Controller {

	var $table = 'categories';
	var $keyField = 'uid';
	
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$this->load->model('backend/categories_model');
		$node = $this->ajax->get('node');
		if($checked = $this->input->get('checked')) {
			$checked = json_decode($checked,true);
			if(!$checked) $checked = array($this->input->get('checked'));
		}
		//$node = ($node!='root') ? $node : FALSE; 
		$this->ajax->children = $this->categories_model->getTree($node, $checked);
		$this->ajax->json();
	}
	
	public function load() {
		$this->load->model('backend/categories_model');
		$res = $this->categories_model->load();
		$this->ajax->data = $res;
		$this->ajax->json();
	}	

	public function create() {
		if($post  = $this->ajax->post()) {
			$this->load->model('backend/categories_model');
			$this->ajax->results = $this->categories_model->edit($post);
		}
		$this->ajax->json();
	}

	public function edit() {
		if($post  = $this->ajax->post()) {
			$this->load->model('backend/categories_model');
			$this->ajax->results = $this->categories_model->edit($post);
		}
		$this->ajax->json();
	}
	
	public function update() {
		$this->load->model('backend/categories_model');
/*
		$data['uid'] = $this->ajax->post_data('id');
		$data['name'] = $this->ajax->post_data('text');
		$data['parent'] = $this->ajax->post_data('parentId');
		$data['active'] =1;
*/		
		$data = $this->ajax->post_data();
		$this->ajax->children = $this->categories_model->update($data);
		$this->ajax->json();
	}

	public function destroy() {
		$this->load->model('backend/categories_model');
		$data['uid'] = json_decode($this->ajax->post('id'), true);		
		$this->ajax->results = $this->categories_model->delete($data);
		$this->ajax->json();
	}
	

}

/* End of Pages Class */