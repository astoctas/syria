<?php

class Contents extends CI_Controller {

	var $table = 'contents';
	var $keyField = 'uid';

	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	public function load() {
		$this->load->model('backend/contents_model');
		$res = $this->contents_model->load();
		$this->ajax->data = $res;
		$this->ajax->json();
	}
	
	public function read() {
		$this->load->model('backend/contents_model');
		if(isset($this->ajax->filters_array['query'])) {
			$res = $this->contents_model->search();
		} else {
			$res = $this->contents_model->get();
		}
		$this->ajax->data = $res;
		$this->ajax->json();
	}

	public function destroy() {
		$this->ajax->set_delete($this->table, $this->keyField);
	}
	
	public function create() {
		if($post  = $this->ajax->post()) {
			$this->load->model('backend/contents_model');
			$this->ajax->results = $this->contents_model->edit($post);
		}
		$this->ajax->json();
	}

	public function edit() {
		if($post  = $this->ajax->post()) {
			$this->load->model('backend/contents_model');
			$this->ajax->results = $this->contents_model->edit($post);
		}
		$this->ajax->json();
	}

	public function files() {
		$this->load->model('backend/contents_model');
		$this->ajax->data = $this->contents_model->get_files();
		$this->ajax->json();
	}
	
	public function toggle($field = "active") {
		$this->load->model('backend/contents_model');
		$this->ajax->success = $this->contents_model->toggleField($this->input->post('uid'), $field);
		$this->ajax->json();
	}
 


}

/* End of Pages Class */