<?php

class Cron extends CI_Controller {


	function __construct() {
		parent::__construct();
	}
	
	public function check_bounces() {
		$this->load->model('backend/config_model');
		$config = $this->config_model->load();
		
		$inbox  = imap_open('{'.$config['mailing_smtp_host'].':143/novalidate-cert}INBOX', $config['mailing_smtp_user'], $config['mailing_smtp_password']);
		$emails = imap_search($inbox,'ALL');

		$this->load->library('BounceHandler');
		$b = new BounceHandler();
		$count = 0;

		foreach($emails as $email_number) {     
		    $overview = imap_fetch_overview($inbox,$email_number,0);
			$body = imap_fetchheader ($inbox, $email_number);
			$body .= imap_body($inbox, $email_number);
			$data = $b->get_the_facts($body);
			if($b->is_a_bounce()){
				preg_match_all('/X-Mailing: (.*)/', $body, $res);
				foreach($data as $d){
					$this->db->insert('mailing_suscribers_bounces', array(
						'mailing_uid' => ($res) ? $res[1][0] : 0,
						'email' => $d['recipient'],
						'status' => $d['status']
					));
					$count++;
				}
			}
			//imap_delete($inbox,$email_number);
		  }
		imap_expunge($inbox); 
		imap_close($inbox);
		$this->ajax->results = $count;
		$this->ajax->json();
	}

}

/* End of Pages Class */