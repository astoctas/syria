<?php

class Fields extends CI_Controller {

	var $table = 'fields';
	var $keyField = 'uid';

	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$res = $this->ajax->query($this->table);
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}

	public function destroy() {
		$this->ajax->delete($this->table, $this->keyField);
	}	
	

}

/* End of Pages Class */