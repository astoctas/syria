<?php
class Filebank extends CI_Controller {
    
	var $table = 'filebank_live';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
		
	public function read() {
		if($query = $this->ajax->get('query')){
			$this->load->model('backend/filebank_model');
			$res = $this->filebank_model->query($query);
		} else {
			$res = $this->ajax->query($this->table);
		}
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}

	public function destroy() {
		if($data = $this->ajax->post_data()) {
			if(key($data)=="id") $data = array($data);
			$this->ajax->results = 0;
			foreach($data as $rec) {
				$update = array('deleted'=> 1);
				$this->db->update('filebank',$update, "uid = '{$rec['uid']}'");
				$this->ajax->results += $this->db->affected_rows();
			}
		}
		$this->ajax->json();
	}
	
	public function sizes() {
		$uid = $this->input->get('filebank_uid');
		$this->load->model('backend/filebank_model');
		$this->ajax->data  = $this->filebank_model->getSizes($uid);
		$this->ajax->json();
	}
	
	public function crop() {
		$data = $this->input->post();
		if(!$data) return FALSE;
		/* TODO CROP FILE */
		$this->load->library('SimpleImage');
		$cropSize = $this->db->get_where('filebank_sizes', array('uid'=>$data['filebank_size_uid']))->row_array();
		$filebank = json_decode($data['filebank'],true);
		$coords = json_decode($data['data'],true);
		$_client = Auth::getClient();
		$filePath = BASEPATH.'../'.UPLOAD_PATH."/_clients/$_client/{$filebank['uid']}/";
		$img = new SimpleImage($filePath.$filebank['filename']);
		$destPath  = $filePath.$cropSize['width']."x".$cropSize['height']; 
		@mkdir($destPath);
		$x = abs((float)$coords['x']) * (float)$filebank['widthRatio'];
		$x2 = abs((float)$coords['x2']) * (float)$filebank['widthRatio'];
		$y = abs((float)$coords['y']) * (float)$filebank['heightRatio'];
		$y2 = abs((float)$coords['y2']) * (float)$filebank['heightRatio'];
		$img->crop($x,$y,$x2,$y2)->resize($cropSize['width'],$cropSize['height'])->save($destPath."/".$filebank['filename']);
		//echo 1; exit; 
		$this->load->model('backend/filebank_model');
		$this->success = $this->filebank_model->saveCrop($data);
		$this->ajax->json();
		
	}
	
	public function cache($uid, $filename) {
		header('Content-type: image/png');
		$_client = Auth::getClient();
		$filePath = BASEPATH.'../'.CACHE_PATH."/$_client/$uid/th_$filename.png";
		if(file_exists($filePath))	readfile($filePath);
		else readfile(BASEPATH.CACHE_GENERIC_IMG);
	}
	
}
