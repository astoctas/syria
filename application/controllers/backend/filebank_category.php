<?php
class Filebank_category extends CI_Controller {

	var $table = "filebank_category";    
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
		
	public function read() {
		$this->load->model('backend/filebank_category_model');
		$node = $this->ajax->get('node');
		$this->ajax->children = $this->filebank_category_model->getTree($node);
		$this->ajax->json();
	}

	public function update() {
		$this->load->model('backend/filebank_category_model');
		$data = $this->ajax->post_data();
		$this->ajax->children = $this->filebank_category_model->update($data);
		$this->ajax->json();
	}

	public function destroy() {
		$this->load->model('backend/filebank_category_model');
		$data['uid'] = $this->ajax->post_data('id');		
		$this->ajax->results = $this->filebank_category_model->delete($data);
		$this->ajax->json();
	}
	
	public function deleteFiles() {
		if($category_uid = $this->ajax->post('category_uid')) {
			$this->db->where('category_uid',$category_uid);
			$this->db->update('filebank', array('deleted'=>1));		
			$this->ajax->results = $this->db->affected_rows();	
		}
		$this->ajax->json();
	}
	
	
}
