<?php

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		$_client = $_SESSION['_client'];
		@include APPPATH."_clients/$_client/license.php";
	}
		
	public function index() {
		$this->load->library('auth');
		//ob_start( 'ob_gzhandler' );
		header("Cache-Control:public, max-age=31536000");
		header("Pragma: cache");
		$ru = $_SERVER['REQUEST_URI'];
		$this->load->model('backend/config_model');
		$this->config_model->load();
		if(substr_compare ($ru, 'backend/', strlen($ru)-8, 8)!=0){
			header("Location: ".base_url().'backend/');
			exit;
		}
		$user = Auth::getUserAdmin();
		$username = ($user) ? $user->username : FALSE;
		$this->smarty->assign('_data',array('username'=>$username));
		$this->smarty->assign('_client',Auth::getClient());
		$this->smarty->template_dir = APPPATH . "views/templates";
		$this->smarty->view( 'backend/home.tpl');
	}
	
}
