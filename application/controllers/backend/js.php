<?php
class JS extends CI_Controller {
    
    public function lang ($controller = "", $language = "")
    {
        $language = ($language) ? $language : $this->config->item ('language');
        $filePath = APPPATH."language/$language/{$controller}_lang.php";
        if ($controller)
        {
            if (file_exists ($filePath))
            {
                $this->lang->load ($controller, $language);
            }
        }
	        header ("Content-type: text/json");
			$data = array();
			foreach($this->lang->language as $k => $v){
				$data[] = array('key'=>$k, 'value'=>$v);
			}
			echo utf8_encode (json_encode (array('data'=>$data)));
//        	header ("Content-type: text/javascript");
//    		echo "var __lang = ".utf8_encode (json_encode ($this->lang->language));
    }
    
    public function constants ()
    {
        $this->load->helper ('url');
        $consts = get_defined_constants (TRUE);
        $consts['user']['BASEURL'] = base_url ();
        header ("Content-type: text/javascript");
        echo "var _const = ".utf8_encode (json_encode ($consts['user']));
    }

}
// END Lang Class
