<?php

class Layouts extends CI_Controller {

	var $table = 'layouts';
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$res = $this->ajax->query($this->table);
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}

}

/* End of Pages Class */