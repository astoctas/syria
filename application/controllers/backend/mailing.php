<?php

class Mailing extends CI_Controller {

	var $table = 'mailing';
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$res = $this->ajax->query('mailing_view');
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}
	
	public function destroy() {
		$this->ajax->delete($this->table, $this->keyField);
	}	

	public function edit() {
		$this->load->model('backend/mailing_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->mailing_model->update($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}

	public function load() {
		$this->load->model('backend/mailing_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->ajax->data = $this->mailing_model->load($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}
	
	public function queue() {
		$uid = $this->input->post('uid');
		$m = $this->db->get_where($this->table, array($this->keyField=>$uid))->row_array();
		$this->ajax->results = $m['queue_count'];
		if($m['status']=='new') {
			$values = array();
			$lists = json_decode($m['mailing_list'],true);
			$pos = 0;
			if(is_array($lists)) {
				foreach($lists as $l) {
					$this->db->select("email");
					$this->db->join("user_site u ", "m.user_uid = u.uid", "inner");
					$addr = $this->db->get_where('mailing_lists_suscribers m', array("mailing_list_uid"=>$l['uid']))->result_array();
					if($addr)
						foreach($addr as $rec) {
							$pos++;
							$values[] = "('$uid', '{$rec['email']}', {$pos})";
						}
				}
			}
			$query = "INSERT IGNORE INTO mailing_queue VALUES ".implode(',',$values);
		    $this->db->query($query);			
			$this->ajax->results = $pos;		
			$this->db->where($this->keyField,$uid);
			$this->db->update($this->table, array('status'=>'queued', 'queued'=>$pos,  'queue_count'=>$pos) );	
		}
		$this->ajax->json();
	}

	private function getContent($_c = array()) {

				$this->db->select('s.*, o.layout, l.file');
				$this->db->join('objects o',"o.uid = s.object", "left");
				$this->db->join('layouts l',"l.uid = o.layout", "left");
				$_ct = $this->db->get_where('contents_active s', array('s.uid'=>$_c['contents_uid']))->row_array();
				$renderData = $_ct;
				$renderData['_content'] = $_ct;
				if(empty($renderData['layout'])) {
					$this->ajax->json();
					return;
				}
				$this->load->model('frontend/datastore');
				$_ct['content_uid'] = $_ct['uid']; 
				$_content = array_shift($this->datastore->getData(array($_ct)));
				/* CONSUMIR LOS TEMPLATES */
				$_client = Auth::getClient();
				$this->smarty->template_dir = APPPATH . "_clients/$_client/templates";
				$this->smarty->assign('URL', $this->config->item('base_url'));
				$this->smarty->assign('VERSION', defined('VERSION')?constant('VERSION'):time());
				$this->smarty->assign('ASSETS', $this->config->item('base_url').'/assets/_clients/'.$_client);
				$this->smarty->assign('UPLOADS', $this->config->item('base_url').'/'.UPLOAD_PATH.'/_clients/'.$_client);
				$this->smarty->assign('content', (isset($_content)) ? $_content : array());
				$this->load->model('frontend/sections');
				$_regions = $this->sections->render($renderData);
				/* ASIGNAR EL HTML DE CADA REGION */
				foreach($_regions as $_r => $_html)
					$this->smarty->assign('REGION_'.$_r, implode('',$_html));
				/* RESOLVER EL TEMPLATE */ 
				return  $this->smarty->fetch($renderData['file']);
		
	}
	
	private function emailConfig($m = array()) {
		 $_config['protocol']=  (Auth::getConfig('mailing_smtp')) ? 'smtp' : 'mail';
		 $_config['smtp_host']= Auth::getConfig('mailing_smtp_host');
		 $_config['smtp_port']=Auth::getConfig('mailing_smtp_port');
		 $_config['smtp_timeout']='90';
		 $_config['smtp_user']=Auth::getConfig('mailing_smtp_user');
		 $_config['smtp_pass'] = Auth::getConfig('mailing_smtp_password');
		 $_config['_smtp_auth']  =  Auth::getConfig('mailing_smtp_auth');
		 $_config['mailtype']='html';
		 $_config['charset']='utf-8';
		 $_config['newline']="\r\n";
		 $_config['crlf'] = "\r\n"; 
		$this->email->initialize($_config);
		$this->email->from(Auth::getConfig('mailing_from'));
		$this->email->reply_to(Auth::getConfig('mailing_reply_to'));
		$this->email->subject($m['subject']);
		$this->email->set_header('Return-Path', Auth::getConfig('mailing_reply_to'));				
		$this->email->set_header('X-Mailing', $m['uid']);				
		$this->email->set_header('User-Agent', 'CMS');				
		$this->email->set_header('X-Mailer', 'CMS');				
		return $_config;		
	}
	
	public function test() {
		$this->load->library('email');
		$uid = $this->input->post('uid');
		$m = $this->db->get_where($this->table, array($this->keyField=>$uid))->row_array();
		$mc = $this->db->get_where('mailing_contents', array('mailing_uid'=>$uid))->result_array();
		$addresses = array();
		foreach($mc as $_c) {
			$html = $this->getContent($_c);
			/* DIRECCIONES */
			$users = $this->input->post('addresses');
			if($users){
				$addresses = preg_split("/[\s,]+/", $users);
			}
			/* USUARIOS */ 
			$users = $this->input->post('users');
			if($users){
				$users = json_decode($users, true);
				foreach($users as $user) {
					$addresses[] = $user['email'];
				}
			}
			 $this->emailConfig($m);
			 $count = 0;
			 foreach($addresses as $email){
				$this->email->to($email);
				$this->email->set_header('X-MailingAddress', $email);
				$this->email->message($html);
				if($this->email->send()) $count++;
				
			 }
			$this->ajax->results = $count;
			$this->ajax->json();
		}
	}
	
	public function send() {
		$this->load->library('email');
		$uid = $this->input->post('uid');
		$m = $this->db->get_where($this->table, array($this->keyField=>$uid))->row_array();
		$limit = (int)Auth::getConfig('mailing_send_packet');
		if($m['queue_count'] > 0) {
			// GET CONTENT
			$mc = $this->db->get_where('mailing_contents', array('mailing_uid'=>$uid))->result_array();
			foreach($mc as $_c) {
				$html = $this->getContent($_c);
				
				/* TRAER DIRECCIONES DE LA COLA */
				$addr = $this->db->order_by('pos')->get_where('mailing_queue', array('mailing_uid'=>$uid),$limit)->result_array();
				$addresses = array();
				foreach($addr as $a){
					$addresses[] = $a['address'];
				}
				 
				 $this->emailConfig($m);
				 
				if($limit == 1) {
					$this->email->to(implode(',',$addresses));
					$this->email->set_header('X-MailingAddress', implode(',',$addresses));
				}	else { 
					$this->email->bcc(implode(',',$addresses));
				}
				$this->email->message($html);
				
				
				if ($this->email->send()) {
					/* ESTO VA ADENTRO DEL SEND */
					if(count($addresses)) {
						$this->db->where('mailing_uid',$uid)->where_in('address',$addresses)->delete('mailing_queue');
						foreach($addresses as $email)
							$this->db->insert('mailing_suscribers_sent', array(
								'email' => $email,
								'content_uid' => $_c['contents_uid']
							));
					}
					/* ******  */
				} else {
					$this->ajax->results = 0;
					$this->ajax->json();
					return;
				}
			}
			$count = $this->db->select('count(address) as queue_count')->group_by('mailing_uid')->where('mailing_uid',$m['uid'])->get('mailing_queue')->row_array();
			$this->ajax->data = $m;									
			$this->ajax->data['queue_count'] = ($count) ? (int)$count['queue_count'] :0;	
			$status = ($this->ajax->data['queue_count'] > 0) ? 'sending' : 'sent';								
			$this->db->where($this->keyField, $m['uid'])->update($this->table, array('status'=>$status,'queue_count'=>$this->ajax->data['queue_count'] ));
			$this->ajax->results = $limit;
		}		
		$this->ajax->json();
	}


}

/* End of Pages Class */