<?php
class Mailing_list extends CI_Controller {

	var $table = "mailing_lists";    
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
		
	public function read() {
		$this->load->model('backend/mailing_list_model');
		$node = $this->ajax->get('node');
		$checked = $this->ajax->get('checked');
		$checked = ($checked!==FALSE) ? empty($checked) ? array() : json_decode($checked,true)
			: FALSE;
		$this->ajax->children = $this->mailing_list_model->getTree($node, $checked);
		$this->ajax->json();
	}

	public function update() {
		$this->load->model('backend/mailing_list_model');
		$data = $this->ajax->post_data();
		$this->ajax->children = $this->mailing_list_model->update($data);
		$this->ajax->json();
	}

	public function destroy() {
		$this->load->model('backend/mailing_list_model');
		$data['uid'] = $this->ajax->post_data('id');		
		$this->ajax->results = $this->mailing_list_model->delete($data);
		$this->ajax->json();
	}
	
	public function deleteSuscribers() {
		if($mailing_list_uid = $this->ajax->post('mailing_list_uid')) {
			$this->db->where('mailing_list_uid',$mailing_list_uid);
			$this->db->update('filebank', array('deleted'=>1));		
			$this->ajax->results = $this->db->affected_rows();	
		}
		$this->ajax->json();
	}


	public function edit() {
		$this->load->model('backend/mailing_list_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->mailing_list_model->edit($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}

	public function load() {
		$this->load->model('backend/mailing_list_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->ajax->data = $this->mailing_list_model->load($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}	
	
}
