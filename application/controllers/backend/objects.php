<?php

class Objects extends CI_Controller {

	var $table = "objects";
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
//		$this->load->model('backend/schemas_model');
		$res = $this->ajax->query($this->table, "objects.*, l.uid as layout, controller", array("layouts l"=>"l.uid = objects.layout"), "left");
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}

	public function destroy() {
		$this->ajax->delete($this->table, $this->keyField);
	}	

	public function load() {
		if($uid = $this->input->post('uid')) {
			$res = $this->db->get_where($this->table, array($this->keyField => $uid));
			$this->ajax->data = $res->row_array();
		}
		$this->ajax->json();
	}

	public function edit() {
		$this->load->model('backend/objects_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->objects_model->update($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}


}

/* End of Pages Class */