<?php

class Options extends CI_Controller {

	var $table = 'layouts';
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function load() {
		$this->load->model('backend/config_model');
		$this->ajax->data = $this->config_model->load();
		$this->ajax->json();
	}


	public function edit() {
		$this->load->model('backend/config_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->config_model->update($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}

}

/* End of Pages Class */