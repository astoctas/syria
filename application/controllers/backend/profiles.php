<?php

class Profiles extends CI_Controller {

	var $table = 'user_site_profiles';
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$res = $this->ajax->query($this->table);
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}

	public function destroy() {
		$this->ajax->delete($this->table, $this->keyField);
	}	

	public function edit() {
		$this->load->model('backend/profiles_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->profiles_model->update($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}

	public function load() {
		if($data = $this->input->post()) {
			$res = $this->db->get_where($this->table, array('uid'=>$data['uid']));
			$this->ajax->data = $res->row_array();
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}

}

/* End of Pages Class */