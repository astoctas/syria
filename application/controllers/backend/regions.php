<?php

class Regions extends CI_Controller {

	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function save() {
		$this->load->model('backend/regions_model');
		$this->ajax->success = FALSE;
		if($data = $this->input->post()) {
			$this->ajax->success = $this->regions_model->save($data);
		}
		$this->ajax->json();
	}

	function read() {
		if($params = $this->input->post()) {
			$this->db->join('sections_sets ss', 'section.uid = ss.section_uid');
			$this->db->where('region',$params['region']);
			$this->db->where('struct',$params['struct']);
			$this->db->where('layout',$params['layout']);
			$this->db->order_by('order','ASC');
			$res = $this->db->get('section');
			$this->ajax->data = $res->result_array();
		}
		$this->ajax->json();
	}

}

/* End of Regions Class */