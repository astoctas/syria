<?php

class Schemas extends CI_Controller {

	var $table = 'schemas';
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$res = $this->ajax->query('schemas');
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}
	
	function fields() {
		$this->db->order_by('order', 'asc');
		$res = $this->db->get_where('fields', array('schema_uid'=>$this->ajax->post('schema')));
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}

	public function destroy() {
		$this->ajax->delete($this->table, $this->keyField);
	}	

	public function edit() {
		$this->load->model('backend/schemas_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->schemas_model->update($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}

	public function load() {
		$this->load->model('backend/schemas_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->ajax->data = $this->schemas_model->load($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}

	public function duplicate() {
		$uid = $this->input->post('uid');
		if(!$uid) return FALSE;
		$this->load->model('backend/schemas_model', 'schemas');
		$this->schemas->duplicate($uid);
		$this->ajax->json();
	}
}

/* End of Pages Class */