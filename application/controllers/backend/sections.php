<?php

class Sections extends CI_Controller {

	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	

	public function read() {
		$res = $this->ajax->query('sets_contents', 'content_uid,set,order, name', array('contents'=>'contents.uid = sets_contents.content_uid')); 
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}
	
	public function get($action="read") {
		$res = $this->ajax->query('section', 'section.*,set, show_default', array('sections_sets'=>'section.uid = sections_sets.section_uid')); 
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}
	
	public function tree($action="read") {
		switch($action) {
			case 'read':
				$this->load->model('backend/sections_model');
				$filters = $this->ajax->filters_array;
				$this->ajax->children = $this->sections_model->getTree($filters['set'], 'root');
				$this->ajax->json();
			break;
		}
	}

	public function filebank($action="read") {
		switch($action) {
			case 'read':
				$res = $this->ajax->query('sets_filebank', 'filebank.*, filebank_uid,set,sets_filebank.order, filename as name', array('filebank'=>'filebank.uid = sets_filebank.filebank_uid')); 
				$this->ajax->data = $res->result_array();
				$this->ajax->json();
			break;
		}
	}

	public function show_default() {
		$uid = $this->input->post('uid');
		$struct = $this->input->post('struct');
		$state = $this->input->post('state');
		$this->load->model('backend/sections_model', 'sections');
		$this->ajax->success = $this->sections->set_default($uid, $struct, $state);
		$this->ajax->json();
	}
	
	public function add() {
		$uid = $this->input->post('section_uid');
		if(!$uid) return FALSE;
		$this->load->model('backend/sections_model', 'sections');
		$this->ajax->data = $this->sections->add($uid);
		$this->ajax->json();
	}

	public function delete() {
		if($data = $this->input->post()) {
			if($data['section']) {
				$this->db->where('uid',$data['section']);
				$this->db->delete('section');
				$this->results = $this->db->affected_rows();			
			}
		}
		$this->ajax->json();
	}

	public function sets($action = 'read') {
		switch($action) {
			case 'read':
				$res = $this->ajax->query('sections_sets', 'section_uid, set', array('section s'=>'s.uid = sections_sets.section_uid'), "inner", "set"); 
				$this->ajax->data = $res->result_array();
			break;
			case 'update':
				$data = $this->input->post();
				$contents = json_decode($data['data'],true);
				$values = array();
				switch($data['section_type']){
					case 'contents':
						$this->db->delete('sets_contents', array('set'=>$data['set']));
						foreach($contents as $c) {
							$values[] = array('set'=>$data['set'], 'content_uid'=>$c['uid'], 'order'=>$c['order']);
						}
						if($values) $this->db->insert_batch('sets_contents', $values);
					break;
					case 'images':
					case 'filebank':
						$this->db->delete('sets_filebank', array('set'=>$data['set']));
						foreach($contents as $c) {
							$values[] = array('set'=>$data['set'], 'filebank_uid'=>$c['uid'], 'order'=>$c['order']);
						}
						if($values) $this->db->insert_batch('sets_filebank', $values);
					break;
					case 'tree':
						$this->db->delete('sets_trees', array('set'=>$data['set']));
						foreach($contents as $c) {
							$values[] = array('set'=>$data['set'], 'node_uid'=>$c['id'],'parent_uid'=>$c['parentId'], 'order'=>$c['index']);
						}
						if($values) $this->db->insert_batch('sets_trees', $values);
					break;
				}
			break;				
		}
		$this->ajax->json();
	}

}

/* End of Regions Class */