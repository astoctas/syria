<?php

class Templates extends CI_Controller {

	var $table = 'templates';
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$res = $this->ajax->query($this->table);
		$this->ajax->data = $res->result_array();
		$this->ajax->json();
	}

	public function edit() {
		$this->load->model('backend/templates_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->templates_model->update($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}
 
	public function destroy() {
		$this->ajax->delete($this->table, $this->keyField);
	}	

	public function load() {
		if($uid = $this->input->post('uid')) {
			$res = $this->db->get_where($this->table, array($this->keyField => $uid));
			$this->ajax->data = $res->row_array();
		}
		$this->ajax->json();
	}
	
}

/* End of Templates Class */