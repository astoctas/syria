<?php
class Upload extends CI_Controller {

	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	    
    public function get_uploadprogress ()
    {
        $data = array();
        $info = false;
        if ($this->input->post ('upID'))
        {    
            if (function_exists ("uploadprogress_get_info"))
            {
                $uploadKey = $this->input->post ('upID');
                $info = uploadprogress_get_info ($uploadKey);
            }
		}
        echo($info) ? json_encode ($info) : 'false';
    }
    
    public function upload_file ()
    {

		$uid =  $this->input->post('uid');
		$isNew = false;
		if(!$uid) {
			$isNew = true;
			$uid = $this->ajax->getUid();
		}
        
		$path_file = TMP_PATH."/$uid";
		
        if (! is_dir ($path_file))
            mkdir ($path_file);

		
        $config['upload_path'] = $path_file;
        $config['allowed_types'] = '*';
        $tam_gigas = 1.5; //Tamanyo maximo del archivo en gigas
        $config['max_size'] = $tam_gigas * 1048576; //
		$this->load->model('backend/filebank_model');
        $this->load->library ('upload');
        $this->upload->initialize ($config);

        if (! $this->upload->do_upload ('file'))
        {
        	if($isNew){
            	$this->ajax->msg = $this->upload->display_errors ('','');
				$this->ajax->success = false;
				$this->ajax->json();
				return;
			} else {
				$this->results = $this->filebank_model->update($this->input->post());
				$res = $this->db->get_where('filebank', array('uid' => $uid));
				$this->ajax->data = $res->result_array();
				$this->ajax->success = true;
				$this->ajax->json();
				return;								
			}
        } else
        {
            $uploaded_file = $this->upload->data ();
			$uploaded_file['uid'] = $uid;
			$uploaded_file['size'] = ceil($uploaded_file['file_size'] * 1024);			
			$uploaded_file['category_uid'] = $this->input->post('category_uid');
			$uploaded_file['caption'] = $this->input->post('caption');
			$uploaded_file['hash'] = sha1_file($uploaded_file['full_path']);
			$uploaded_file['filetype'] = ($uploaded_file['is_image']) ? 'image' : '';
			unset($uploaded_file['file_size']);
			unset($uploaded_file['image_size_str']);
						
			$rep = $this->db->get_where('filebank', array('hash' => $uploaded_file['hash'], 'deleted' => 0));
			if($rep->row_array()) {
				$this->ajax->msg = "filebank_file_repeated";
			}
			
			$this->ajax->data = $uploaded_file;
			$this->ajax->success = true;
			$this->ajax->json();
            return;
        }
    	
    }
	
	public function confirm() {
		if($data = $this->input->post()) {
			$this->load->model('backend/filebank_model');
			$data = json_decode($data['data'],true); 
			$uid = $data['uid'];
			$_client = Auth::getClient();
			$path_file = BASEPATH.'../'.UPLOAD_PATH."/_clients/$_client/$uid";
	        
	        if (! is_dir ($path_file))
	            mkdir ($path_file);

				
			if($data['is_image']) {
				$data['file_name'] = strtolower($data['file_name']);
				// SAVE CACHE FILE
				$path_cache_file = BASEPATH.'../'.CACHE_PATH."/$_client/$uid";
	             if (! is_dir ($path_cache_file))
	            	mkdir ($path_cache_file);
				$path_cache_file .= "/th_{$data['file_name']}.png";				
				$this->filebank_model->CreateCacheThumb($data['full_path'], $path_cache_file,CACHE_THUMB_W,CACHE_THUMB_H);
			}

			rename($data['full_path'], $path_file."/".$data['file_name']);

			if($this->filebank_model->edit($data)) {
				$res = $this->db->get_where('filebank', array('uid' => $uid));
				$this->ajax->data = $res->result_array();
				$this->ajax->success = true;
				$this->ajax->json();
				return;
			}
		}
	}
}
// END Upload Class

