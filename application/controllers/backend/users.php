<?php
class Users extends CI_Controller {
    
	var $table = "user";
	
	function __construct() {
		parent::__construct();
	}
		
    public function logout () {
		$this->session->unset_userdata('admin');
    	session_destroy();
		header("Location: ".$this->config->item('base_url').'/backend');    	
    }
	
    public function login ()
    {
    	$this->load->model('backend/users_model');
    	$data = $this->users_model->login($this->ajax->post());
		if($data) {
			$this->session->set_userdata(array('admin'=>$data));
		}
		$this->ajax->data = !empty($data);
    	$this->ajax->json();
    }
	
	function read() {
		$this->load->model('backend/users_model');
		if(isset($this->ajax->filters_array['query'])) {
			$res = $this->users_model->search();
			$this->ajax->data = $res;
		} else {
			$res = $this->ajax->query($this->table, "user.*", array('user_profiles'=>'user.profile = user_profiles.uid'), 'left');
			$this->ajax->data = $res->result_array();
		}
		$this->ajax->json();
	}

	public function destroy() {
		$this->ajax->delete($this->table, $this->keyField);
	}	

	public function load() {
		$this->load->model('backend/users_model');
		$this->ajax->data = $this->users_model->load();
		$this->ajax->json();
	}

	public function create() {
			$this->edit();
	}
	
	public function edit() {
		$this->load->model('backend/users_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->users_model->edit($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}
	
	public function chk_session() {
		$this->ajax->success = ($admin = $this->session->userdata('admin')) ? true : false;
		$this->ajax->json();
	}
	
}
