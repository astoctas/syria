<?php

class Websuscribers extends CI_Controller {

	var $table = "user_site";
	var $table_view = "user_site_view";
	var $keyField = 'uid';
	
	function __construct() {
		parent::__construct();
		Auth::checkLogin('admin');
	}
	
	function read() {
		$this->load->model('backend/webusers_model');
		if(isset($this->ajax->filters_array['query'])) {
			$res = $this->webusers_model->search();
			$this->ajax->data = $res;
		} else {
			// LISTAS DE ENVIO
			if(isset($this->ajax->filters_array['mailing_list_uid'])) {
				$joinType = 'inner';
				if($this->ajax->filters_array['mailing_list_uid'] == 'root') {
					unset($this->ajax->filters_array['mailing_list_uid']);
					$joinType = 'left';
				} 
				$res = $this->ajax->query($this->table_view, "$this->table_view.*", array('mailing_lists_suscribers m'=>'user_site_view.uid= m.user_uid'), $joinType);
			// ***** //	
			} else
				$res = $this->ajax->query($this->table_view, "$this->table_view.*, schema", array('user_site_profiles'=>'user_site_view.profile = user_site_profiles.uid'), 'left');
			$this->ajax->data = $res->result_array();
		}
		$this->ajax->json();
	}

	public function destroy() {
		$mailing_list_uid = $this->input->get('mailing_list_uid');
		if($mailing_list_uid == 'root') {
			$this->ajax->json();
			return;
		}
		$data = $this->ajax->post_data();
		if(isset($data['uid'])) $data = array($data);
		foreach($data as $d) {
			$this->db->where('user_uid',$d['uid']);
			$this->db->where('mailing_list_uid',$mailing_list_uid);
			$this->db->delete('mailing_lists_suscribers');
		}
		$this->ajax->success = true;
		$this->ajax->json();
	}	

	public function load() {
		$this->load->model('backend/webusers_model');
		$res = $this->webusers_model->load();
		$this->ajax->data = $res;
		$this->ajax->json();
	}

	public function create() {
			$this->edit();
	}
	
	public function edit() {
		$this->load->model('backend/webusers_model');
		$this->ajax->success = false;
		if($data = $this->input->post()) {
			$this->webusers_model->edit($data);
			$this->ajax->success = true;
		}
		$this->ajax->json();
	}


}

/* End of Pages Class */