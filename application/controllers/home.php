<?php

class Home extends CI_Controller {

	function __construct() {
		parent::__construct();
		$_client = Auth::getClient();
		@include APPPATH."_clients/$_client/license.php";
		if(!isset($_license)) show_404();
		/* IDIOMAS */
		if(defined('DEFAULT_LANGUAGE')) {
			if(!Auth::getLang())
				Auth::setLang(DEFAULT_LANGUAGE);
		}
		if($_lang = $this->input->get('lang')) { 
			Auth::setLang($_lang);
			header("Location: ".qs('lang','',true));	
		}
		$this->db->db_debug = (constant('ENVIRONMENT') != 'production');

	}
		
	public function index() {
		//echo $this->uri->uri_string();
		$error =& load_class('Exceptions', 'core');
		$this->load->model('backend/config_model');
		if($this->uri->total_segments()==0) {  // REDIRIGIR A LA HOME
			$this->load->helper('url');
			$uri = $this->config_model->get('home');
			if($uri) redirect($this->config->item('base_url')."/".$uri, 'refresh');
			exit;
		}

		$_level = 1;
		$_parent = 'root';
		$_breadcrum = array();
		/* CHECKEAR SI EXISTE LA RUTA */
		while($_slug = $this->uri->segment($_level)) {
			if($_level > 1 && (int)$_slug > 0) { // ES UN CONTENIDO - LOS CONTENIDOS COMIENZAN CON EL ID (ID-SLUG)
				$this->db->select('s.*, o.layout, l.file');
				$this->db->join('objects o',"o.uid = s.object", "left");
				$this->db->join('layouts l',"l.uid = o.layout", "left");
				$_ct = $this->db->get_where('contents_active s', array('id'=>(int)$_slug))->row_array();
				$_breadcrum[] = array('name'=>$_ct['name'], 'link' => $_slug);
				if(!$_ct) show_404();
			} else {  // ES UNA PAGINA
				$this->db->select('s.*, l.file');
				$this->db->join('layouts l',"l.uid = s.layout", 'left');
				$_st = $this->db->get_where('struct_active s', array('slug'=>$_slug, 'parent'=>$_parent))->row_array();
				$_breadcrum[] = array('name'=>$_st['name'], 'link' => $_st['slug']);
				if(isset($_st['private'])&&$_st['private']) {
					echo $error->show_error('ERROR','Debe ingresar para ver este contenido.', 'error_login');
					exit;
				}
				if(!$_st) show_404();
				$_parent = $_st['uid'];
			}
			$_level++;
		}
		//print_r($_st);  // OK
		/* CHECKEAR SI REDIRIGE */
		if(!isset($_ct) && $_st['redirect']){
			$_location = $this->config->item('base_url')."/".$this->uri->uri_string()."/".$_st['redirect'];
			if(strpos($_st['redirect'], 'http://')===0) $_location = $_st['redirect'];
			if(strpos($_st['redirect'], '/')===0) $_location = $this->config->item('base_url')."/".$_st['redirect'];
			header("Location: {$_location}");
			exit;
			//redirect($_st['redirect'], 'refresh');
		}
		/* TIENE QUE TENER LAYOUT ASIGNADO */
		if(isset($_ct)) {
			if($_st['layout_for_contents']) {
				$renderData = $_st;
				$renderData['struct'] = $_st['uid'];
			}	else {
				$renderData = $_ct;
				$renderData['struct'] = $_ct['object'];
			}
			$renderData['_content'] = $_ct;
			$pageTitle = $_ct['name'];
		} else {
			$renderData = $_st;
			$renderData['struct'] = $_st['uid'];
			$renderData['_content'] = $_st;
			$pageTitle = $_st['name'];
		}
		if(empty($renderData['layout'])) show_404(); 

		/* TRAER DATOS DE LA PAGINA Y/O EL CONTENIDO  */
		$this->load->model('frontend/datastore');
		if(isset($_ct)) {
			$_ct['content_uid'] = $_ct['uid']; 
			$_content = array_shift($this->datastore->getData(array($_ct)));
		}
		$_st['content_uid'] = $_st['uid']; 
		$_page = array_shift($this->datastore->getData(array($_st)));
		$_page['_breadcrum'] = $_breadcrum;

		/* TRAER ESTRUCTURA DE PAGINAS */
		$this->load->model('backend/pages_model');
		$renderData['_pages'] = $this->pages_model->getTree('root', FALSE, $_pagesFlat);
		$renderData['_pagesFlat'] = $_pagesFlat;
		$_SESSION['renderData'] = $renderData;
		
		
		/* CONSUMIR LOS TEMPLATES */
		$_client = Auth::getClient();
		@include APPPATH."_clients/$_client/language/".Auth::getLang().".php";
		$this->smarty->assign('URL', $this->config->item('base_url'));
		$this->smarty->assign('VERSION', defined('VERSION')?constant('VERSION'):time());
		$this->smarty->assign('ASSETS', $this->config->item('base_url').'/assets/_clients/'.$_client);
		$this->smarty->assign('UPLOADS', $this->config->item('base_url').'/'.UPLOAD_PATH.'/_clients/'.$_client);
		$this->smarty->assign('content', (isset($_content)) ? $_content : array());
		$this->smarty->assign('page', $_page);
		$this->smarty->assign('_lang', isset($_lang) ? $_lang : array());
		$this->smarty->assign('current_lang', Auth::getLang());
		$this->smarty->assign('pageTitle', $pageTitle);
		$this->smarty->assign('_config', $this->config_model->get());
		
		$this->load->model('frontend/sections');
		$_regions = $this->sections->render($renderData);
		/* ASIGNAR EL HTML DE CADA REGION */
		foreach($_regions as $_r => $_html)
			$this->smarty->assign('REGION_'.$_r, implode('',$_html));
		
		/* MOSTRAR EL TEMPLATE */ 

		$this->smarty->view($renderData['file']);
		 
	}
	
	public function redir($uid = "") {
		if(!Auth::getUserAdmin()) { 
			show_404();
			exit;
		}
		$segments = array();
		do {
			$p = $this->db->get_where('struct', array('uid'=>$uid))->row_array();
			if($p) {
				$segments[] = $p['slug'];
				$uid = $p['parent'];
			} else $uid = '';
		} while($p && ($p['parent']!='root'));
		if(count($segments)==0) show_404();
		$loc = $this->config->item('base_url').'/'.implode('/', array_reverse($segments));
		header("Location: $loc");
	}
	
	
}
