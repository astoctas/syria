<?php

class Request extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		@session_start();
	}
		
	public function lang($tab = FALSE) {
		$this->tab($tab);
	}
	
	public function tab($tab = FALSE) {
		if(!$tab) {
			echo Auth::getTab();
			return FALSE;
		}
		Auth::setTab($tab);
		header("Location: ".$_SERVER['HTTP_REFERER']);
		return;
	}
	
	public function get($model = FALSE, $method = 'get') {
		$_client = Auth::getClient();
		if(!$model) show_404();
		$this->load->model("../_clients/$_client/models/{$model}_custom", 'model');
		$this->model->$method();
	}
	
}