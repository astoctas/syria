<?php

class Sitemap extends CI_Controller {

	function __construct() {
		parent::__construct();
	}
		
	public function xml() {
		header('Content-type: text/xml');
		$_pagesFlat = array();
		$base_url = $this->config->item('base_url')."/";
		/* TRAER ESTRUCTURA DE PAGINAS */
		$this->load->model('backend/pages_model');
		$_pages = $this->pages_model->getTree('root', FALSE, $_pagesFlat);
		
		$lines = array();
		$lines[] = '<?xml version="1.0" encoding="UTF-8"?>';
		$lines[] = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
		foreach($_pagesFlat as $uid => $_p) {
			if(!empty($_p['slug'])&&!empty($_p['layout'])) {
				$lines[] = "<url>";			
				$lines[] = "<loc>".$base_url.$_p['slug']."</loc>";			
				$lines[] = "<lastmod>".date("Y-m-d",strtotime($_p['mod_date']))."</lastmod>";			
				$lines[] = "</url>";
			}
		}
		
		$lines[] = '</urlset>';
		echo implode("\n",$lines);
	}
	
}
