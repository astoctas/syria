<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Generic Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 *  
 */

// ------------------------------------------------------------------------
if ( ! function_exists('GenUid'))
{
	function GenUid() {
		$CI =& get_instance();
		$res = $CI->db->query('SELECT UUID() AS uuid');
		$res = $res->row();
		return $res->uuid;
	}
}

	// ValidateUid()
	// - Return true if given string is a valid uid format
if ( ! function_exists('ValidateUid'))
{	
	 function ValidateUid($string) {
	    if (strlen($string) == 36) {
		if (preg_match('/[0-9a-f]{8,8}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{4,4}-[0-9a-f]{12,12}/', $string)) {
		    return true;
		}
	    }
	    return false;
	}
}

if ( ! function_exists('array_quote_string'))
{
	function array_quote_string(&$a, $i) {
		$a = "'$a'";
	}
}

if ( ! function_exists('array_quote'))
{
	function array_quote($array = array()) {
		array_walk_recursive($array, 'array_quote_string');
		return $array;
	}
}


// ------------------------------------------------------------------------
/* End of file database_helper.php */
/* Location: ./application/helpers/database_helper.php */