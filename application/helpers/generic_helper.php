<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter Generic Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 *  
 */

// ------------------------------------------------------------------------

/**
 * Pr
 *
 * Equivalent to print_r
 *
 * @access	public
 * @param	string	print_r argument
 */
if ( ! function_exists('pr'))
{
	function pr($arg = '')
	{
		print_r($arg);
	}
}


/**
 * Qs
 *
 * Changes a portion of query string
 *
 * @access	public
 * @param	string	print_r argument
 */
if ( ! function_exists('qs'))
{
	function qs($key = '', $value = "", $delete = FALSE)
	{
		$parse = parse_url($_SERVER['REQUEST_URI']);
		$path = $parse['path'];
		$path = (substr($path,-1)=="/") ? $path : "$path/";
		if(!isset($parse['query'])) $parse['query'] = "";
		parse_str($parse['query'], $qs);
		if($delete)
			unset($qs[$key]);
		else
			$qs[$key] = $value;
		$bq = http_build_query($qs);
		return ($bq) ? $path.'?'.$bq : $path;
	}
}

// ------------------------------------------------------------------------
/* End of file generic_helper.php */
/* Location: ./application/helpers/generic_helper.php */