<?php

function loadClient() {
	session_start();
	if(!isset($_COOKIE['_client'])) $_COOKIE['_client'] = DEFAULT_CLIENT;
	$_SESSION['_client'] = isset($_GET['_client']) ? $_GET['_client'] : $_COOKIE['_client'];
	if(isset($_SERVER['QUERY_STRING'])) {
		$qs = $_SERVER['QUERY_STRING'];
		parse_str($qs, $arr);
		if(isset($arr['_client'])){
			$_SESSION['_client'] = $arr['_client'];
			setcookie('_client', $arr['_client'],0,'/');
		}
	}
}


function loadDB() {
	$CI =& get_instance();
	$_client = $_SESSION['_client'];
	if(!empty($_client)) {
		$dbConfig = BASEPATH.'../'.APPPATH."_clients/$_client/config/database.php";
		include $dbConfig;
		$CI->load->database($db[$active_group]);
	} else 
		$CI->load->database();
}
