<?php

$lang['si']  = "Sí";
$lang['no']  = "No";
$lang['of']  = "de";

$lang['region_west']  = "Administradores";
$lang['region_center']  = "Editores";

$lang['login_login']  = "Ingresar";
$lang['login_user']  = "Usuario";
$lang['login_pass']  = "Clave";
$lang['login_invalid']  = "Usuario y/o clave inválidos";

$lang['form_submit']  = "Guardar";
$lang['form_submit_close']  = "Guardar y cerrar";
$lang['form_send']  = "Enviar";
$lang['form_not_valid']  = "Existen campos con errores.  Revise el formulario.";
$lang['form_wait_msg']  = "Espere, por favor.";

$lang['menu_admin']  = "Administrar";
$lang['menu_pages']  = "Páginas";
$lang['menu_contents']  = "Contenidos";
$lang['menu_categories']  = "Categorías";
$lang['menu_objects']  = "Objetos";
$lang['menu_schemas']  = "Esquemas";
$lang['menu_templates']  = "Plantillas";
$lang['menu_edit']  = "Editar";
$lang['menu_filebank']  = "Banco de Archivos";
$lang['menu_webusers']  = "Usuarios Web";
$lang['menu_cmsusers']  = "Usuarios CMS";
$lang['menu_profiles']  = "Perfiles Web";
$lang['menu_logout']  = "Salir";
$lang['menu_config']  = "Configuración del sitio";
$lang['menu_mailing']  = "Mailing";
$lang['menu_mailing_list']  = "Listas de envío";

$lang['editor_already_open'] = "El editor ya se encuentra abierto.";

$lang['pages_diagramm']  = "Diagramar";
$lang['pages_no_layout']  = "La página no tiene plantilla asignada";
$lang['pages_saved']  = "La página <strong>{0}</strong> ha sido guardada";
$lang['pages_new']  = "Nueva página";
$lang['pages_view']  = "Ver";
$lang['pages_edit']  = "Editar página {0}";
$lang['pages_name']  = "Nombre";
$lang['pages_slug']  = "Slug";
$lang['pages_diagram']  = "Diagrama";
$lang['pages_schema']  = "Esquema";
$lang['pages_properties']  = "Propiedades";
$lang['pages_prompt']  = "Nombre de la página";
$lang['pages_private']  = "Privada";
$lang['pages_redirect']  = "Redirigir a";
$lang['pages_diagram_for_contents']  = "Usar diagrama para contenidos";
$lang['pages_pagecontent']  = "Contenidos de la página";
$lang['pages_contents_open']  = "Abrir";
$lang['pages_no_paged_selected']  = "Debe seleccionar una página";

$lang['categories_saved']  = "La categoría <strong>{0}</strong> ha sido guardada";
$lang['categories_new']  = "Nueva categoría";
$lang['categories_edit']  = "Editar categoría {0}";
$lang['categories_name']  = "Nombre";
$lang['categories_schema']  = "Esquema";
$lang['categories_properties']  = "Propiedades";
$lang['categories_prompt']  = "Nombre de la categoría";

$lang['objects_no_layout']  = "El objeto no tiene plantilla asignada";
$lang['objects_combo_empty']  = "Elegir un objeto...";

$lang['schema_schema']  = "Esquema";
$lang['schema_name']  = "Nombre";
$lang['schema_saved']  = "El esquema <strong>\"{0}\"</strong> ha sido guardado";

$lang['fields_name']  = "Nombre";
$lang['fields_tab']  = "Solapa";
$lang['fields_datatype']  = "Tipo de campo";
$lang['fields_label']  = "Etiqueta";
$lang['fields_required']  = "Requerido";
$lang['fields_vtype']  = "Validador";
$lang['fields_default_value']  = "Valor por defecto";
$lang['fields_fields']  = "Campos";
$lang['fields_help']  = "Ayuda";
$lang['fields_new_field']  = "Nuevo Campo";
$lang['fields_added']  = "El campo ha sido agregado";
$lang['fields_modified']  = "El campo ha sido modificado";
$lang['fields_editor']  = "Editar Campo";
$lang['fields_textfield']  = "Texto";
$lang['fields_textareafield']  = "Texto largo";
$lang['fields_datefield']  = "Fecha";
$lang['fields_timefield']  = "Hora";
$lang['fields_filefield']  = "Archivo";
$lang['fields_numberfield']  = "Numérico";
$lang['fields_checkboxfield']  = "Verificación";
$lang['fields_selectfield']  = "Selección";
$lang['fields_checkgroupfield']  = "Grupo de Checks";
$lang['fields_radiogroupfield']  = "Grupo de Radios";
$lang['fields_keyvaluefield']  = "Listado";
$lang['fields_options']  = "Opciones";
$lang['fields_options_show_other']  = "Mostrar Otro";
$lang['fields_categoryfield']  = "Categorías";
$lang['fields_contentsfield']  = "Contenidos";
$lang['fields_pagesfield']  = "Páginas";
$lang['fields_category_parent']  = "Categoría Raíz";
$lang['fields_object_parent']  = "Objeto";
$lang['fields_mailinglistfield']  = "Lista de envío";

$lang['vtype_text']  = "Texto";
$lang['vtype_email']  = "Email";
$lang['vtype_url']  = "URL";
$lang['vtype_alphanum']  = "Alfanumérico";
$lang['vtype_image']  = "Imagen";
$lang['vtype_audio']  = "Audio";
$lang['vtype_video']  = "Video";
$lang['vtype_document']  = "Documento";
$lang['vtype_any']  = "Cualquiera";
$lang['vtype_slug_msg']  = "El campo debe comenzar con una letra y sólo puede contener letras, números y guión";
$lang['vtype_json_msg']  = "El texto debe tener formato JSON.";

$lang['objects_object']  = "Objeto";

$lang['contents_name']  = "Nombre";
$lang['contents_url']  = "URL";
$lang['contents_date']  = "Fecha";
$lang['contents_new']  = "Nuevo";
$lang['contents_edit']  = "Editar";
$lang['contents_status']  = "Estado";
$lang['contents_fields']  = "Campos";
$lang['contents_saved']  = "El contenido <strong>\"{0}\"</strong> ha sido guardado.";

$lang['grid_new']  = "Nuevo";
$lang['grid_edit']  = "Editar";
$lang['grid_delete']  = "Eliminar";
$lang['grid_duplicate']  = "Duplicar";
$lang['grid_move_to']  = "Mover";
$lang['grid_config']  = "Configurar";
$lang['grid_actions']  = "Acciones";

$lang['filegrid_dragText']  = "Arrastrar para reordenar";
$lang['filegrid_details']  = "Detalles";
$lang['filegrid_image']  = "Imagen";
$lang['filegrid_add']  = "Agregar";
$lang['filegrid_up']  = "Subir";
$lang['filegrid_down']  = "Bajar";
$lang['filegrid_delete']  = "Eliminar";
$lang['filegrid_files']  = "Archivos";
$lang['filegrid_info']  = "Info";

$lang['gridfield_up']  = "Subir";
$lang['gridfield_down']  = "Bajar";

$lang['filebank_filebank']  = "Banco de Archivos";
$lang['filebank_noCategory']  = "Sin categoría";
$lang['filebank_add']  = "Adjuntar";
$lang['filebank_add_selected']  = "Adjuntar archivo";
$lang['filebank_caption']  = "Descripción";
$lang['filebank_edit']  = "Editar";
$lang['filebank_editing']  = "Editando";
$lang['filebank_delete']  = "Eliminar";
$lang['filebank_file']  = "Archivo";
$lang['filebank_filename']  = "Nombre de Archivo";
$lang['filebank_image']  = "Imagen";
$lang['filebank_new']  = "Nuevo";
$lang['filebank_new_file']  = "Archivo Nuevo";
$lang['filebank_upload']  = "Subir archivo";
$lang['filebank_upload_add']  = "Subir archivo y adjuntar";
$lang['filebank_upload_progress']  = "{0} subidos - Restan: {1} seg.";
$lang['filebank_file_uploaded']  = "El archivo <strong>\"{0}\"</strong> ha sido subido";
$lang['filebank_file_added']  = "El archivo <strong>\"{0}\"</strong> ha sido adjuntado";
$lang['filebank_file_not_valid']  = "No se permiten este tipo de archivos en este campo";
$lang['filebank_file_repeated']  = "El archivo subido ya se encuentra en el Banco de Archivos.";
$lang['filebank_not_an_image']  = "El archivo no es una imagen";
$lang['filebank_empty']  = "<div class=\"x-grid-empty\">No hay archivos en esta categoría</div>";
$lang['filebank_records']  = "{0} archivos";
$lang['filebank_crop']  = "Recortar";
$lang['filebank_crop_success']  = "El recorte ha sido guardado.";
$lang['filebank_sizes']  = "Tamaños";
$lang['filebank_select_size']  = "Elegir Tamaño...";
$lang['filebank_reset']  = "Restablecer";

$lang['filebank_category_new']  = "Nueva categoría";
$lang['filebank_category_edit']  = "Editar categoría";
$lang['filebank_category_prompt']  = "Nombre de la nueva categoría";
$lang['filebank_category_changeName']  = "Nombre de la categoría";
$lang['filebank_category_category']  = "Categoría";
$lang['filebank_category_category_files']  = "Categoría y archivos";
$lang['filebank_category_confirm_delete']  = "¿Desea eliminar la categoría?<br/>Los archivos se transfieren a \"Sin categoría\"";
$lang['filebank_category_confirm_delete_files']  = "¿Desea eliminar la categoría?<br/>Los archivos se eliminarán del sistema";


$lang['validation_image']  = "No se permite ese tipo";
$lang['validation_noexecutables']  = "No se permiten archivos ejecutables";


$lang['pager_noResults']  = "No hay registros";
$lang['pager_of']  = "de";
$lang['pager_beforePage']  = "Pág";

$lang['msg_title']  = "Alerta";
$lang['msg_title_information']  = "Alerta";
$lang['msg_title_error']  = "Error";
$lang['msg_yes']  = "Sí";
$lang['msg_no']  = "No";
$lang['msg_cancel']  = "Cancelar";
$lang['msg_confirm_delete']  = "¿Confirma eliminar el registro seleccionado?";
$lang['msg_confirm_duplicate']  = "¿Confirma duplicar el registro seleccionado?";
$lang['msg_no_selection']  = "Debe seleccionar un registro";
$lang['msg_file_exists']  = "El archivo <strong>\"{0}\"</strong> ya existe en este campo.";

$lang['record_modified_ok']  = "El registro fue modificado con éxito";
$lang['record_delete']  = "Los registros fueron eliminados.";
$lang['record_duplicated']  = "Los registros fueron duplicados.";
$lang['record_modified_nok']  = "ERROR: No se pudo modificar el registro";

$lang['template_select'] = "Elegir plantilla";
$lang['template_region'] = "Región {0}";
$lang['template_save'] = "Guardar";
$lang['template_saved'] = "La plantilla ha sido guardada";
$lang['template_properties'] = "Propiedades";
$lang['template_empty'] = "Debe seleccionar una plantinlla.  Revise las secciones.";
$lang['template_collapse_all'] = "Colapsar todo";
$lang['template_close_all'] = "Cerrar todo";
$lang['template_expand_all'] = "Expandir todo";
$lang['template_name'] = "Nombre";
$lang['template_controller'] = "Controlador";
$lang['template_method'] = "Método";
$lang['template_file'] = "Archivo";

$lang['object_edit'] = "Editar Objeto";
$lang['object_new'] = "Objeto Nuevo";
$lang['object_saved']  = "El objecto <strong>\"{0}\"</strong> ha sido guardado.";
$lang['object_properties']  = "Propiedades";
$lang['object_name']  = "Nombre";
$lang['object_schema']  = "Esquema";

$lang['section_add'] = "Agregar sección";
$lang['section_delete'] = "Eliminar sección";
$lang['section_save'] = "Guardar";
$lang['section_deleted'] = "La sección ha sido eliminada";
$lang['section_content'] = "Contenido";
$lang['section_dragdrop'] = "Arrastrar para ubicar en la sección";
$lang['section_confirm_delete'] = "¿Eliminar la sección?";
$lang['section_config'] = "Configurar Sección";
$lang['section_template'] = "Template";
$lang['section_name'] = "Nombre";
$lang['section_properties']  = "Propiedades";
$lang['section_titulo']  = "Título";
$lang['section_params']  = "Parámetros";
$lang['section_contents']  = "Contenidos";
$lang['section_images']  = "Imágenes";
$lang['section_filebank']  = "Banco de Archivos";
$lang['section_default']  = "Mostrar por defecto";
$lang['section_tree']  = "Árbol";
$lang['section_contents_select']  = "Contenidos";
$lang['section_contents_self']  = "Propios";
$lang['section_contents_auto']  = "Automatización";
$lang['section_contents_auto_category']  = "Categoría";
$lang['section_contents_auto_object']  = "Objeto";
$lang['section_contents_auto_number']  = "Cantidad";
$lang['section_contents_auto_frequency']  = "Frecuencia";
$lang['section_set_new']  = "Nuevo";
$lang['section_contents_renew']  = "Crear nuevo conjunto de contenidos";
$lang['section_confirm_renew']  = "Esta acción renovará el conjunto de contenidos de la sección.  ¿Desea proceder?";
$lang['section_paging']  = "Paginar";
$lang['section_combo_select_section']  = "Elegir una sección";

$lang['cmsusers_new'] = "Nuevo Usuario CMS";
$lang['cmsusers_edit'] = "Editar Usuario {0}";
$lang['users_saved']  = "El usuario <strong>\"{0}\"</strong> ha sido guardado.";

$lang['webusers_new'] = "Nuevo Usuario Web";
$lang['webusers_edit'] = "Editar Usuario {0}";
$lang['webusers_username'] = "Usuario";
$lang['webusers_password'] = "Clave";
$lang['webusers_firstname'] = "Nombres";
$lang['webusers_lastname'] = "Apellidos";
$lang['webusers_email'] = "Email";
$lang['webusers_profile'] = "Perfil";
$lang['webusers_saved']  = "El usuario <strong>\"{0}\"</strong> ha sido guardado.";

$lang['layout_select'] = "Elegir un diagrama";

$lang['profiles_new'] = "Nuevo Perfil";
$lang['profiles_edit'] = "Editar Perfil {0}";
$lang['profiles_select'] = "Elegir un perfil";
$lang['profiles_name'] = "Nombre";
$lang['profiles_schema'] = "Esquema";
$lang['profiles_saved']  = "El perfil <strong>\"{0}\"</strong> ha sido guardado.";
$lang['profiles_properties']  = "Propiedades";

$lang['tree_uncheck_all']  = "Ninguno";
$lang['tree_check_all']  = "Todo";
$lang['tree_select']  = "Seleccionar";
$lang['tree_records_selected']  = "{0} registros seleccionados";

$lang['keyvalue_key']  = "Valor a guardar";
$lang['keyvalue_value']  = "Valor a mostrar";

$lang['config_name']  = "Nombre del sitio";
$lang['config_home']  = "URL de inicio";
$lang['config_saved']  = "La configuración ha sido guardada.";
$lang['config_properties']  = "Propiedades";
$lang['config_mailing']  = "Mailing";
$lang['config_mailing_smtp']  = "Usar SMTP";
$lang['config_mailing_from']  = "Email de remitente";
$lang['config_mailing_reply_to']  = "Email de respuesta";
$lang['config_mailing_send_packet']  = "Direcciones por envío";
$lang['config_mailing_user']  = "Usuario";
$lang['config_mailing_password']  = "Clave";
$lang['config_mailing_host']  = "Servidor";
$lang['config_mailing_port']  = "Puerto";
$lang['config_mailing_auth']  = "Autenticación";
$lang['config_meta']  = "Metatags";
$lang['config_meta_title']  = "Título";
$lang['config_meta_description']  = "Descripción";
$lang['config_meta_imagen']  = "Imagen";


$lang['mailing']  = "Mailing";
$lang['mailing_name']  = "Nombre";
$lang['mailing_date']  = "Fecha";
$lang['mailing_edit']  = "Editar Envío {0}";
$lang['mailing_new']  = "Nuevo Envío";
$lang['mailing_send']  = "Enviar";
$lang['mailing_send_test']  = "Envío de prueba";
$lang['mailing_send_editor']  = "Enviar: {0}";
$lang['mailing_saved']  = "El envío ha sido guardado.";
$lang['mailing_subject']  = "Asunto";
$lang['mailing_reply_to']  = "Email respuesta";
$lang['mailing_contents']  = "Contenido";
$lang['mailing_pause']  = "Detener";
$lang['msg_confirm_mailing_send']  = "¿Enviar el mailing?";
$lang['mailing_sent']  = "Enviados";
$lang['mailing_percent']  = "Enviado";
$lang['mailing_bounces']  = "Rebotes";
$lang['mailing_sent_success']  = "El mailing: <strong>{0}</strong> ha sido enviado.";
$lang['mailing_test_sent_success']  = "Se enviaron {0} envíos de prueba.";
$lang['mailing_test_sent_start']  = "Enviando envíos de prueba...";
$lang['mailing_mailing_list']  = "Lista de envío";
$lang['mailing_check_bounces']  = "Comprobar Rebotes";
$lang['mailing_checking_bounces']  = "Comprobando Rebotes";
$lang['mailing_checked_bounces']  = "{0} Rebotes encontrados";

$lang['mailing_list_new']  = "Nueva";
$lang['mailing_list_edit']  = "Editar";
$lang['mailing_list_delete']  = "Eliminar";
$lang['mailing_list_all']  = "Todos";
$lang['mailing_list_name']  = "Nombre";
$lang['mailing_list_suscribers']  = "Suscriptores";
$lang['mailing_list_import']  = "Importar direcciones";
$lang['mailing_list_import_users']  = "Agregar Usuarios";
$lang['mailing_list_add']  = "Agregar";
$lang['mailing_list_add_info']  = "(Una dirección por línea)";
$lang['mailing_list_replace']  = "Reemplazar";
$lang['mailing_list_prompt']  = "Nombre de la nueva lista";
$lang['mailing_list_changeName']  = "Nombre de la lista";
$lang['mailing_list_list']  = "Lista de envío";
$lang['mailing_list_list_suscribers']  = "Lista y suscriptores";
$lang['mailing_list_confirm_delete']  = "¿Desea eliminar la Lista?";
$lang['mailing_list_confirm_delete_suscribers']  = "¿Desea eliminar la Lista?<br/>Los suscriptores se eliminarán del sistema";
$lang['mailing_list_saved']  = "La lista de envíos ha sido guardada.";
$lang['mailing_list_properties']  = "Propiedades";
