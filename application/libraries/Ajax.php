<?php
if (! defined ('BASEPATH'))
    exit('No direct script access allowed');
    
/**
 * Ajax Class
 *
 * @package	CodeIgniter
 * @subpackage	Libraries
 * @category	Ajax
 *
 */

class Ajax extends CI_Input {

    var $sort = array();
    var $filters = array();
    var $sort_array = array();
    var $filters_array = array();
	
    function __construct ()
    {
        parent::__construct ();
		$CI =& get_instance();
		$CI->db = Auth::loadDB();
        $this->encodeUTF8 = true;
        ini_set ('always_populate_raw_post_data', true);
        $this->success = TRUE;
        $this->data = array();
        $this->children = array();
        $this->results = -1;
        $this->msg = "";
        if (isset($GLOBALS['HTTP_RAW_POST_DATA'] ))
        {
            $this->_post_data = json_decode ($GLOBALS['HTTP_RAW_POST_DATA'] , true);
            $this->_post_data = $this->_post_data['data'];
        }
		$this->sort = ($this->get('sort')) ?  json_decode($this->get('sort'),true) : array();
		foreach($this->sort as $i) {
			$this->sort_array[$i['property']] = $CI->db->escape_str($i['direction']);
		}
		$this->filters = ($this->get('filter')) ? json_decode($this->get('filter'),true) : array();
		foreach($this->filters as $i) {
			if(isset($i['value'])) $this->filters_array[$i['property']] = $CI->db->escape_str($i['value']);
		}
        log_message ('debug', "Ajax Class Initialized");
    }
    
	public function post_data($key = "") {
		if(!$key) return $this->_post_data;
		return (isset($this->_post_data[$key])) ? $this->_post_data[$key] : FALSE;
	}
	
    public function json ()
    {
		header('Content-Type: application/json');
		$this->results = ($this->results > -1) ? $this->results : count($this->data);
        $out = array('success'=>$this->success, 'data'=>$this->data,  'children'=>$this->children, 'results'=>$this->results, 'msg'=>$this->msg);
        echo($this->encodeUTF8) ? utf8_encode (json_encode ($out)) : json_encode ($out);
    }
    
    public function html ($out = array())
    {
    	header('Content-type: text/html');
        echo($this->encodeUTF8) ? utf8_encode ($out) : $out;
    }
	
	public function query($table = '', $select = '', $joins = array(), $joinType = "inner", $group_by = "") {
		if(!$table) return FALSE;
		$CI =& get_instance();
		foreach($this->filters_array as $k => $v) $CI->db->where($k, $v);
		foreach($joins as $k => $v) $CI->db->join($k, $v, $joinType);
		$this->results = $CI->db->count_all_results($table);
		if($select) $CI->db->select($select);
		foreach($joins as $k => $v) $CI->db->join($k, $v, $joinType);
		foreach($this->filters_array as $k => $v) $CI->db->where($k, $v);
		foreach($this->sort_array as $k => $v) $CI->db->order_by($k,$v);
		if($group_by) $CI->db->group_by($group_by); 
		if($this->get('limit')) $CI->db->limit($this->get('limit'),$this->get('start'));
		$res = $CI->db->get($table);
		return $res;		
	}
	
	public function set_delete($table = "", $keyField = "") {
		if($uid = $this->post_data($keyField)) {
			$CI =& get_instance();
			$CI->db->where($keyField,$uid);
			$CI->db->update($table, array('deleted'=>1));
			$this->results = $CI->db->affected_rows();
			$this->json();
		}
	}

	public function delete($table = "", $keyField = "", $where = "") {
		if($uid = $this->post_data($keyField)) {
			$CI =& get_instance();
			$CI->db->where($keyField, $uid);
			if(is_array($where))
				foreach($where as $k => $v) $CI->db->where($k, $v);
			$CI->db->delete($table);
			$this->results = $CI->db->affected_rows();
			$this->json();
		}
	}
	
	public function getUid(){
		$CI =& get_instance();
		$res = $CI->db->query("SELECT UUID() as uid");
		$rec =  $res->row();
		return $rec->uid;	
	}

	public function foundRows($intoResults=TRUE) {
		$CI =& get_instance();
		$fr = $CI->db->query("SELECT FOUND_ROWS() as total");
		$fr = $fr->row();
		if($intoResults) $this->results = $fr->total;
		return $fr->total;		
	}

}
// END Ajax Class
