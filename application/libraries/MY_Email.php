<?php

class MY_Email extends CI_Email {

    public function __construct()
    {
        parent::__construct();
		$this->emailConfig();
     }

	public function emailConfig() {
		 $_config['protocol']= (Auth::getConfig('mailing_smtp')) ? 'smtp' : 'mail';
		 $_config['smtp_host']= Auth::getConfig('mailing_smtp_host');
		 $_config['smtp_port']=Auth::getConfig('mailing_smtp_port');
		 $_config['smtp_timeout']='90';
		 $_config['smtp_user']=Auth::getConfig('mailing_smtp_user');
		 $_config['smtp_pass'] = Auth::getConfig('mailing_smtp_password');
		 $_config['_smtp_auth']  =  Auth::getConfig('mailing_smtp_auth');
		 $_config['mailtype']='html';
		 $_config['charset']='utf-8';
		 $_config['newline']="\r\n";
		 $_config['crlf'] = "\r\n"; 
		$this->initialize($_config);
		$this->from(Auth::getConfig('mailing_from'));
		$this->reply_to(Auth::getConfig('mailing_reply_to'));
		$this->set_header('Return-Path', Auth::getConfig('mailing_reply_to'));				
		$this->set_header('User-Agent', 'CMS');				
		$this->set_header('X-Mailer', 'CMS');				
		return $_config;		
	}

    public function set_header($header, $value){
        $this->_headers[$header] = $value;
    }
}

