<?php
class MY_Smarty extends CI_Smarty {
    
	var $templateJS = array();
	
    function __construct ()
    {
        parent::__construct ();
		$CI = &get_instance ();
        $CI->load->helper ('url');
        $configFile = APPPATH."config/smarty.conf";
        $this->caching = false;
        if (file_exists ($configFile))
        {
            $this->configLoad ($configFile);
        }
		$CI->load->library('auth');
		$_client = $CI->auth->getClient();
		$this->template_dir = APPPATH . "_clients/$_client/templates";        
		$this->assign ('_lang', $CI->lang->language);
        $consts = get_defined_constants (TRUE);
        $consts['user']['BASEURL'] = base_url ();
		foreach ($consts['user'] as $key => $value)		
        	$this->assign ($key, $value);
        $this->registerPlugin ("function", "loadJS", array($this, "loadJS"));
        $this->registerPlugin ("function", "loadCSS", array($this, "loadCSS"));
        $this->registerPlugin ("function", "loadLang", array($this, "loadLang"));
        $this->registerPlugin ("function", "loadConstants", array($this, "loadConstants"));
        $this->registerPlugin ("function", "loadData", array($this, "loadData"));
        $this->registerPlugin ("function", "templateJS", array($this, "templateJS"));
        $this->registerPlugin ("function", "loadTemplateJS", array($this, "loadTemplateJS"));
        $this->registerPlugin ("function", "act", array($this, "act"));
        $this->registerPlugin ("function", "URL", array($this, "URL"));
        $this->registerPlugin ("function", "segment", array($this, "segment"));
        $this->registerPlugin ("function", "QS", array($this, "QS"));
        $this->registerPlugin ("function", "image", array($this, "image"));
        $this->registerPlugin ("modifier", "strip_selected_tags", array($this, "strip_selected_tags"));
        $this->registerPlugin ("modifier", "strip_p", array($this, "strip_p"));
    }
	
	function loadData($params, $smrty) {
		echo "<script>";
		echo "var _data = ".json_encode($this->getTemplateVars('_data'));
		echo "</script>";
	}	

	function templateJS($params, $smarty) {
        if (isset($params["src"]) && ! empty($params["src"])) {		
			$version = defined ('VERSION') ? constant ('VERSION') : "";
			$src = $params['src'];
			$this->templateJS[] = "<script src=\"$src?$version\"></script>";
		}
	}

	function loadTemplateJS($params, $smarty) {
		return implode("\n",$this->templateJS);
	}

    
    /**
     * Smarty plugin: Loads a javascript file
     *
     * @access	public
     * @param	array
     * @param	object
     * @return	string
     */
    function loadJS ($params, $smarty)
    {
        $CI = &get_instance ();
        $CI->load->helper ('url');
        $version = defined ('VERSION') ? constant ('VERSION') : "";
        
        if (isset($params["src"]) && ! empty($params["src"]))
        {
            $src = defined ('JS_PATH') ? base_url ().JS_PATH."/".$params["src"] : $params["src"];
            return (isset($params['ie'])) ? "<!--[if lt IE 9]><script src=\"$src?$version\"></script><![endif]-->" : "<script src=\"$src?$version\"></script>";
        } else
        {
            $result = "";
            $uri = explode ('/', uri_string ());
            $controller = ($uri && isset($uri[0]) && $uri[0]) ? $uri[0] : constant ('DEFAULT_CONTROLLER');
            $method = ($uri && isset($uri[1]) && $uri[1]) ? $uri[1] : 'index';
            if ($controller)
            {
                $src = defined ('JS_PATH') ? base_url ().JS_PATH."/".JS_CONTROLLERS_PATH."/$controller/$controller.js" : FALSE;
                if ($src)
                {
                    $hdrs = @get_headers ($src);
                    if ($hdrs[0] != 'HTTP/1.1 404 Not Found')
                        $result .= "<script src=\"$src?$version\"></script>";
                }
            }
            if ($method)
            {
                $src = defined ('JS_PATH') ? base_url ().JS_PATH."/".JS_CONTROLLERS_PATH."/$controller/$method.js" : FALSE;
                if ($src)
                {
                    $hdrs = @get_headers ($src);
                    if ($hdrs[0] != 'HTTP/1.1 404 Not Found')
                        $result .= "<script src=\"$src?$version\"></script>";
                }
            }
            return $result;
        }
        return false;
    }
    
    /**
     * Smarty plugin: Loads a css styles file
     *
     * @access	public
     * @param	array
     * @param	object
     * @return	string
     */
    function loadCSS ($params, $smarty)
    {
        $CI = &get_instance ();
        $CI->load->helper ('url');
        $version = defined ('VERSION') ? constant ('VERSION') : "";
        if (! empty($params["src"]))
        {
            $src = defined ('CSS_PATH') ? base_url ().CSS_PATH."/".$params["src"] : $params["src"];
            $media = (isset($params['media'])) ? $params['media'] : 'screen';
            return "<link rel=\"stylesheet\" type=\"text/css\" media=\"$media\" href=\"$src?$version\" />";
        }
        return false;
    }
    
    /**
     * Smarty plugin: Loads a js lang file
     *
     * @access	public
     * @param	array
     * @param	object
     * @return	string
     */
    function loadLang ($params, $smarty)
    {
        $CI = &get_instance ();
        $CI->load->helper ('url');
        $uri = explode ('/', uri_string ());
        if ($uri && $uri[0])
        {
            $controller = $uri[0];
        } else
        {
            $controller = constant ('DEFAULT_CONTROLLER');
        }
        $src = base_url ()."backend/js/lang/$controller";
        return "<script src=\"$src\" ></script>";
    }
    
    /**
     * Smarty plugin: Loads php constants to JS
     *
     * @access	public
     * @param	array
     * @param	object
     * @return	string
     */
    function loadConstants ($params, $smarty)
    {
        $CI = &get_instance ();
        $CI->load->helper ('url');
        $src = base_url ()."backend/js/constants";
        return "<script src=\"$src\" ></script>";
    }

	function act($params, $smarty){
		$CI = &get_instance ();
		if(!isset($params['l'])) {
			//$res = strpos($CI->uri->uri_string(), $params['s']) !== FALSE;
			$res = FALSE;
			for($i=0;$i<=$CI->uri->total_segments();$i++){
				if($CI->uri->segment($i)==$params['s']) $res = TRUE;
			}
			if($res) return (isset($params['class'])) ? $params['class'] : "active";
			return ""; 
		}
		if ($CI->uri->segment($params['l']) == $params['s'])
			return (isset($params['class'])) ? $params['class'] : "active";
	}

	function URL($params, $smarty){
		$CI = &get_instance ();
		$result = $CI->config->item('base_url')."/";
		if(!isset($params['n'])) return $result;
		for($i=1;$i<=(int)$params['n'];$i++)
			$result .= $CI->uri->slash_segment($i);
		return $result;
	}

	function segment($params, $smarty){
		$CI = &get_instance ();
		$result = $CI->config->item('base_url')."/";
		if(!isset($params['n'])) return $result;
		$result = $CI->uri->segment((int)$params['n']);
		return $result;
	}

	function image($params, $smarty){
		$CI = &get_instance ();
		if(!isset($params['text'])) return;
		$string = $params['text'];
		$font = 'assets/_clients/syria/css/fonts/bryantlg-regular-webfont.ttf';
		//$fp = imageloadfont($font);
		$width  = imagefontwidth(4) * strlen($string);
		$height = imagefontheight(4);
		$CI->load->library('SimpleImage');
		$img = new SimpleImage(null, $width, $height, '#FFF');
		$img->text($string, $font, 12, '#98999c', 'top', 0, 0);
		
		return $img->output_base64('png');
	}
	
	function QS($params, $smarty){
		$CI = &get_instance ();
		$result = $CI->config->item('base_url')."/".$CI->uri->uri_string();
		$qs = $CI->input->get();
		if(!isset($params['key'])) return $result;
		$k = $params['key'];
		$v = $params['value'];
		$qs[$k] = $v;
		return implode('?', array($result,http_build_query($qs))); 
//		return http_build_query($qs); 
//		return $result;
	}
	
	/* MODIFIERS */
    function strip_p($text){
    	return $this->strip_selected_tags($text, "p,br");	
    }
	
    function strip_selected_tags($text, $tags = "")
    {
    	$tags = explode(',', $tags);
        $args = func_get_args();
        $text = array_shift($args);
        $tags = func_num_args() > 2 ? array_diff($args,array($text))  : (array)$tags;
        foreach ($tags as $tag){
            if(preg_match_all('/<'.$tag.'[^>]*>(.*)<\/'.$tag.'>/iU', $text, $found)){
                $text = str_replace($found[0],$found[1],$text);
          }
        }

        return $text;
    }	
}
// END MY_Smarty Class
