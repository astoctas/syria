<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Auth {
	
	
	function __construct() {
	}

	public static function getLang() {
		$CI =& get_instance();
		$user = $CI->session->userdata('lang');
		return $user;		
	}

	public static function setLang($lang) {
		$CI =& get_instance();
		$CI->session->set_userdata('lang', $lang);
		//setcookie('_lang', $lang);
		return true;
	}

	public static function getUser() {
		$CI =& get_instance();
		$user = $CI->session->userdata('user');
		return $user;		
	}
	
	public static function getUserAdmin() {
		$CI =& get_instance();
		$user = $CI->session->userdata('admin');
		return $user;		
	}
	
	public static function checkLogin($kind = 'user') {
		if($kind =='admin' && !Auth::getUserAdmin()) {
			show_404();
			exit;
		}
		if($kind =='user' && !Auth::getUser()) {
			show_404();
			exit;
		}
	}
	
	public static function setTab($tab) {
		$_SESSION['_tab'] = $tab;
		return $tab;
	}
	
	public static function getTab() {
		if(isset($_SESSION['_tab']) && !empty($_SESSION['_tab'])) 
			return $_SESSION['_tab'];
		else return FALSE;		
	}
	
	public static function getClient() {
		if(isset($_SESSION['_client']) && !empty($_SESSION['_client'])) 
			return $_SESSION['_client'];
		else return FALSE;		
	}
	
	public static function loadDB() {
		$CI =& get_instance();
		$_client = $_SESSION['_client'];
		if(!empty($_client)) {
			$dbConfig = APPPATH."_clients/$_client/config/database.php";
			require_once $dbConfig;
			$CI->load->database($db[$active_group]);
		} else 
			$CI->load->database();
		return $CI->db;
	}	
	
	public static function saveConfig($conf = array()) {
		$_SESSION['site_config'] = $conf;	
	}
	
	public static function getConfig($option = "") {
		if(empty($option))
			return isset($_SESSION['site_config']) ? $_SESSION['site_config'] : array();
		return isset($_SESSION['site_config']) ? $_SESSION['site_config'][$option] : "";
	}
	
}
/* End of file Auth.php */