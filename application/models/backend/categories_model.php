<?php

class Categories_model extends CI_Model {
	
	var $table = 'categories';
	var $keyField = 'uid';
	
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function getTree($node = FALSE, $checked = FALSE) {
		$data = array();
		$isRoot = ($node=='root');
		
		function recursive($id, $results, $checked) {
			$nodes = array();
			foreach($results as $r){
				if($r['parent']==$id) {
					$node = $r;
					if(is_array($checked))
						$node['checked'] = in_array($r['uid'], $checked);
					$node['children'] = recursive($r['uid'], $results, $checked);
					$nodes[] = $node;
				}
			}
			return $nodes; 
		}
		$whereNode = ($isRoot)  ? "" : " AND parent = '$node' ";
		$query = "SELECT st.name as text, st.uid, st.parent,  st.`schema`, st.`order` as 'index' FROM $this->table st
			LEFT JOIN `schemas` sc ON st.schema = sc.uid 
			WHERE 1 
			ORDER BY st.`order`ASC
		";
		$values = array($node);
		$res = $this->db->query($query,$values);
//		if($isRoot) {
			if($res->num_rows()) {
				$results = $res->result_array();
				$data = recursive($node,$results, $checked);			
				return $data;
			} else return array();
//		} else return ($res) ? $res->result_array() : FALSE;
	}

	public function load() {
		$this->load->library('ajax');
		$data = array();
		$uid  = $this->db->escape($this->ajax->post('uid'));
		$query = "
			(SELECT d.data, d.field FROM data_varchar d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_text d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_date d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_numeric d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_uid d WHERE d.uid = $uid)
			UNION	(SELECT name, 'name' as field FROM categories c WHERE c.uid = $uid)
			UNION	(SELECT `schema`, 'schema' as field FROM categories c WHERE c.uid = $uid)
		";
		$res = $this->db->query($query);
		if ($res->num_rows()) {
			foreach($res->result_array() as $r) {
				$data[$r['field']] = $r['data'];
			}
		}
		return $data;
	}

	public function edit($post = array()) {
		if(!$post) return FALSE;
		$this->load->model('backend/datastore_model');
		$this->uid = ($post['uid']) ? $post['uid'] : $this->ajax->getUid();
		// SAVE DATA
		$this->datastore_model->saveData($this->uid, $post);
		// INSERT RECORD IN struct
		$this->load->library('auth');
		$user = $this->auth->getUser();
		$query = "INSERT INTO `$this->table` (`uid`,`name`, `schema`, `active`) VALUES ('{$this->uid}','{$post['name']}', '{$post['schema']}', 1)
			ON DUPLICATE KEY UPDATE `name`=VALUES(name), `schema`=VALUES(`schema`)";
		$this->db->query($query);
		return $this->db->affected_rows();
	}
	
	
	public  function update($data){
		if(!array_key_exists(0,$data)) $data = array($data);
		$result = array();
		$query = "INSERT INTO $this->table (`uid`,`name`,`parent`,`active`, `order`, `level`) 
		VALUES (?,?,?,?,?,?) 
		ON DUPLICATE KEY UPDATE name=VALUES(name), parent=VALUES(parent), active=VALUES(active), `order`=VALUES(`level`), `level`=VALUES(`level`)";
		foreach($data as $d){
			$values = array($d['id'],$d['text'],$d['parentId'],1, $d['index'], $d['depth']);
			$this->db->query($query, $values);
			//$res = $this->db->get_where($this->table, array('uid'=>$d['id']));
			$result[] = $d;//$res->row_array();
		}
		return $result;
	}
		
	public function delete($data) {
		$this->db->delete($this->table, array('uid'=>$data['uid']));
		return $this->db->affected_rows();
	}
	
	
}