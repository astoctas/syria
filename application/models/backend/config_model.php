<?php

class Config_model extends CI_Model {

	var $table = "config";
	var $keyField = 'key';
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function get($key=""){
		if(!$key) return $this->load();
		$this->db->where('key', $key);
		$res = $this->db->get($this->table)->row_array();
		return $res['value'];
	}

	public function load(){
		$result = array();
		$res = $this->db->get($this->table)->result_array();
		if($res)
		foreach($res as $rec) {
			$result[$rec['key']] = $rec['value'];
		}
		Auth::saveConfig($result);
		return $result;
	}
	
	public  function update($data){
		if(!$data) return FALSE;
		$query = "INSERT INTO $this->table (`key`,`value`) 
		VALUES (?,?) 
		ON DUPLICATE KEY UPDATE value=VALUES(value)";
		foreach($data as $key => $value){
			$values = array($key,$value);
			$this->db->query($query, $values);
			$result[$key] = $value;
		}
		Auth::saveConfig($result);
		return TRUE;
	}	

	
}