<?php

class Contents_model extends CI_Model {

	var $table = 'contents';
	var $keyField = 'uid';

    function __construct()
    {
        parent::__construct();
    }

	public function get() {
		$this->load->library('ajax');
		$where = (!empty($this->ajax->filters) && $s = $this->ajax->filters[0]) ? " WHERE `{$s['property']}` = '{$s['value']}'" : "";
		$sort = (!empty($this->ajax->sort) && $s = $this->ajax->sort[0]) ?  "ORDER BY st.`{$s['property']}`  {$s['direction']}" : "";
		/*
		$query = "SELECT dv.data as name, st.uid, st.create_date, sc.uid as `schema` FROM `contents_live` st
			INNER JOIN `objects` o ON st.object = o.uid
			INNER JOIN `schemas` sc ON o.schema = sc.uid
			INNER JOIN `fields` f ON f.schema_uid = sc.uid AND label = 1
			INNER JOIN data_varchar dv ON dv.field = f.uid
				AND (f.datatype = 'text' OR f.datatype = 'email') AND st.uid = dv.uid
			$where $sort";
		*/
		$start = $this->ajax->get('start');
		$limit = $this->ajax->get('limit');
		$limit = "LIMIT $start,$limit";
		$query = "SELECT SQL_CALC_FOUND_ROWS st.*,  o.`schema` as `schema` FROM contents_live st
					INNER JOIN `objects` o ON st.object = o.uid
					$where $sort $limit";
		$res = $this->db->query($query);
		$this->ajax->foundRows();
		return ($res->num_rows()) ? $res->result_array() : FALSE;
	}

	public function search() {
		$query = $this->ajax->filters_array['query'];
		$object = $this->ajax->filters_array['object'];
		$where = "WHERE `data` LIKE '%$query%' AND st.object = '$object'";
		$nameWhere = "WHERE `name` LIKE '%$query%' AND st.object = '$object'";
		$sort = (!empty($this->ajax->sort) && $s = $this->ajax->sort[0]) ?  "ORDER BY st.`{$s['property']}`  {$s['direction']}" : "";
		$q = "SELECT SQL_CALC_FOUND_ROWS st.*, o.uid as `schema` FROM (
				SELECT st.* FROM `contents_live` st
				$nameWhere
				UNION
				SELECT st.* FROM `contents_live` st
				INNER JOIN data_varchar dv ON dv.uid = st.uid
				$where
				UNION
				SELECT st.* FROM `contents_live` st
				INNER JOIN data_text dv ON dv.uid = st.uid
				$where
				) as st
			INNER JOIN `objects` o ON st.object = o.uid
			$sort";
		$res = $this->db->query($q);
		$this->ajax->foundRows();
		return ($res->num_rows()) ? $res->result_array() : FALSE;

	}

	public function load() {
		$this->load->library('ajax');
		$this->load->library('slug');
		$data = array();
		$relData = array();
		$uid  = $this->db->escape($this->ajax->post('uid'));
		// TRAIGO LOS CAMPOS
		$query = "SELECT *
				FROM `fields` f
				INNER JOIN `schemas` s ON s.uid = f.schema_uid
				INNER JOIN `objects` o ON o.`schema` = s.uid
				INNER JOIN `contents` c ON c.object = o.uid
				WHERE c.uid = $uid
				ORDER BY f.`order`";
		$res = $this->db->query($query);
		foreach($res->result_array() as $field) {
			$data[$field['uid']] = FALSE;
		}
		$query = "
			SELECT data.*, f.datatype FROM (
			(SELECT dv.data, dv.field, 0 as 'order' FROM data_varchar dv WHERE dv.uid = $uid)
			UNION	(SELECT dt.data, dt.field, 0 as 'order' FROM data_text dt WHERE dt.uid = $uid)
			UNION	(SELECT dd.data, dd.field, 0 as 'order' FROM data_date dd WHERE dd.uid = $uid)
			UNION	(SELECT di.data, di.field, 0 as 'order' FROM data_time di WHERE di.uid = $uid)
			UNION	(SELECT dn.data, dn.field, 0 as 'order' FROM data_numeric dn WHERE dn.uid = $uid)
			UNION	(SELECT du.data, du.field, `order` as 'order' FROM data_uid du WHERE du.uid = $uid)
			UNION	(SELECT name, 'name' as field, 0 as 'order' FROM contents c WHERE c.uid = $uid)
			UNION	(SELECT create_date, 'create_date' as field, 0 as 'order' FROM contents c WHERE c.uid = $uid)
			UNION	(SELECT id, 'id' as field, 0 as 'order' FROM contents c WHERE c.uid = $uid)
			) as data
			LEFT JOIN fields f ON data.field = f.uid
			ORDER BY f.`order`, data.`order`";
		$res = $this->db->query($query);
		if ($res->num_rows()) {
			foreach($res->result_array() as $r) {
				switch($r['datatype']) {
					case 'contents':
						$rel = $this->db->get_where('contents', array('uid'=>$r['data']))->row_array();
						if($rel) $relData[$r['field']][] = $rel;
					break;
					case 'pages':
					case 'category':
						$relData[$r['field']][] = $r['data'];
					break;
					case 'checkgroup':
					case 'radiogroup':
						$data[$r['field'].'[]'] = json_decode($r['data'],true);
					break;
					default:
						$data[$r['field']] = $r['data'];
				}
			}
			foreach($relData as $field => $values)
				$data[$field] = json_encode($values,JSON_NUMERIC_CHECK);
		}
		$data['url'] = $data['id']."-".$this->slug->slugify($data['name']);
		return $data;
	}

	public function get_files() {
		$uid = $this->input->get('uid');
		$fieldUid = $this->input->get('field');
		if(!$uid || !$fieldUid) return FALSE;
		$query = "SELECT f.*, du.`order` FROM data_uid du
			INNER JOIN filebank_live f ON f.uid = du.data
			WHERE du.uid = ? AND du.`field`= ?
			ORDER BY du.`order` ASC";
		$values = array($uid,$fieldUid);
		$res = $this->db->query($query, $values);
		return ($res->num_rows()) ? $res->result_array() : array();
	}

	public function edit($post = array()) {
		if(!$post) return FALSE;
		$this->load->model('backend/datastore_model');
		$this->uid = ($post['uid']) ? $post['uid'] : $this->ajax->getUid();
		// SAVE DATA
		$this->datastore_model->saveData($this->uid, $post);
		// INSERT RECORD IN contents
		$this->load->library('auth');
		$user = $this->auth->getUserAdmin();
		$data = array('uid'=>$this->uid, 'object'=>$post['object'], 'active'=>1, 'user'=>$user->uid);
		$query = "INSERT INTO `$this->table` (`uid`,`name`,`object`,`active`, `user`, `create_date`) VALUES ('{$this->uid}','{$post['name']}','{$post['object']}', 1, '{$user->uid}', '{$post['create_date']}')
			ON DUPLICATE KEY UPDATE `name`=VALUES(name),`create_date`=VALUES(create_date), `mod_user`=VALUES(user), `mod_date`=NOW()";
		$this->db->query($query);
		return $this->db->affected_rows();
	}

	public function toggleField($uid = "", $field = "active") {
		$query = "UPDATE $this->table SET `$field` = NOT `$field` WHERE `$this->keyField` = '$uid' ";
		$this->db->query($query);
		return $this->db->affected_rows();
	}

}