<?php

class Datastore_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }


	public function saveData($uid = "", $post = array()) {
		$res = $this->db->get_where('fields', array('schema_uid'=>$post['schema']));
		$fields = $res->result_array();
		foreach($fields as $f)
		{
			if(isset($post[$f['uid']]))
			{
				//var_dump($f['uid'], strlen($post[$f['uid']]), $post[$f['uid']]);
				$this->setDatastore($f['datatype']);
				$this->field = $f['uid'];
				if(is_array($post[$f['uid']])) $post[$f['uid']] = json_encode($post[$f['uid']]);
				$this->data = strlen($post[$f['uid']]) ? $post[$f['uid']] : "";
				//var_dump($this->field,$this->datatable, $this->data);
				if(!( empty($this->field) || empty($uid) || empty($this->datatable) ))
				{
				    switch($this->datatable) {
				    case 'data_uid':
						$_data = $this->data;
						$this->data = json_decode($_data, true);
						if(!is_array($this->data)) $this->data = array(array('uid'=>$_data));
						if(is_array($this->data)){
							$query = "DELETE  FROM {$this->datatable} WHERE `uid` = ? AND `field` = ?";
							$values = array($uid,$this->field);
							$this->db->query($query, $values);
							$order = 0;
							foreach($this->data as $data) {
								$query = "INSERT INTO {$this->datatable} (`dataset_uid`,`uid`,`field`,`data`,`create_date`,`order`)
									VALUES (UUID(), ?, ?, ?, NOW(), $order)";
								$values = array($uid, $this->field, $data['uid']);
								$this->db->query($query, $values);
								$order++;
							}
						}
			        break;
				    default:
						$query = "INSERT INTO `$this->datatable` (`uid`,`field`,`data`) VALUES ('{$uid}','{$this->field}',?)
							ON DUPLICATE KEY UPDATE `data`=VALUES(data)";
                          $values = array($this->data);
						//echo $query;
						$this->db->query($query, $values);
				        break;
				    }
				}
			}
		}
		return TRUE;
	}

   public function setDatastore($string) {
	switch($string) {
	    case 'number':
	    case 'checkbox':
			$this->datatable = 'data_numeric';
		break;
	    case 'text':
        case 'email':
        case 'url':
			$this->datatable = 'data_varchar';
		break;
	    case 'textarea':
	    case 'extra':
	    case 'keyvalue':
	    case 'checkgroup':
	    case 'radiogroup':
			$this->datatable = 'data_text';
		break;
        case 'date':
			$this->datatable = 'data_date';
		break;
        case 'time':
			$this->datatable = 'data_time';
		break;
	    case 'radio':
	    case 'select':
	    case 'contents':
	    case 'pages':
	    case 'category':
	    case 'file':
			$this->datatable = 'data_uid';
		break;
	    default:
			$this->datatable = '';
	}
	return $this->datatable;
    }

}