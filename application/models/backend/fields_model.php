<?php

class Fields_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	
	public function update($data = FALSE) {
		if(!$data) return FALSE;
		if(!$data['uid']) {
			$data['uid'] = $this->ajax->getUid();
		}
		$query = "INSERT INTO `fields`(`uid`,`schema_uid`,`name`,`tab`, `datatype`,`vtype`,`required`, `default_value`, `order`, `options`, `show_other`, `help`) 
		VALUES (?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?)
		ON DUPLICATE KEY UPDATE name=VALUES(name), tab=VALUES(tab), vtype=VALUES(vtype), required=VALUES(required), default_value=VALUES(default_value), `order`=VALUES(`order`), `options`=VALUES(`options`), `show_other`=VALUES(`show_other`), `help`=VALUES(`help`) ";
		$values = array($data['uid'],$data['schema_uid'],$data['name'],$data['datatype'],$data['vtype'],$data['required'],$data['default_value']. $data['order'], $data['options'], $data['show_other'], $data['help']);
		$this->db->query($query,$values);
		if($this->db->affected_rows()) {
			$query = "SELECT * FROM `fields` WHERE uid = ?";
			$values = array($data['uid']);
			$res = $this->db->query($query,$values);
			return ($res) ? $res->row_array() : FALSE;
		} else return FALSE;
	}
	
	
}