<?php

class Filebank_category_model extends CI_Model {
	
	var $data = array();
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function getTree($node = FALSE) {
		$data = array();
		$isRoot = ($node=='root');
		
		function recursive($id, $results) {
			$nodes = array();
			foreach($results as $r){
				if($r['parent']==$id) {
					$node = $r;
					$node['children'] = recursive($r['id'], $results);
					$nodes[] = $node;
				}
			}
			return $nodes; 
		}
		$whereNode = ($isRoot)  ? "" : " parent = ? AND ";
		
		$query = "SELECT uid, uid as id, name as text, parent, `order` as `index` FROM filebank_category fc
			WHERE $whereNode active = 1 
			ORDER BY `name` ASC";
		$values = array($node);
		$res = $this->db->query($query,$values);
		if($isRoot) {
			if($res->num_rows()) {
				$results = $res->result_array();
				$data = recursive('root',$results);			
				return $data;
			} else return array();
		} else return ($res) ? $res->result_array() : FALSE;

	}

	
	public  function update($data){
		if(!array_key_exists(0,$data)) $data = array($data);
		$result = array();
		$query = "INSERT INTO filebank_category (`uid`,`name`,`parent`,`active`, `order`) 
		VALUES (?,?,?,?, ?) 
		ON DUPLICATE KEY UPDATE name=VALUES(name), parent=VALUES(parent), active=VALUES(active), `order`=VALUES(`order`)";
		foreach($data as $d){
			$values = array($d['id'],$d['text'],$d['parentId'],1, $d['index']);
			$this->db->query($query, $values);
			$result[] = $d;
		}
		return $result;
	}
	
	public function delete($data) {
		$query = "UPDATE filebank SET category_uid = '0' WHERE category_uid = ?";
		$values = array($data['uid']);
		$this->db->query($query, $values);
		$this->db->delete('filebank_category', array('uid'=>$data['uid']));
		return $this->db->affected_rows();
	}
	
}

