<?php
class Filebank_model extends CI_Model {
    
    var $mime_icon = array('image'=>array('image/*'), 'audio'=>array('audio/*'=>'ftype_audio.png', 'audio-player/*'=>'ftype_audio_player.png'), 'video'=>array('default'=>'ftype_video.png', 'application/octet-stream'=>'ftype_video.png'), 'doc'=>array('default'=>'ftype_txt.png', 'application/x-zip'=>'ftype_zip.png', 'application/pdf'=>'ftype_pdf.png', 'application/rtf'=>'ftype_rtf.png', 'application/macwriteii'=>'ftype_macwrite.png', 'application/msword'=>'ftype_word.png', 'application/mspowerpoint'=>'ftype_powerpoint.png', 'application/ms-powerpoint'=>'ftype_powerpoint.png', 'application/vnd.ms-powerpoint'=>'ftype_powerpoint.png', 'application/vnd.ms-excel'=>'ftype_excel.png', 'application/x-msexcel'=>'ftype_excel.png', 'application/ms-excel'=>'ftype_excel.png', 'application/vnd.ms-project'=>'ftype_txt.png', 'application/vnd.sun.xml.writer'=>'ftype_word.png', 'application/vnd.sun.xml.writer.template'=>'ftype_word.png', 'application/vnd.sun.xml.writer.global'=>'ftype_word.png', 'application/vnd.sun.xml.calc'=>'ftype_excel.png', 'application/vnd.sun.xml.calc.template'=>'ftype_excel.png', 'application/vnd.sun.xml.impress'=>'ftype_powerpoint.png', 'application/vnd.sun.xml.impress.template'=>'ftype_powerpoint.png', 'application/vnd.sun.xml.draw'=>'ftype_powerpoint.png', 'application/vnd.sun.xml.draw.template'=>'ftype_powerpoint.png', 'application/vnd.sun.xml.math'=>'ftype_excel.png', 'application/vnd.openofficeorg.extension'=>'ftype_txt.png'), 'flash'=>array('default'=>'ftype_flash.png', 'application/x-shockwave-flash'=>'ftype_flash.png'), 'other'=>array('other'=>'ftype_file.png'));
    
    function __construct ()
    {
        parent::__construct ();
    }
    
	public function query($query) {
		$query = $this->db->escape_str($query);
		$this->db->like('filename', $query);
		$this->db->or_like('caption', $query);
		$res = $this->db->get('filebank_live');
		return $res;		 				
	}
	
	public function edit($data) {
		$query = "INSERT INTO filebank (`uid`,`category_uid`,`filetype`,`filename`,`ext`,`size`,`mime`,`width`,`height`,`active`,`caption`,`modified_time`,`hash`)
		VALUES (?,?,?,?,?,?,?,?,?, 1, ?, NOW(), ?)
		ON DUPLICATE KEY UPDATE category_uid=VALUES(category_uid), filetype=VALUES(filetype), filename=VALUES(filename), ext=VALUES(ext), size=VALUES(size), mime=VALUES(mime), width=VALUES(width), height=VALUES(height), caption=VALUES(caption), modified_time=NOW(), hash=VALUES(hash) ";
		$values = array($data['uid'],$data['category_uid'],$data['filetype'],$data['file_name'],$data['file_ext'],$data['size'],$data['file_type'],$data['image_width'],$data['image_height'],$data['caption'],$data['hash']);
		$this->db->query($query,$values);
		return $this->db->affected_rows();
	}

	public function update($data) {
		$query = "UPDATE filebank SET caption = ? WHERE uid = ? ";
		$values = array($data['caption'], $data['uid']);
		$this->db->query($query,$values);
		return $this->db->affected_rows();
	}

	public function getSizes($uid) {
		$query = "SELECT * FROM filebank_sizes s";
		$query .= ($uid) ? " LEFT JOIN filebank_crops c ON c.filebank_size_uid = s.uid AND filebank_uid = '$uid'" : "";
		return $this->db->query($query)->result_array();
	}
	
	public function saveCrop($data) {
		$query = "INSERT INTO filebank_crops (`filebank_uid`, `filebank_size_uid`, `data`) VALUES (?,?,?) 
			ON DUPLICATE KEY UPDATE data=VALUES(data)";
		$values = array($data['filebank_uid'], $data['filebank_size_uid'], $data['data']);
		$this->db->query($query,$values);
		return $this->db->affected_rows();
	}
	
    /**
     * Set width
     *
     * Sets the {@link FileBank::$width} property for the file
     * @param	string width in pixels
     */
    function setWidth ($string)
    {
        $this->width = $string;
    }
    
    /**
     * Set height
     *
     * Sets the {@link FileBank::$height} property for the file
     * @param	string width in pixels
     */
    function setHeight ($string)
    {
        $this->height = $string;
    }

    /**
     * Create cache thumb
     *
     * Create a thumbnail of an image
     * @param string source path
     * @param string destination path
     * @param int width in pixels
     * @param int height in pixels
     * @param string hex color used to fill the resized image
     * @return boolean true on success, false otherwise
     */
    public function CreateCacheThumb ($source, $dest, $width = false, $height = false, $fill_color = '#ff0000')
    {
        $retorno = false;
        $iOriginal = false;
        
        /* Obtengo la imagen original y creo la resizada y la final */
        $size = @getimagesize ($source);
        
        if (! $size)
        {
            return false;
        }
        
        // Set image width & height
        $this->setWidth ($size[0]);
        $this->setHeight ($size[1]);
        
        $rwidth = (! $width) ? $size[0] : $width;
        $rheight = (! $height) ? $size[1] : $height;
        
        // Flag para hacer o no la imagen con el fill_color
        $final = true;
        if (! $width || ! $height)
        {
            $final = false;
        }
        switch ($size[2])
        {
            case 1:
                $iOriginal = imagecreatefromgif ($source);
                break;
            case 2:
                $iOriginal = imagecreatefromjpeg ($source);
                break;
            case 3:
                $iOriginal = imagecreatefrompng ($source);
                break;
        }
        
        if ($iOriginal)
        {
            $radio = min (1, $rwidth / $size[0], $rheight / $size[1]);
            $iWidth = $radio * $size[0];
            $iHeight = $radio * $size[1];
            $posX = (! $width) ? 0 : ($width - $iWidth) / 2;
            $posY = (! $height) ? 0 : ($height - $iHeight) / 2;
            
            $iResizada = imagecreatetruecolor ($iWidth, $iHeight);
            $fwidth = (! $width) ? $iWidth : $rwidth;
            $fheight = (! $height) ? $iHeight : $rheight;
            if ($final)
            {
                $iFinal = imagecreatetruecolor ($fwidth, $fheight);
            
            }
            
            /* Resizo, y lleno la imagen Final con el color de fondo */
            //	if ($size[2] == 1 || $size[2] == 3) {
            // Para gif y png preservo la transparencia
            imagecolortransparent ($iResizada, imagecolorallocate ($iResizada, 0, 0, 0));
            imagealphablending ($iResizada, false);
            imagesavealpha ($iResizada, true);
            //	}
            imagecopyresampled ($iResizada, $iOriginal, 0, 0, 0, 0, $iWidth, $iHeight, $size[0], $size[1]);
            
            if ($final)
            {
                // Checkeo color
                /*
                 if (!validateHexColor($fill_color)) {
                 $fill_color = '#ffffff';
                 }
                 */
                $r = hexdec (substr ($fill_color, 1, 2));
                $g = hexdec (substr ($fill_color, 3, 2));
                $b = hexdec (substr ($fill_color, 5, 2));
                $color = imagecolorallocatealpha ($iFinal, $r, $g, $b, 127);
                imagefill ($iFinal, 0, 0, $color);
                imagecopy ($iFinal, $iResizada, $posX, $posY, 0, 0, $iWidth, $iHeight);
                imagecolortransparent ($iFinal, imagecolorallocate ($iFinal, 255, 0, 0));
                imagealphablending ($iFinal, false);
                imagesavealpha ($iFinal, true);
            } else
            {
                $iFinal = $iResizada;
            }

            $retorno = imagepng ($iFinal, $dest);
            //		echo 1; exit;
            if ($final)
            {
                imagedestroy ($iFinal);
            }
            imagedestroy ($iResizada);
        }
        return $retorno;
    }


}
