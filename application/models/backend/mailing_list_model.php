<?php

class Mailing_list_model extends CI_Model {

	var $table = "mailing_lists";    
	var $data = array();
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function getTree($node = FALSE, $checked = FALSE) {
		$data = array();
		$isRoot = ($node=='root');
		
		function recursive($id, $results, $checked = FALSE) {
			$nodes = array();
			foreach($results as $r){
				if($r['parent']==$id) {
					$node = $r;
					$node['children'] = recursive($r['id'], $results);
					if($checked !== FALSE) $node['checked'] = in_array($r['uid'], $checked);
					$nodes[] = $node;
				}
			}
			return $nodes; 
		}
		$whereNode = ($isRoot)  ? "" : " parent = ? AND ";
		
		$query = "SELECT uid, uid as id, name as text, parent, `order` as `index` FROM $this->table fc
			WHERE $whereNode active = 1 
			ORDER BY `name` ASC";
		$values = array($node);
		$res = $this->db->query($query,$values);
		if($isRoot) {
			if($res->num_rows()) {
				$results = $res->result_array();
				$data = recursive('root',$results, $checked);			
				return $data;
			} else return array();
		} else return ($res) ? $res->result_array() : FALSE;

	}

	public function load($data) {
		if(!$data) return FALSE;
		$res = $this->db->get_where($this->table, array('uid'=>$data['uid']))->row_array();
		return $res;
	}
	
	public  function update($data){
		if(!array_key_exists(0,$data)) $data = array($data);
		$result = array();
		$query = "INSERT INTO $this->table (`uid`,`name`,`parent`,`active`, `order`) 
		VALUES (?,?,?,?, ?) 
		ON DUPLICATE KEY UPDATE name=VALUES(name), parent=VALUES(parent), active=VALUES(active), `order`=VALUES(`order`)";
		foreach($data as $d){
			$values = array($d['id'],$d['text'],$d['parentId'],1, $d['index']);
			$this->db->query($query, $values);
			$result[] = $d;
		}
		return $result;
	}
	
	public  function edit($data){
		$query = "INSERT INTO $this->table (`uid`,`name`) 
		VALUES (?,?) 
		ON DUPLICATE KEY UPDATE name=VALUES(name) ";
		$values = array($data['uid'],$data['name']);
		$this->db->query($query, $values);

		// IMPORT SUSCRIBERS USERS
//		$this->db->delete('mailing_lists_suscribers', array('mailing_list_uid'=>$data['uid']));
		$values = array();
		$contents = json_decode($data['users'], true);
		if(is_array($contents)) {
			foreach($contents as $_c){
				$values[] = array('mailing_list_uid'=>$data['uid'], 'user_uid' =>$_c['uid']);
			}
			$this->db->insert_batch('mailing_lists_suscribers', $values);
		}
		
		// IMPORT SUSCRIBERS ADDRESSES
		$mailing_list_uid = $data['uid']; 
		$addresses = preg_split("/[\s,]+/", $data['addresses']);
		foreach($addresses as $addr){
			// TODO: Validar la direcci�n de email
			if(empty($addr)) continue;
			$addr = trim($addr);
			$user_uid = $this->ajax->getUid();
			$query = "INSERT IGNORE user_site (uid, email) VALUES (?,?)";
			$values = array($user_uid, $addr);
			$this->db->query($query, $values);
			$query = "INSERT IGNORE mailing_lists_suscribers (mailing_list_uid, user_uid) VALUES (?,?) ";
			$values = array($mailing_list_uid, $user_uid);
			$this->db->query($query, $values);
		}
		return true;
	}
	
	public function delete($data) {
	//	$query = "UPDATE filebank SET category_uid = '0' WHERE category_uid = ?";
	//	$values = array($data['uid']);
	//	$this->db->query($query, $values);
		$this->db->delete($this->table, array('uid'=>$data['uid']));
		return $this->db->affected_rows();
	}
	
}

