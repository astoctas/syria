<?php

class Mailing_model extends CI_Model {

	var $table = 'mailing';
	var $keyField = 'uid';
		
    function __construct()
    {
        parent::__construct();
    }
	
	
	public function update($data = FALSE) {
		if(!$data) return FALSE;
		if(!$data['uid']) {
			$data['uid'] = $this->ajax->getUid();
		}
		// UPDATE MAILING
		$query = "INSERT INTO `{$this->table}` (`uid`,`name`,`subject`, `reply_to`, `mailing_list`) 
			VALUES (?,?,?,?,?)
			ON DUPLICATE KEY UPDATE name=VALUES(name), `subject`=VALUES(subject), reply_to=VALUES(reply_to), mailing_list=VALUES(mailing_list) ";
		$values = array($data['uid'],$data['name'],$data['subject'],$data['reply_to'],$data['mailing_list']);
		$this->db->query($query,$values);

		// UPDATE NEWSLETTERS
		$this->db->delete('mailing_contents', array('mailing_uid'=>$data['uid']));
		$values = array();
		$contents = json_decode($data['contents'], true);
		foreach($contents as $_c){
			$values[] = array('mailing_uid'=>$data['uid'], 'contents_uid' =>$_c['uid'], 'order'=>$_c['order']);
		}
		$this->db->insert_batch('mailing_contents', $values);
		return true;
	}
	
	public function load($data) {
		if(!$data) return FALSE;
		$res = $this->db->get_where($this->table, array('uid'=>$data['uid']))->row_array();
		$this->db->join('contents c', 'c.uid = mailing_contents.contents_uid');
		$contents = $this->db->get_where('mailing_contents', array('mailing_uid'=>$data['uid']))->result_array();
		$res['contents'] = json_encode($contents);
		if(isset($res['mailing_list']) && !empty($res['mailing_list'])) {
			$ml = array();
			$mlist = json_decode($res['mailing_list'],true);
			if(is_array($mlist))
				foreach($mlist as $obj)
					$ml[] = $obj['uid'];
			$res['mailing_list'] = json_encode($ml);
		} else 
		$res['mailing_list'] = "";
		return $res;
	}
	
	
}