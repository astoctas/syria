<?php

class Objects_model extends CI_Model {

	var $table = "objects";
	var $keyField = 'uid';
	
    function __construct()
    {
        parent::__construct();
    }
	
	public  function update($data){
		if(!array_key_exists(0,$data)) $data = array($data);
		$result = array();
		$query = "INSERT INTO $this->table (`uid`,`name`,`schema`, `layout`) 
		VALUES (?,?,?,?) 
		ON DUPLICATE KEY UPDATE name=VALUES(name), `schema`=VALUES(`schema`), `layout`=VALUES(`layout`)";
		foreach($data as $d){
			if(empty($d['uid'])) $d['uid'] = $this->ajax->getUid();
			$values = array($d['uid'],$d['name'],$d['schema'],$d['layout']);
			$this->db->query($query, $values);
			$result[] = $d;//$res->row_array();
		}
		return $result;
	}	

	
}