<?php

class Pages_model extends CI_Model {
	
	var $table = 'struct';
	var $keyField = 'uid';
	
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function recursive($id, $results, $checked) {
		$nodes = array();
		foreach($results as $r){
			if($r['parent']==$id) {
				$node = $r;
				if(is_array($checked))
					$node['checked'] = in_array($r['uid'], $checked);
				$node['children'] = $this->recursive($r['uid'], $results, $checked);
				$nodes[] = $node;
			}
		}
		return $nodes; 
	}

	public function recursiveSlug(&$results = array(), $slug = "", &$plain){
		foreach($results as &$r){
			$r['slug'] = (empty($slug)) ? $r['slug'] : $slug."/".$r['slug'];
			//if(isset($plain)) 
				$plain[$r['uid']] = $r;
			if($r['children']) $this->recursiveSlug($r['children'], $r['slug'], $plain);
		}
		return $results;	
	}	
	
	public function getTree($node = FALSE, $checked = FALSE, &$plain = NULL) {
		$data = array();
		$isRoot = ($node=='root');
		
		$whereNode = ($isRoot)  ? "" : " parent = '$node' AND ";
		$query = "SELECT st.name as text, st.uid, st.layout, l.controller, st.`schema`, st.parent, st.`order` as 'index', st.slug, st.mod_date FROM struct st
			LEFT JOIN layouts l ON l.uid = st.layout 
			LEFT JOIN `schemas` sc ON st.schema = sc.uid 
			WHERE $whereNode in_tree = 1 AND is_form = 0
			ORDER BY st.`order`ASC
		";
		$values = array($node);
		$res = $this->db->query($query,$values);
		if($isRoot) {
			if($res->num_rows()) {
				$results = $res->result_array();
				$data = $this->recursive('root',$results, $checked);
				$data = $this->recursiveSlug($data, "", $plain);
				return $data;
			} else return array();
		} else return ($res) ? $res->result_array() : FALSE;
	}

	public function load() {
		$this->load->library('ajax');
		$data = array();
		$relData = array();
		$uid  = $this->db->escape($this->ajax->post('uid'));
		// TRAIGO LOS CAMPOS
		$query = "SELECT *
		FROM `fields` f
		INNER JOIN `schemas` s ON s.uid = f.schema_uid
		INNER JOIN `objects` o ON o.`schema` = s.uid
		INNER JOIN `contents` c ON c.object = o.uid
		WHERE c.uid = $uid
		ORDER BY f.`order`";
		$res = $this->db->query($query);
		foreach($res->result_array() as $field) {
			$data[$field['uid']] = FALSE;
		}
		$query = "
		SELECT data.*, f.datatype FROM (
		(SELECT dv.data, dv.field, dv.order FROM data_all dv WHERE dv.uid = $uid)
		) as data
		LEFT JOIN fields f ON data.field = f.uid
		ORDER BY f.`order`, data.`order`";
		$res = $this->db->query($query);
		if ($res->num_rows()) {
			foreach($res->result_array() as $r) {
				switch($r['datatype']) {
					case 'contents':
						$rel = $this->db->get_where('contents', array('uid'=>$r['data']))->row_array();
						if($rel) $relData[$r['field']][] = $rel;
						break;
					case 'pages':
					case 'category':
						$relData[$r['field']][] = $r['data'];
						break;
					case 'checkgroup':
					case 'radiogroup':
						$data[$r['field'].'[]'] = json_decode($r['data'],true);
						break;
					default:
						$data[$r['field']] = $r['data'];
				}
			}
			foreach($relData as $field => $values) {
				$data[$field] = json_encode($values,JSON_NUMERIC_CHECK);
			}
		}
		$query = "SELECT * FROM struct WHERE uid = $uid";
		$res = $this->db->query($query);
		foreach($res->row_array() as $f => $v) {
			$data[$f] = $v;
		}			
		return $data;
	}

	public function edit($post = array()) {
		if(!$post) return FALSE;
		$this->load->model('backend/datastore_model');
		$this->uid = ($post['uid']) ? $post['uid'] : $this->ajax->getUid();
		// SAVE DATA
		$this->datastore_model->saveData($this->uid, $post);
		// INSERT RECORD IN struct
		$this->load->library('auth');
		$user = $this->auth->getUserAdmin();
		$query = "INSERT INTO `$this->table` (`uid`,`name`, `slug`, `layout`, `layout_for_contents`, `schema`, `active`, `user`,`private`, `redirect`) 
			VALUES (?,?,?,?,?,?,?,?,?,?)
			ON DUPLICATE KEY UPDATE `name`=VALUES(name), `slug`=VALUES(slug), `mod_user`=VALUES(user), `mod_date`=NOW(),
			 `layout`=VALUES(`layout`), `layout_for_contents`=VALUES(`layout_for_contents`), `schema`=VALUES(`schema`), `private`=VALUES(`private`), 
			 `redirect`=VALUES(`redirect`)";
		$values = array($this->uid,$post['name'],$post['slug'],$post['layout'],$post['layout_for_contents'],$post['schema'], 1,$user->uid,$post['private'], $post['redirect']);			 
		$this->db->query($query,$values);
		return $this->db->affected_rows();
	}

	public function duplicate($data = array()) {
		if(!$data) return FALSE;
		/* DUPLICAR CAMPOS */
		$query = "UPDATE $this->table SET 
				`layout`= ? , `layout_for_contents`= ?, `schema`= ?, `private`= ? WHERE uid = ? ";
		$values = array($data['layout'],$data['layout_for_contents'],$data['schema'], $data['private'], $data['id']);			 
		$this->db->query($query, $values);
		/* DUPLICAR SECCIONES */
		$sections = $this->db->get_where('section', array('struct'=>$data['duplicate_uid']))->result_array();
		foreach($sections as $_s) {
			$newUid =  $this->ajax->getUid();
			$query = "INSERT IGNORE INTO section (`uid`, `name`, `section_type`, `struct`, `paging`, `layout`, `region`, `template`, `auto`, `order`, `params`, `titulo`)
				(SELECT ?, `name`, `section_type`, ?, `paging`, `layout`, `region`, `template`, `auto`, `order`, `params`, `titulo`
				 FROM section WHERE uid = ?)";
			$values = array($newUid, $data['id'], $_s['uid']);	 
			$this->db->query($query, $values);
			/* DUPLICAR SETS */
			$query = "INSERT IGNORE INTO sections_sets 
				(SELECT ?, `set`, `show_default` FROM sections_sets WHERE section_uid = ?)";
			$values = array($newUid, $_s['uid']);
			$this->db->query($query, $values);
		}
	}

	public  function update($data){
		if(!array_key_exists(0,$data)) $data = array($data);
		$result = array();
		$query = "INSERT INTO $this->table (`uid`,`name`,`parent`,`active`, `order`) 
		VALUES (?,?,?,?, ?) 
		ON DUPLICATE KEY UPDATE name=VALUES(name), parent=VALUES(parent), active=VALUES(active), `order`=VALUES(`order`)";
		foreach($data as $d){
			$values = array($d['id'],$d['text'],$d['parentId'],1, $d['index']);
			$this->db->query($query, $values);
			//$res = $this->db->get_where($this->table, array('uid'=>$d['id']));
			$result[] = $d;//$res->row_array();
			// DUPLICATE
			if(isset($d['duplicate_uid']) && !empty($d['duplicate_uid'])) $this->duplicate($d);
		}
		return $result;
	}
		
	public function delete($data) {
		$this->db->delete($this->table, array('uid'=>$data['id']));
		return $this->db->affected_rows();
	}
	
	
}