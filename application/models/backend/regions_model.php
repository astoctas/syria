<?php

class Regions_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function save($data = FALSE) {
		if(!$data) return FALSE;
		$query = "INSERT INTO section (`uid`, `name`, `section_type`, `struct`,`layout`,`region`,`template`,`order`, `auto`, `params`, `paging`, `titulo`)
			VALUES (?,?,?,?,?,?,?,?,?,?,?,?) 
			ON DUPLICATE KEY UPDATE `name`=VALUES(`name`), `template`=VALUES(`template`), `order`=VALUES(`order`),  `titulo`=VALUES(`titulo`),  
			`region`=VALUES(`region`), `auto`=VALUES(`auto`), `params`=VALUES(`params`), `paging`=VALUES(`paging`)";
		$values = array($data['section'],$data['name'],$data['section_type'],$data['struct'],$data['layout'],$data['region'],$data['template'],$data['order'], $data['auto'], $data['params'], $data['paging'], $data['titulo']);		
		$res = $this->db->query($query,$values);
		$query = "INSERT IGNORE INTO sections_sets (`section_uid`, `set`) VALUES (?,?)
			ON DUPLICATE KEY UPDATE `set` = VALUES(`set`)";
		$values = array($data['section'], $data['set']);
		$res = $this->db->query($query,$values);
		/* UPDATE LASTMOD TIME */
		$query = "UPDATE struct SET mod_date = NOW() WHERE uid = ?";
		$values = array($data['struct']);
		$this->db->query($query,$values);
		
		return TRUE;
	}
	
}