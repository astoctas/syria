<?php

class Schemas_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	
	public function update($data = FALSE) {
		if(!$data) return FALSE;
		if(!$data['uid']) {
			$data['uid'] = $this->ajax->getUid();
		}
		// UPDATE SCHEMA
		$query = "INSERT INTO `schemas` (`uid`,`name`,`create_date`) 
			VALUES (?,?,NOW())
			ON DUPLICATE KEY UPDATE name=VALUES(name) ";
		$values = array($data['uid'],$data['name']);
		$this->db->query($query,$values);
		// AGREGAR o MODIFICAR CAMPOS
		$uidFields = array();
		$fields = json_decode($data['fields'],true);
		$query = "INSERT INTO `fields`(`uid`,`schema_uid`,`name`,`tab`, `datatype`,`vtype`,`required`, `default_value`, `order`, `options`, `show_other`, `help`) 
		VALUES (?, ?, ?, ?, ?, ? ,?, ?, ?, ?, ?, ?)
		ON DUPLICATE KEY UPDATE name=VALUES(name), tab=VALUES(tab), vtype=VALUES(vtype), required=VALUES(required), default_value=VALUES(default_value), `order`=VALUES(`order`), `options`=VALUES(`options`), `show_other`=VALUES(`show_other`), `help`=VALUES(`help`) ";
		if($fields) 
		foreach($fields as $field) {
			if(empty($field['uid'])) $field['uid'] = $this->ajax->getUid();
			$uidFields[] = "'{$field['uid']}'";
			$values = array($field['uid'],$data['uid'],$field['name'],$field['tab'],$field['datatype'],$field['vtype'],$field['required'],$field['default_value'], $field['order'], $field['options'], $field['show_other'], $field['help']);
			$this->db->query($query,$values);
		}
		// BORRAR LOS ELIMINADOS
		$query = "DELETE FROM `fields` WHERE schema_uid = '{$data['uid']}' "; 
		if($uidFields) $query .= " AND uid NOT IN (".implode(',',$uidFields).")"; 
		$this->db->query($query);
		return true;
	}
	
	public function load($data) {
		if(!$data) return FALSE;
		$res = $this->db->get_where('schemas', array('uid'=>$data['uid']));
		if($res->num_rows()) {
			$schema = $res->row_array();
			$this->db->where('schema_uid',$data['uid']);
			$this->db->order_by('order','ASC');
			$res = $this->db->get('fields');
			$schema['fields'] = json_encode($res->result_array(),JSON_NUMERIC_CHECK);
			return $schema;
		} else return FALSE;
	}
	
	public function duplicate($uid = "") {
		if(!$uid) return FALSE;
		$schema_uid = $this->ajax->getUid();
		$query = " INSERT INTO  `schemas`
			SELECT  '$schema_uid',  concat(`name`,'-copy'),  `color`,  `template`,  `etype`,  `create_date` 
			FROM `schemas`
			WHERE uid = '$uid'";
		$this->db->query($query);
		$query = " INSERT INTO `fields`
			SELECT UUID(), '$schema_uid', `name`, `datatype`, `label`, `order`, `vtype`, `max_files`, `is_unique`, `required`, `default_value`, `th_width`, `th_height`, `th_fill_color`, `options`, `show_other`, `help`
			FROM `fields`
			WHERE schema_uid = '$uid'";
		$this->db->query($query);
	}
	
}