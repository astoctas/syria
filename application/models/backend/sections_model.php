<?php

class Sections_model extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	
	public function add($uid = "") {
		if(!$uid) return FALSE;
		$query = "INSERT INTO sections_sets  (`section_uid`, `set`) 
			(SELECT'$uid', IF(MAX(`set`),max(`set`)+1,1) as `set` FROM sections_sets)
			ON DUPLICATE KEY UPDATE `set` = VALUES(`set`) ";
		$this->db->query($query);
		return $this->db->get_where('sections_sets', array('section_uid'=>$uid))->row_array();
	}
	
	public function getTree($set = 0, $node = FALSE, $checked = FALSE) {
		$data = array();
		$isRoot = ($node=='root');
		
		function recursive($id, $results, $checked) {
			$nodes = array();
			foreach($results as $r){
				if($r['parent']==$id) {
					$node = $r;
					if(is_array($checked))
						$node['checked'] = in_array($r['uid'], $checked);
					$node['children'] = recursive($r['uid'], $results, $checked);
					$nodes[] = $node;
				}
			}
			return $nodes; 
		}
		$whereNode = ($isRoot)  ? "" : " parent_uid = '$node' AND  ";
		$query = "SELECT st.name as name, ss.node_uid as 'uid', ss.parent_uid as 'parent', ss.`order` as 'index' FROM sets_trees ss
			INNER JOIN struct st ON st.uid = ss.node_uid
			WHERE $whereNode `set` = '$set'
			ORDER BY ss.`order`ASC
		";
		$values = array($node);
		$res = $this->db->query($query,$values);
		if($isRoot) {
			if($res->num_rows()) {
				$results = $res->result_array();
				$data = recursive('root',$results, $checked);			
				return $data;
			} else return array();
		} else return ($res) ? $res->result_array() : FALSE;
	}

	public function set_default($uid = "", $struct = "", $state = false) {
		/*
		$query = "UPDATE sections_sets SET `show_default` = 0
			WHERE section_uid IN
			(SELECT uid	FROM section	WHERE struct = '$struct' )";
		$this->db->query($query);
		*/
		$query = "UPDATE sections_sets SET `show_default` = $state WHERE section_uid = '$uid' ";
		$this->db->query($query);
		return true;
	}
	
}