<?php

class Templates_model extends CI_Model {

	var $table = "templates";
	var $keyField = 'uid';
	
    function __construct()
    {
        parent::__construct();
    }
	
	public  function update($data){
		if(!array_key_exists(0,$data)) $data = array($data);
		$result = array();
		$query = "INSERT INTO $this->table (`uid`,`name`,`controller`, `_method`, `file`) 
		VALUES (?,?,?,?,?) 
		ON DUPLICATE KEY UPDATE name=VALUES(name), `controller`=VALUES(`controller`), `_method`=VALUES(`_method`), `file`=VALUES(`file`)";
		foreach($data as $d){
			if(empty($d['uid'])) $d['uid'] = $this->ajax->getUid();
			$values = array($d['uid'],$d['name'],$d['controller'],$d['_method'],$d['file']);
			$this->db->query($query, $values);
			$result[] = $d;//$res->row_array();
		}
		return $result;
	}	

	
}