<?php

class Users_model extends CI_Model {

	var $table = "user";
	
    function __construct()
    {
        parent::__construct();
    }
		
	public function login($data=array()) {
		$query = "SELECT * FROM user WHERE username = ? AND password = ? ";
		$values  = array($data['user'],md5($data['pass']));
		$res = $this->db->query($query,$values);
		return $res->row();
	}

	public function search() {
		$query = $this->ajax->filters_array['query'];
		$where = "WHERE `data` LIKE '%$query%' ";
		$nameWhere = "WHERE 
			(`username` LIKE '%$query%' 
			OR `email` LIKE '%$query%')	";
		$sort = (!empty($this->ajax->sort) && $s = $this->ajax->sort[0]) ?  "ORDER BY st.`{$s['property']}`  {$s['direction']}" : "";
		$select = "st.username, st.uid,  st.email, st.profile";
		$q = "SELECT SQL_CALC_FOUND_ROWS st.* FROM (
				SELECT $select  FROM `$this->table` st 
				$nameWhere
				) as st
				LEFT JOIN user_profiles p ON st.profile = p.uid
			$sort";
		$res = $this->db->query($q);
		$this->ajax->foundRows();
		return ($res->num_rows()) ? $res->result_array() : FALSE;
		
	}
	
	public function load() {
		$uid  = $this->db->escape($this->ajax->post_data('uid'));
		$this->db->select("*, '' as password", false);
		$res = $this->db->get_where($this->table, array('uid'=> $uid));
		return  $res->row_array();
	}
	
	
	public function edit($post = array()) {
		if(!$post) return FALSE;
		$this->uid = ($post['uid']) ? $post['uid'] : $this->ajax->getUid();
		// INSERT RECORD IN users_site
		$query = "INSERT INTO `$this->table` (`uid`,`username`, `email`, `profile`, `active`, `create_date`) 
		VALUES (?, ?, ?, ?, ?, NOW())
			ON DUPLICATE KEY UPDATE `username`=VALUES(username), `email`=VALUES(email), `profile`=VALUES(profile)";
		$values = array($this->uid, $post['username'], $post['email'], $post['profile'], 1);
		$this->db->query($query, $values);
		// CHANGE PASSWORD
		if($post['password']) {
			$this->load->library('encrypt');
			$query = "UPDATE $this->table SET password = ? WHERE uid = ? ";
			$values  = array(md5($post['password']),$this->uid);
			$this->db->query($query, $values);
		}
		return $this->db->affected_rows();
	}
	


		
}
