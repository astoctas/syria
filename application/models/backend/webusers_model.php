<?php

class Webusers_model extends CI_Model {

	var $table = "user_site";
	var $table_view = "user_site_view";
	
    function __construct()
    {
        parent::__construct();
    }
	

	public function search() {
		$query = $this->ajax->filters_array['query'];
		$where = "WHERE `data` LIKE '%$query%' ";
		$join = "";
		$where2= "";
		if(isset($this->ajax->filters_array['mailing_list_uid']) && $this->ajax->filters_array['mailing_list_uid'] != 'root') {
			$where2 = " WHERE mailing_list_uid = '{$this->ajax->filters_array['mailing_list_uid']}' ";
			$join = " INNER JOIN mailing_lists_suscribers m ON m.user_uid = st.uid ";
		}
		$nameWhere = "WHERE 
			(`username` LIKE '%$query%' 
			OR `firstname` LIKE '%$query%' 
			OR `lastname` LIKE '%$query%' 
			OR `email` LIKE '%$query%')	";
		$sort = (!empty($this->ajax->sort) && $s = $this->ajax->sort[0]) ?  "ORDER BY st.`{$s['property']}`  {$s['direction']}" : "";
		$select = "st.username, st.uid, st.firstname, st.lastname, st.email, st.profile";
		$start = $this->input->get('start');
		$limit = $this->input->get('limit');
		$q = "SELECT SQL_CALC_FOUND_ROWS st.*, p.`schema` as `schema` FROM (
				SELECT $select  FROM `$this->table_view` st 
				$nameWhere
				UNION
				SELECT $select FROM `$this->table_view` st 
				INNER JOIN data_varchar dv ON dv.uid = st.uid
				$where
				UNION
				SELECT $select FROM `$this->table_view` st 
				INNER JOIN data_text dv ON dv.uid = st.uid
				$where
				) as st
				LEFT JOIN user_site_profiles p ON st.profile = p.uid
				$join
				$where2
				$sort
				LIMIT $start, $limit";
		$res = $this->db->query($q);
		$this->ajax->foundRows();
		return ($res->num_rows()) ? $res->result_array() : FALSE;
		
	}
	
	public function load() {
		$this->load->library('ajax');
		$data = array();
		$uid  = $this->db->escape($this->ajax->post('uid'));
		$query = "
			(SELECT d.data, d.field FROM data_varchar d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_text d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_date d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_numeric d WHERE d.uid = $uid)
			UNION	(SELECT d.data, d.field FROM data_uid d WHERE d.uid = $uid)
			UNION	(SELECT username, 'username' as field FROM $this->table c WHERE c.uid = $uid)
			UNION	(SELECT firstname, 'firstname' as field FROM $this->table c WHERE c.uid = $uid)
			UNION	(SELECT lastname, 'lastname' as field FROM $this->table c WHERE c.uid = $uid)
			UNION	(SELECT email, 'email' as field FROM $this->table c WHERE c.uid = $uid)
			UNION	(SELECT profile, 'profile' as field FROM $this->table c WHERE c.uid = $uid)
		";
		$res = $this->db->query($query);
		if ($res->num_rows()) {
			foreach($res->result_array() as $r) {
				$data[$r['field']] = $r['data'];
			}
		}
		return $data;
	}
	
	public function get_files() {
		$uid = $this->input->get('uid');
		$fieldUid = $this->input->get('field');
		if(!$uid || !$fieldUid) return FALSE;
		$query = "SELECT f.*, du.`order` FROM data_uid du 
			INNER JOIN filebank_live f ON f.uid = du.data
			WHERE du.uid = ? AND du.`field`= ?
			ORDER BY du.`order` ASC";
		$values = array($uid,$fieldUid);
		$res = $this->db->query($query, $values);
		return ($res->num_rows()) ? $res->result_array() : array();
	}

	
	public function edit($post = array()) {
		if(!$post) return FALSE;
		$this->load->model('backend/datastore_model');
		$this->uid = ($post['uid']) ? $post['uid'] : $this->ajax->getUid();
		// SAVE DATA
		$this->datastore_model->saveData($this->uid, $post);
		// INSERT RECORD IN users_site
		$query = "INSERT INTO `$this->table` (`uid`,`username`,`firstname`, `lastname`, `email`, `profile`, `active`, `create_date`) 
		VALUES (?, ?, ?, ?, ?, ?, ?, NOW())
			ON DUPLICATE KEY UPDATE `username`=VALUES(username), `firstname`=VALUES(firstname), `lastname`=VALUES(lastname), `email`=VALUES(email), `profile`=VALUES(profile)";
		$values = array($this->uid, $post['username'], $post['firstname'], $post['lastname'], $post['email'], $post['profile'], 1);
		$this->db->query($query, $values);
		// CHANGE PASSWORD
		if($post['password']) {
			$this->load->library('encrypt');
			$query = "UPDATE $this->table SET password = ? WHERE uid = ? ";
			$values  = array($this->encrypt->encode($post['password']),$this->uid);
			$this->db->query($query, $values);
		}
		return $this->db->affected_rows();
	}
	

}