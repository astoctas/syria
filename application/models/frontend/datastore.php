<?php

class Datastore extends CI_Model {
	
	var $cropSizes = array();
	
    function __construct()
    {
        parent::__construct();
    }

	public function getFilebankPaths(&$_r = array()) {
		if(empty($this->cropSizes)) $this->cropSizes = $this->db->get('filebank_sizes')->result_array();
		$crops = $this->db->get_where('filebank_crops', array('filebank_uid'=>$_r['uid']))->result_array();
		$_client = Auth::getClient();
		$filePath = $this->config->item('base_url')."/".UPLOAD_PATH."/_clients/$_client/{$_r['uid']}/";
		$_r['src']['Original'] =  $filePath.$_r['filename'];
		foreach($this->cropSizes as $size) {
			$_index = $size['width']."x".$size['height']; 
			$absPath = $filePath.$_index."/".$_r['filename'];
			$_r['src'][$_index] = "";
			$_r['src'][$size['name']] = "";
			foreach($crops as $crop) {
				if($crop['filebank_size_uid'] == $size['uid']) {
					$_r['src'][$_index] = $absPath;
					$_r['src'][$size['name']] = $absPath;
				}
			}
		}
		return $_r;
	}
	
	public function getData($contents = array()) {
		if(!$contents) return array();
		$data = array();
		$relData = array();
		$relContents = array();
		$relContentsUid = array();
		$relFilebank = array();
		$relFilebankUid = array();
		$relPages = array();
		$relPagesUid = array();
		$relCategores = array();
		$relCategoresUid = array();
		$_client = Auth::getClient();

		$uids = array();
		foreach($contents as $c) {
			if(!isset($c['content_uid'])) continue;
			if($c['content_uid']!='root') {
				$data[$c['content_uid']]['_data'] = $c;
				$data[$c['content_uid']]['_uid'] = $c['content_uid'];
				if(isset($c['object'])) { // ES UN CONTENIDO 
					$this->load->library('slug');
					$data[$c['content_uid']]['_data']['slug'] = $c['id']."-".$this->slug->slugify($c['name']); 
				}
				if(isset($c['filebank_uid'])) { // ES UN ARCHIVO
					$this->getFilebankPaths($data[$c['content_uid']]['_data']);
				}
				if(isset($c['parent_uid'])) $data[$c['content_uid']]['_parent_uid'] = $c['parent_uid'];
				$c['content_uid'] = $this->db->escape($c['content_uid']);
				$uids[] = $c['content_uid'];
			}
		}

		$uid = implode(',',$uids);

		/* TRAIGO LOS DATOS DEL ESQUEMA */		
/*
			(
			(SELECT dv.uid, dv.data, dv.field, 0 as 'order' FROM data_varchar dv WHERE dv.uid IN ($uid))
			UNION	(SELECT dt.uid, dt.data, dt.field, 0 as 'order' FROM data_text dt WHERE dt.uid IN ($uid))
			UNION	(SELECT dd.uid, dd.data, dd.field, 0 as 'order' FROM data_date dd WHERE dd.uid IN ($uid))
			UNION	(SELECT di.uid, di.data, di.field, 0 as 'order' FROM data_time di WHERE di.uid IN ($uid))
			UNION	(SELECT dn.uid, dn.data, dn.field, 0 as 'order' FROM data_numeric dn WHERE dn.uid IN ($uid))
			UNION	(SELECT du.uid, du.data, du.field, `order` as 'order' FROM data_uid du WHERE du.uid IN ($uid))
			) as data
 */
		$query = "
			SELECT data.*, f.datatype, f.name as fieldname, f.tab, f.vtype FROM 
			data_all as data
			INNER JOIN fields f ON data.field = f.uid
			WHERE data.uid IN ($uid)
			ORDER BY f.`order`, data.`order`ASC ";

		if($uids) {
			$res = $this->db->query($query);
	
		if ($res->num_rows()) {
			foreach($res->result_array() as $r) {
				$r['tab'] = empty($r['tab']) ? '0' : $r['tab'];
				switch($r['datatype']) {
					case 'select':
					case 'contents':
						$relContents[] = array('uid'=>$r['uid'], 'field'=>$r['fieldname'], 'tab'=>$r['tab'], 'data'=>$r['data'], 'order'=>$r['order']);
						$relContentsUid[] = $r['data'];
					break;
					case 'file':
						$relFilebank[] = array('uid'=>$r['uid'], 'field'=>$r['fieldname'], 'tab'=>$r['tab'], 'data'=>$r['data']);
						$relFilebankUid[] = $r['data'];
					break;
					case 'pages':
						$relPages[] = array('uid'=>$r['uid'], 'field'=>$r['fieldname'], 'tab'=>$r['tab'], 'data'=>$r['data']);
						$relPagesUid[] = $r['data'];
					break;
					case 'category':
						$relCategories[] = array('uid'=>$r['uid'], 'field'=>$r['fieldname'], 'tab'=>$r['tab'], 'data'=>$r['data']);
						$relCategoresUid[] = $r['data'];
					break;
					case 'checkgroup':
					case 'radiogroup':
						$tmp = json_decode($r['data'],true);
						$relData[$r['uid']][$r['fieldname']][$r['tab']] = $tmp;
					break;
					default:
//						$relData[$r['uid']][$r['fieldname']] = (empty($r['tab'])) ? $r['data'] : array($r['tab']=>$r['data']);
						$relData[$r['uid']][$r['fieldname']][$r['tab']] = $r['data'];
						if($r['vtype'] == 'url') {
							$relData[$r['uid']][$r['fieldname']][$r['tab']] = str_replace('http://_base.url_', $this->config->item('base_url'), $r['data']);
						}
				}
			}
			/* TRAER CONTENIDOS RELACIONADOS */
			if($relContentsUid) {
				$this->db->where_in('uid', $relContentsUid);
				$res = $this->db->get('contents_active')->result_array();
				$res  = $this->getData($res);
				// RECUPERAR EL ORDEN
				$res_ordenado = array();
				foreach($res as $_r)
					foreach($relContents as $v) {
						if($_r['_uid']==$v['data'])  
							$res_ordenado[$v['uid']][$v['order']] = $_r;
					}
				foreach($res_ordenado as $res) {
					ksort($res,SORT_NUMERIC );
					foreach($res as $_r)
						foreach($relContents as $v) {
							if($_r['_uid']==$v['data'])  
								$relData[$v['uid']][$v['field']][$v['tab']][] = $_r;
						}
					}
				}
			/* TRAER ARCHIVOS  RELACIONADOS */
			if($relFilebankUid) {
				$this->db->where_in('uid', $relFilebankUid);
				$res = $this->db->get('filebank_active')->result_array();
				foreach($relFilebank as $v) {
					foreach($res as $_r) {
						if($_r['uid']==$v['data']) {
							$this->getFilebankPaths($_r);
							$relData[$v['uid']][$v['field']][$v['tab']][] = $_r;
						}
					}
				}
			}
			/* TRAER PAGINAS  RELACIONADAS */
			if($relPagesUid) {
				$this->db->where_in('uid', $relPagesUid);
				$res = $this->db->get('struct_active')->result_array();
				foreach($res as $_r)
					foreach($relPages as $v) {
						if($_r['uid']==$v['data']) 
							$relData[$v['uid']][$v['field']][$v['tab']][] = $_r;
					}
			}
			/* TRAER CATEGORIAS  RELACIONADAS */
			if(isset($relCategoresUid)) 
			if($relCategoresUid) {
				$this->db->where_in('uid', $relCategoresUid);
				$res = $this->db->get('categories')->result_array();
				foreach($res as $_r)
					foreach($relCategories as $v) {
						if($_r['uid']==$v['data']) 
							$relData[$v['uid']][$v['field']][$v['tab']][] = $_r;
					}
			}
			
		}
		}
		
		/* FILTRO POR SOLAPA Y FORMATO DE DATOS */
		$_tab= Auth::getLang();
		$this->load->library('slug');
		foreach($relData as $_uid => $values) {
			foreach($values as $field => $_d) {
				$saniField = $this->slug->slugify($field, FALSE, '_');
				if($_tab) {
					if(isset($_d[$_tab]) ) {
						$data[$_uid][$saniField] = $_d[$_tab];
					}	else {
						 $data[$_uid][$saniField] = array_key_exists('0', $_d) ? array_shift($_d)  : "";
					}
				}
				else 
					$data[$_uid][$saniField] =  array_key_exists('0', $_d) ? array_shift($_d)  : "";
			}
		}
		
		return $data;
	}

}