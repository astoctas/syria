<?php

class Sections extends CI_Model {
	
    function __construct()
    {
        parent::__construct();
    }
	

	public function render($data = array()) {
		/* TRAER LAS SECCIONES DEL LAYOUT */
		$this->db->select('t.*, s.*');
		$this->db->join('templates t', "t.uid = s.template");
		$this->db->order_by('order');
		$sections = $this->db->get_where('section s', array('layout'=>$data['layout'], 'struct'=>$data['struct']))->result_array();
		$_client = Auth::getClient();
		
		$_regions = array();
		foreach($sections as $_sec) {
			$_data = array();
			$_sec['content_uid'] = (isset($data['_content']['content_uid'])) ? $data['_content']['content_uid']  : false;
			$_sec['params'] = json_decode($_sec['params']) ?: $_sec['params'];
			$_sec['auto'] = json_decode($_sec['auto'],true) ?: $_sec['auto'];
			if(isset($_sec['auto']['category'])){
				$_sec['auto']['category'] = $this->db->get_where('categories', array('uid'=>$_sec['auto']['category']))->row_array();
			}
			$tpl = $_sec['file'];
			if($this->smarty->templateExists($tpl)) {
				$pagInfo = array();
				$_data = $this->getContents($_sec, $pagInfo);
				if($_data && $_sec['section_type']=='tree') {
					$_data = $this->recursive('root', $_data, $data['_pagesFlat']);
				}
				if($_sec['controller']) { 
					$this->load->model("../_clients/$_client/models/{$_sec['controller']}_custom", 'mod');
					if($_sec['_method']) 
						if(method_exists($this->mod, $_sec['_method'])) {
							$_data = $this->mod->$_sec['_method']($_sec, $_data);
						}
				}
				$this->smarty->assign('paginfo', $pagInfo);
				$this->smarty->assign('contents', $_data);
				$this->smarty->assign('section', $_sec);
				$tplHtml = $this->smarty->fetch($tpl);
				// DEBUGS				
				if($this->input->get('debug')=='templates'){
					$html = "<cms-template data-file=\"{$_sec['file']}\" data-name=\"{$_sec['name']}\" id=\"{$_sec['uid']}\">";
					$html .= "<cms-overlay><p>{$_sec['file']} - {$_sec['name']}</p></cms-overlay>";
					$html .= $tplHtml;
					$html .= "</cms-template>";
				} else {
					$html = $tplHtml;
				}

				$_regions[$_sec['region']][] = $html;
				$this->smarty->clearAssign('contents');
			}
		}
		return $_regions;	
		
	}
	
	public function recursive($id, $results, $slugs = array()) {
		$nodes = array();
		foreach($results as $r){
			if($r['_parent_uid']==$id) {
				$node = $r;
				$node['_children'] = $this->recursive($r['_uid'], $results);
				if($slugs)
					$node['slug'] = $slugs[$r['_uid']]['slug'];
				$nodes[] = $node;
			}
		}
		return $nodes; 
	}	
	
	public function getContents($section, &$pagInfo) {
		$data = array();
		$xpag = 0;
		$pag = 0;
		if(isset($section['paging'])) {
			$xpag = ($this->input->get('xpag')) ?: $section['paging'];
			$pag = ($this->input->get('pag')) ? (int)$this->input->get('pag') - 1: 0;
			$limitFrom = (int)$pag * (int)$xpag;
			$limitTo = (int)$xpag;
		}
		if(isset($section['auto']['object']) && $section['auto']['object']) {
			// NO TRAER EL CONTENIDO QUE SE ESTA VIENDO
			$whereNot = ($section['content_uid']) ? " AND c.uid <> '{$section['content_uid']}' " : "";
			// TRAE LOS CONTENIDOS DEL OBJETO CONFIGURADO 
			$query = "SELECT SQL_CALC_FOUND_ROWS * FROM contents_active c
					WHERE object = '{$section['auto']['object']}' $whereNot
					ORDER BY c.create_date DESC"; 			
			/* TODO: CONFIGURAR OTROS MODOS DE ORDENAMIENTO */
		} else {
			switch($section['section_type']){
				case 'contents': 
					$query = "SELECT SQL_CALC_FOUND_ROWS * FROM sets_contents sc
						INNER JOIN sections_sets ss ON ss.set = sc.set 
						INNER JOIN contents_active c ON c.uid = sc.content_uid
						WHERE ss.section_uid = '{$section['uid']}' 
						ORDER BY sc.`order` ASC";
				break;
				case 'tree': 
					$query = "SELECT SQL_CALC_FOUND_ROWS *, node_uid as content_uid FROM sets_trees sc
						INNER JOIN sections_sets ss ON ss.set = sc.set 
						INNER JOIN struct_active c ON c.uid = sc.node_uid
						WHERE ss.section_uid = '{$section['uid']}' 
						ORDER BY sc.`order` ASC";
				break;
				case 'images': 
				case 'filebank': 
					$query = "SELECT SQL_CALC_FOUND_ROWS *, filebank_uid as content_uid FROM sets_filebank sc
						INNER JOIN sections_sets ss ON ss.set = sc.set 
						INNER JOIN filebank_active c ON c.uid = sc.filebank_uid
						WHERE ss.section_uid = '{$section['uid']}' 
						ORDER BY sc.`order` ASC";
				break;
			}
		}
		$query .= ($xpag) ? " LIMIT $limitFrom, $limitTo " : "";
		$contents = $this->db->query($query);
		if(!$contents) return $data;
		$contents = $contents->result_array();
		$params = array(
				'xpag' => $xpag,
				'pag' => $pag,
		);
		$this->getPaging($params, $pagInfo);
		if($contents && $pagInfo && $pag  > $pagInfo['pages']) {
			show_404();
			exit;
		}
		$this->load->model('frontend/datastore');
		$data = $this->datastore->getData($contents);
		return $data;
	}

	public function getPaging($params, &$pagInfo) {
		if($params['xpag']){
			$totalRes = $this->db->query("SELECT FOUND_ROWS() as total")->row_array();
			$pages = ceil((int)$totalRes['total'] / $params['xpag']);
			$pagInfo = array(
				'xpag' => $params['xpag'],
				'pag' => (int)$params['pag'] + 1,
				'records' => $totalRes['total'],
				'pages' => $pages,
				'prev' => $params['pag'],
				'next' => ($params['pag'] + 1< $pages) ? $params['pag'] + 2 : 0,
				'viewfrom' => ($params['pag'])*$params['xpag']+1,
				'viewto' => ($totalRes['total'] > ($params['pag']+1)*$params['xpag']) ? $totalRes['total'] : $totalRes['total'] -  ($params['pag'])*$params['xpag']
			);
		}		
	}


}