<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>{block name="title"}{/block}</title>
        <meta name="description" content="CMS">
        <meta name="author" content="CMS">
		{loadConstants}
		{loadData}
		{loadJS src="backend/js/tinymce/tiny_mce.js"}
		{block name="meta"}{/block}
		{block name="jsie"}{/block}
		{block name="css"}
			<link rel="stylesheet" type="text/css" media="screen" href="{$BASEURL}{$JS_PATH}/backend/js/jquery/jcrop/css/jquery.Jcrop.min.css" />
			<link rel="stylesheet" type="text/css" media="screen" href="{$BASEURL}{$JS_PATH}/backend/js/extjs/resources/css/ext-all.css" />
			<link rel="stylesheet" type="text/css" media="screen" href="{$BASEURL}{$JS_PATH}/backend/css/backend.css" />
		{/block}
		<script>
			var _client = '{$_client}';
		</script>
    </head>
    <body>
    	{block name="body"}
			{block name="header"}{/block}
			{block name="content"}
				<div id="content"><img src="{$BASEURL}assets/backend/css/img/loading.gif" /></div>
			{/block}
			{block name="footer"}{/block}		
		{/block}
		{block name="js"}
		{loadJS src="backend/js/extjs/ext-all.js"}
		{loadJS src="backend/js/extjs/locale/ext-lang-es.js"}
		{loadJS src="backend/js/Ext.init.js"}
		{loadJS src="backend/js/MailingSender.js"}
		{loadJS src="backend/js/jquery/jquery-1.8.2.min.js"}
		{loadJS src="backend/js/jquery/jquery-ui-1.10.2.custom.min.js"}
		{*loadJS src="backend/js/TinyMCE.js"*}
		{loadJS src="backend/js/jquery/jcrop/js/jquery.Jcrop.min.js"}
		{loadJS src="backend/js/CMS.js"}
		{/block}
    </body>
</html>