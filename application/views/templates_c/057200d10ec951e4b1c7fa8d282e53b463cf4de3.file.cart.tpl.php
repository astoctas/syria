<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:36:50
         compiled from "application/_clients/default/templates/cart.tpl" */ ?>
<?php /*%%SmartyHeaderCode:14462196455f04f902a15a03-21172902%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '057200d10ec951e4b1c7fa8d282e53b463cf4de3' => 
    array (
      0 => 'application/_clients/default/templates/cart.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '14462196455f04f902a15a03-21172902',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page' => 0,
    '_sendok' => 0,
    'contents' => 0,
    'item' => 0,
    'URL' => 0,
    'ASSETS' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5f04f902ad2391_90807932',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f04f902ad2391_90807932')) {function content_5f04f902ad2391_90807932($_smarty_tpl) {?>	<div class="th"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['page']->value['Titulo'])===null||$tmp==='' ? '' : $tmp);?>
</div>
	<?php if (isset($_smarty_tpl->tpl_vars['_sendok']->value)&&$_smarty_tpl->tpl_vars['_sendok']->value){?>
	<div class="alert alert-warning " id="msg-sent"><h3>El carrito ha sido enviado para su cotización.  Le responderemos a la brevedad.</h3></div>

	
<!-- Google Code for syria-contacto Conversion Page
In your html page, add the snippet and call
goog_report_conversion when someone clicks on the
chosen link or button. -->
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 970864604;
    w.google_conversion_label = "QpoUCOLlgWoQ3O_4zgM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script>
	

	<?php }else{ ?>
		<div class="text">
			<?php echo (($tmp = @$_smarty_tpl->tpl_vars['page']->value['Texto'])===null||$tmp==='' ? '' : $tmp);?>

		</div>
		<div id="cart-content">
		<div class="listing row">

					<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item']->iteration++;
?>
					<div class= " col-xs-12 col-md-6 " >

					<div class="item <?php if ($_smarty_tpl->tpl_vars['item']->iteration%2){?>left<?php }?>">
						<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Imagenes'])){?>
						<a class="" href="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
/productos/<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
"><img class="photo <?php if (isset($_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua'])&&$_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua']==1){?> watermark<?php }?>" src="<?php echo $_smarty_tpl->tpl_vars['item']->value['Imagenes'][0]['src']['Thumbnail'];?>
" /></a>
						<?php }else{ ?>
						<a class="" href="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
/productos/<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
"><img class="photo" src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/images/no-image.jpg" /></a>
						<?php }?>
						<div class="info">
							<div class="title"><?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['name'];?>
</div>
							<strong>Tipo: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Tipo'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Marca: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Marca'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Tránsito: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Transito'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Corte: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Corte'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Medida: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Medida'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Precio 1ra calidad: </strong><span class="price">$ <?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Precio_primera'])===null||$tmp==='' ? "N/D" : $tmp);?>
 m<sup>2</sup></span><br/>
							<strong>Precio 2da calidad: </strong><span class="price">$ <?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Precio_segunda'])===null||$tmp==='' ? "N/D" : $tmp);?>
 m<sup>2</sup></span><br/>
							<div class="btns">
							<div class="cantidad pull-left"><label>Cantidad</label>&nbsp;<input type="text" data-uid="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
" class="cantidad" value="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['cantidad'];?>
" />&nbsp;</div>
							<a class="btn btn-success pull-left btn-narrow btn-cart-update" data-uid="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
"><span class="fa fa-check"></span></a>
							<a class="btn btn-danger btn-cart-delete pull-left btn-narrow" data-uid="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
" data-toggle="modal" data-target="#modal-<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
"><span class="fa fa-trash"></span></a>
							</div>
						</div>
					</div>
					</div>
						<div id="modal-<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-lg">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">Quitar producto</h4>
						      </div>
							   <div class="modal-body">
						      ¿Desea quitar el producto <strong><?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['name'];?>
</strong> del carrito?
								</div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						        <button type="button" class="btn btn-orange btn-cart-confirm" data-uid="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
">Quitar</button>
						      </div>
						    </div>
						  </div>
						</div>
					<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
					 <div class="alert alert-warning"><h3>No hay productos en el carrito.</h3></div>
					<?php } ?>
			</div>
	        <div class="clearfix"></div>
			<?php if ($_smarty_tpl->tpl_vars['contents']->value){?>
			<a class="btn btn-orange btn-lg"  data-toggle="modal" data-target="#modal-ok"><span class="fa fa-shopping-cart"></span> Enviar</a>

						<div id="modal-ok" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-lg">
						    <div class="modal-content">
						      <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						        <h4 class="modal-title" id="myModalLabel">Enviar carrito</h4>
						      </div>
							   <div class="modal-body">

				              		<div class="alert alert-danger hidden-xs-up" id="alert-failure">
				              			<h4>Error al enviar la cotización.</h4>  Por favor, intente nuevamente.
				              		</div>
				              		<div class="alert alert-success hidden-xs-up" id="alert-success">
				              			<h4>La cotización ha sido enviada.</h4>  Nos pondremos en contacto a la brevedad.
				              		</div>


							   	<div id="form-content">
						      ¿Desea enviar todos los productos del carrito para cotizar?  Complete el formulario.<br/><br/>

								<form class="form-horizontal" role="form" method="post" id="cart-send-form">
									<input type="hidden" name="cart" value="1" />
								  <div class="form-group">
								    <label for="inputPassword3" class="col-sm-2 control-label">Nombre</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control " id="inputPassword3" placeholder="Nombre" name="nombre" required>
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
								    <div class="col-sm-10">
								      <input type="email" class="form-control " id="inputEmail3" placeholder="Email" name="email" required>
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputTel3" class="col-sm-2 control-label">Teléfono</label>
								    <div class="col-sm-10">
								      <input type="text" class="form-control " id="inputTel3" placeholder="Teléfono" name="telefono">
								    </div>
								  </div>
								  <div class="form-group">
								    <label for="inputText3" class="col-sm-2 control-label">Comentarios</label>
								    <div class="col-sm-10">
								      <textarea class="form-control" id="inputEmail3" placeholder="Comentarios" rows="10" name="comentarios"></textarea>
								    </div>
								  </div>
								  <div class="form-group">
								    <div class="col-sm-offset-2 col-sm-10">
								      <button   onclick="goog_report_conversion ('http://syriaceramicos.com.ar/cotizar')" type="submit" id="cart-submit-btn" class="btn btn-orange btn-cart-send">Enviar</button>
								      <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
								    </div>
								  </div>
								</form>
							   	</div>
								</div>
						    </div>
						  </div>
						</div>
			<?php }?>
		</div>
		<?php }?>
<?php }} ?>