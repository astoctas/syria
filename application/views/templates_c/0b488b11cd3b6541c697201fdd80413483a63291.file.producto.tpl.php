<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:30:26
         compiled from "application/_clients/default/templates/producto.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7426838205f04f7829c7e89-00906351%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b488b11cd3b6541c697201fdd80413483a63291' => 
    array (
      0 => 'application/_clients/default/templates/producto.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7426838205f04f7829c7e89-00906351',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'content' => 0,
    'item' => 0,
    'ASSETS' => 0,
    'img' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5f04f782a7ce21_94608907',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f04f782a7ce21_94608907')) {function content_5f04f782a7ce21_94608907($_smarty_tpl) {?><?php $_smarty_tpl->tpl_vars['item'] = new Smarty_variable($_smarty_tpl->tpl_vars['content']->value, null, 0);?>
	<div class="th no-border"><span class="bc1">Productos :: </span> <span class="bc2"><?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['name'];?>
</span></div>

	<div class="product">
		<div class="row">
			<div class="col-md-6">
				<div class="title orange"><?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['name'];?>
</div>
				<div class="info">
					<strong>Tipo: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Tipo'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
					<strong>Marca: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Marca'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
					<strong>Tránsito: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Transito'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
					<strong>Corte: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Corte'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
					<strong>Medida: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Medida'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
					<strong>Colores: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Colores'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
					<strong>Precio primera calidad: </strong><span class="price">$ <?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Precio_primera'])===null||$tmp==='' ? "N/D" : $tmp);?>
 /<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad'])&&$_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad']==1){?>unidad<?php }else{ ?>m<sup>2</sup><?php }?><br/>
				<?php if ($_smarty_tpl->tpl_vars['item']->value['Precio_segunda']>0){?>
					<strong>Precio segunda calidad: </strong><span class="price">$ <?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Precio_segunda'])===null||$tmp==='' ? "N/D" : $tmp);?>
 /<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad'])&&$_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad']==1){?>unidad<?php }else{ ?>m<sup>2</sup><?php }?></span><br/>
				<?php }?>
					<?php if ($_smarty_tpl->tpl_vars['item']->value['Mas_Info']){?><strong>Más Info:</strong><br/><?php echo $_smarty_tpl->tpl_vars['item']->value['Mas_Info'];?>
<br/><?php }?>
					<!--div class="pull-left"><strong>Colores: </strong></div><div class="color img-rounded" style="background-color: #FFFFFF">&nbsp;</div><div class="color img-rounded" style="background-color: #d3c2a6">&nbsp;</div-->
		            <div class="clearfix"></div>

		            <div class="clearfix"></div><br/>
					<a class="btn btn-gray btn-cart" data-uid="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
"><img src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/images/carrito-blanco.png" /> Cotizar</a>
				</div>
				</div>
			<div class="col-md-6">
				<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Imagenes'])){?>
				<div class="title">Galería de imágenes</div>
				<div class="imgs">
					<?php  $_smarty_tpl->tpl_vars['img'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['img']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item']->value['Imagenes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['img']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['img']->key => $_smarty_tpl->tpl_vars['img']->value){
$_smarty_tpl->tpl_vars['img']->_loop = true;
 $_smarty_tpl->tpl_vars['img']->iteration++;
?>
						<a class="gallery <?php if (isset($_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua'])&&$_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua']==1){?> watermark<?php }?> " href="<?php echo $_smarty_tpl->tpl_vars['img']->value['src']['Original'];?>
" title="<?php echo $_smarty_tpl->tpl_vars['img']->value['caption'];?>
" ><img class="photo <?php if ($_smarty_tpl->tpl_vars['img']->iteration%2){?>left<?php }?> pull-left <?php if (isset($_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua'])&&$_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua']==1){?> watermark<?php }?>" src="<?php echo $_smarty_tpl->tpl_vars['img']->value['src']['Thumbnail'];?>
" /></a>
					<?php } ?>
				</div>
				<?php }?>
			</div>
		</div>
	</div>

<?php echo $_smarty_tpl->getSubTemplate ("./modal-add-to-cart.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['templateJS'][0][0]->templateJS(array('src'=>($_smarty_tpl->tpl_vars['ASSETS']->value)."/js/templates/colorbox.js"),$_smarty_tpl);?>

<?php }} ?>