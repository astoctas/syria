<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:29:30
         compiled from "application/_clients/default/templates/home.tpl" */ ?>
<?php /*%%SmartyHeaderCode:7289626255efa1efa378329-25609787%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '47f6ac0229d74433d8ab2f7888e6f86431b37529' => 
    array (
      0 => 'application/_clients/default/templates/home.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
    'be4cecbace832692471d1fc0c3e9250113539603' => 
    array (
      0 => '/var/www/html/astoctas/syria/application/_clients/default/templates/base.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '7289626255efa1efa378329-25609787',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5efa1efa4550c0_95022764',
  'variables' => 
  array (
    '_config' => 0,
    'pageTitle' => 0,
    'ASSETS' => 0,
    'VERSION' => 0,
    'URL' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5efa1efa4550c0_95022764')) {function content_5efa1efa4550c0_95022764($_smarty_tpl) {?><!DOCTYPE html>
<html>
  <head>
      
<!-- Google Tag Manager -->
<script rel="preconnect" defer>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-P3MDSPL');</script>
<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script rel="preconnect">
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '706364523260613'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=706364523260613&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

    <title><?php echo $_smarty_tpl->tpl_vars['_config']->value['name'];?>
 :: <?php echo $_smarty_tpl->tpl_vars['pageTitle']->value;?>
</title>
    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1,shrink-to-fit=no, user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="<?php echo $_smarty_tpl->tpl_vars['_config']->value['name'];?>
" />
    <meta property="og:url" content="http://<?php echo $_SERVER['SERVER_ADDR'];?>
<?php echo $_SERVER['REQUEST_URI'];?>
" />
    <meta property="og:image" content="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/img/og-thumb.png" />
    <link rel="shortcut icon" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/img/favicon.ico?v=<?php echo time();?>
" />
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    
        
            <!-- Bootstrap -->
            <link rel="preload" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/bootstrap/bootstrap.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" as="style" onload="this.onload=null;this.rel='stylesheet'" media="screen">
            <noscript><link href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/bootstrap/bootstrap.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" rel="stylesheet" media="screen"></noscript>
            <!--link href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/colorbox.css?5" rel="stylesheet" media="screen"-->
            <link rel="preload" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/swipebox/css/swipebox.min.css?5" as="style" onload="this.onload=null;this.rel='stylesheet'" media="screen">
            <noscript><link href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/swipebox/css/swipebox.min.css?5" rel="stylesheet" media="screen"></noscript>
             <link href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/theme_config.css?5" rel="stylesheet" media="screen">
            <link rel="preload" href="styles.css" as="style" onload="this.onload=null;this.rel='stylesheet'" type="text/css" media="all" >
            <noscript><link rel="stylesheet" type="text/css" media="all" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/hover-min.css"></noscript>
           

           <link rel="preload" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/owl-carousel2/assets/owl.carousel.css" as="style" onload="this.onload=null;this.rel='stylesheet'" media="all">
           <noscript><link rel="stylesheet" type="text/css" media="all" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/owl-carousel2/assets/owl.carousel.css"></noscript>
           <link rel="stylesheet" type="text/css" media="all" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/owl-carousel2/assets/owl.carousel.css">

           <link rel="preload" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/fancybox/dist/jquery.fancybox.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'" type="text/css" media="all" >
            <noscript><link rel="stylesheet" type="text/css" media="all" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/fancybox/dist/jquery.fancybox.min.css"></noscript>
           
            <link rel="preload" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/style.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" as="style" onload="this.onload=null;this.rel='stylesheet'" media="screen">
            <noscript><link rel="stylesheet" href="styles.css"><link href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/style.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" rel="stylesheet" media="screen"></noscript>
            <link rel="preload" href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/search.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" as="style" onload="this.onload=null;this.rel='stylesheet'" media="screen">
            <noscript><link href="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/search.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" rel="stylesheet" media="screen"></noscript>
            <link rel="preload" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['URL'][0][0]->URL(array(),$_smarty_tpl);?>
assets/backend/css/debug.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" as="style" onload="this.onload=null;this.rel='stylesheet'" media="screen">
            <noscript><link href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['URL'][0][0]->URL(array(),$_smarty_tpl);?>
assets/backend/css/debug.css?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" rel="stylesheet" media="screen"></noscript>
            
            <!-- FONTS -->
            <link rel="preload" as="style" href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" as="text/css'" onload="this.onload=null;this.rel='stylesheet'" media="screen">
            <noscript><link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'></noscript>
            <script src="https://use.fontawesome.com/add36a7374.js" async></script>            

        
            <!--script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/jquery-3.1.1.slim.min.js?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
"></script-->
            <script
              src="https://code.jquery.com/jquery-2.2.4.min.js"
              integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
              crossorigin="anonymous"></script>
            <script defer src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/utils.js?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" async></script>
        
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/bs/assets/js/html5shiv.js"></script>
    <![endif]-->
        
            <script>
                var _const = {
                    URL: '<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
',
                    ASSETS: '<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
'
                }
            </script>
        
            <script type="text/javascript">
                //window.$crisp=[];window.CRISP_WEBSITE_ID="826b2afc-ad5a-42b4-a634-93812affcbcf";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();
            </script>
        
        <meta name="google-site-verification" content="YCNp8vcyAKgaTIrXzREN__HqVns-9X727MiFxeaslrY" />
    </head>
    <body>
        
<!-- Google Tag Manager (noscript) -->
<noscript><iframe  rel="preconnect" src="https://www.googletagmanager.com/ns.html?id=GTM-P3MDSPL"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->        

        
            <?php echo (($tmp = @$_smarty_tpl->tpl_vars['REGION_6']->value)===null||$tmp==='' ? '' : $tmp);?>


        <div class="container">

            <header>
                <div class="header row">
                    <div class="col">
                        <?php echo (($tmp = @$_smarty_tpl->tpl_vars['REGION_1']->value)===null||$tmp==='' ? '' : $tmp);?>

                    </div>
                </div>
                <div class="clearfix">
                </div>
            </header>



            <div class="">
                        <?php echo (($tmp = @$_smarty_tpl->tpl_vars['REGION_2']->value)===null||$tmp==='' ? '' : $tmp);?>


                <div class="promos row">
                            <?php echo (($tmp = @$_smarty_tpl->tpl_vars['REGION_3']->value)===null||$tmp==='' ? '' : $tmp);?>

                            <?php echo (($tmp = @$_smarty_tpl->tpl_vars['REGION_4']->value)===null||$tmp==='' ? '' : $tmp);?>

                        <div class="clearfix"> </div>
                </div>

            </div>
            
            </div> <!-- container -->
            <?php echo (($tmp = @$_smarty_tpl->tpl_vars['REGION_7']->value)===null||$tmp==='' ? '' : $tmp);?>



            <div class="container-fluid">
            <footer>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="footer">
                            <div class="container main-container">
                                <div class="">
                                    <div class="logo"><img class="hvr-grow" src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/images/Syria_Logo_Header2.png?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" alt="Logo"/></div>
                                        <?php echo (($tmp = @$_smarty_tpl->tpl_vars['REGION_5']->value)===null||$tmp==='' ? '' : $tmp);?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            </div>


        
                        <!--script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/tether/tether.min.js?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
"></script>
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/bootstrap/bootstrap.min.js?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
"></script-->
            <script defer src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

        <script src="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['URL'][0][0]->URL(array(),$_smarty_tpl);?>
assets/backend/js/debug.js?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
"></script>
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/jquery.validate.min.js"> </script>
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/messages_es_AR.js"> </script>
            <!--script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/jquery.colorbox-min.js"> </script-->
            <script defer src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/jquery.watermark.min.js"> </script>
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/swipebox/js/jquery.swipebox.min.js"></script>
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/fancybox/dist/jquery.fancybox.min.js"></script>
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/plugins/owl-carousel2/owl.carousel.js"></script>
            <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
"></script>            
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/templates/cart.js"> </script>
            <script src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/js/jquery.sticky.js"> </script>
            <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['loadTemplateJS'][0][0]->loadTemplateJS(array(),$_smarty_tpl);?>

            
            <script type="text/javascript">
                $('#fixed-header').sticky();
                AOS.init();
            </script>

                <!-- Google Code for syria-contacto Conversion Page
                In your html page, add the snippet and call
                goog_report_conversion when someone clicks on the
                chosen link or button. -->
                <script type="text/javascript">
                  /* <![CDATA[ */
                  goog_snippet_vars = function() {
                    var w = window;
                    w.google_conversion_id = 970864604;
                    w.google_conversion_label = "QpoUCOLlgWoQ3O_4zgM";
                    w.google_remarketing_only = false;
                  }
                  // DO NOT CHANGE THE CODE BELOW.
                  goog_report_conversion = function(url) {
                    goog_snippet_vars();
                    window.google_conversion_format = "3";
                    var opt = new Object();
                    opt.onload_callback = function() {
                    if (typeof(url) != 'undefined') {
                      window.location = url;
                    }
                  }
                  var conv_handler = window['google_trackConversion'];
                  if (typeof(conv_handler) == 'function') {
                    conv_handler(opt);
                  }
                }
                /* ]]> */
                </script>
                <script type="text/javascript"
                  src="//www.googleadservices.com/pagead/conversion_async.js">
                </script>		
            
        
<script>
            var config = {
                "path": "<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/images/Syria_Watermark_Home2.png",
                "gravity": "sw",
                "opacity": 0
            };
            $(document).ready(function(){
                $('img.watermark').watermark(config);
            });
</script>

        
<a href="https://api.whatsapp.com/send?phone=5491162943825" target="_blank" class="js-statsd-wa-event-click btn-whatsapp  btn-floating fixed-bottom visible-when-content-ready">
<svg baseProfile="tiny" xmlns="http://www.w3.org/2000/svg" viewBox="300 -476.1 1792 1792"><path d="M1413 497.9c8.7 0 41.2 14.7 97.5 44s86.2 47 89.5 53c1.3 3.3 2 8.3 2 15 0 22-5.7 47.3-17 76-10.7 26-34.3 47.8-71 65.5s-70.7 26.5-102 26.5c-38 0-101.3-20.7-190-62-65.3-30-122-69.3-170-118s-97.3-110.3-148-185c-48-71.3-71.7-136-71-194v-8c2-60.7 26.7-113.3 74-158 16-14.7 33.3-22 52-22 4 0 10 .5 18 1.5s14.3 1.5 19 1.5c12.7 0 21.5 2.2 26.5 6.5s10.2 13.5 15.5 27.5c5.3 13.3 16.3 42.7 33 88s25 70.3 25 75c0 14-11.5 33.2-34.5 57.5s-34.5 39.8-34.5 46.5c0 4.7 1.7 9.7 5 15 22.7 48.7 56.7 94.3 102 137 37.3 35.3 87.7 69 151 101a44 44 0 0 0 22 7c10 0 28-16.2 54-48.5s43.3-48.5 52-48.5zm-203 530c84.7 0 165.8-16.7 243.5-50s144.5-78 200.5-134 100.7-122.8 134-200.5 50-158.8 50-243.5-16.7-165.8-50-243.5-78-144.5-134-200.5-122.8-100.7-200.5-134-158.8-50-243.5-50-165.8 16.7-243.5 50-144.5 78-200.5 134S665.3 78.7 632 156.4s-50 158.8-50 243.5a611 611 0 0 0 120 368l-79 233 242-77a615 615 0 0 0 345 104zm0-1382c102 0 199.5 20 292.5 60s173.2 93.7 240.5 161 121 147.5 161 240.5 60 190.5 60 292.5-20 199.5-60 292.5-93.7 173.2-161 240.5-147.5 121-240.5 161-190.5 60-292.5 60a742 742 0 0 1-365-94l-417 134 136-405a736 736 0 0 1-108-389c0-102 20-199.5 60-292.5s93.7-173.2 161-240.5 147.5-121 240.5-161 190.5-60 292.5-60z"></path></svg>                
</a>        
    </body>
</html>
<?php }} ?>