<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:29:29
         compiled from "application/_clients/default/templates/destacados.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17059427425efa1efa1bc3f5-34648704%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '8111cfea979668a750b9d3a70ceb355fd91e6f31' => 
    array (
      0 => 'application/_clients/default/templates/destacados.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17059427425efa1efa1bc3f5-34648704',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5efa1efa1ea738_18495114',
  'variables' => 
  array (
    'section' => 0,
    'contents' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5efa1efa1ea738_18495114')) {function content_5efa1efa1ea738_18495114($_smarty_tpl) {?>				<div class="col-md-6 col-sm-12" data-aos="fade-up">
					<div class="colheader img-rounded"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['section']->value['params']->title)===null||$tmp==='' ? '' : $tmp);?>
</div>
					<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
					<div class="destacado" >
						<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Imagen'])){?>
						<a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['Link'];?>
"><img class=" img-rounded img-fluid watermark" src="<?php echo $_smarty_tpl->tpl_vars['item']->value['Imagen'][0]['src']['Producto'];?>
" /></a>
						<?php }?>
						<div class="caption img-rounded">
							<div class="title"><a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['Link'];?>
"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Titulo'])===null||$tmp==='' ? '' : $tmp);?>
</a></div>
							<div class="desc"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Descripcion'])===null||$tmp==='' ? '' : $tmp);?>
</div>
							<div class="price"><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Precio'])===null||$tmp==='' ? '' : $tmp);?>
</div>
						</div>
					</div>
					<?php } ?>

				</div><?php }} ?>