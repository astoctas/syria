<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:30:14
         compiled from "application/_clients/default/templates/listado-filtros.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11616909055f04f776605e79-32226840%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a282be1a3cfb1d2c9b032a20486654e6672d990b' => 
    array (
      0 => 'application/_clients/default/templates/listado-filtros.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11616909055f04f776605e79-32226840',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page' => 0,
    'contents' => 0,
    'item' => 0,
    'URL' => 0,
    'ASSETS' => 0,
    'paginfo' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5f04f7767d7559_94655473',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5f04f7767d7559_94655473')) {function content_5f04f7767d7559_94655473($_smarty_tpl) {?>				<div class="row">
					<div class="col-xs-12 col-md-7 col-lg-6 th no-border f_left "><span class="bc1">Productos ::</span> <span class="bc2"><?php echo $_smarty_tpl->tpl_vars['page']->value['_data']['name'];?>
</span></div>
					<div class="col-xs-12 col-md-5 col-lg-6 f_right p_relative "><div class="leyenda b_10 r_0">* Todos nuestros productos cuentan con garantía de fábrica</div></div>
				</div>
				<div class="row">
				<div class="col-sm-12">
				<div class="filters">
					<form role="form" class="">
						  
						  <div class="form-group">
						    <label>Por tránsito</label>
						    <select class="form-control" name="transito">
							  <option value="0">Todos</option>
							  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value['transito']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item']->iteration++;
?>
							  	<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['uid'];?>
" <?php if (isset($_REQUEST['transito'])){?><?php if ($_REQUEST['transito']==$_smarty_tpl->tpl_vars['item']->value['uid']){?>selected="selected"<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</option>
							  <?php } ?>
							</select>
						  </div>

						  <div class="form-group">
						    <label>Por corte</label>
						    <select class="form-control" name="corte">
							  <option value="0">Todos</option>
							  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value['corte']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item']->iteration++;
?>
							  	<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['uid'];?>
" <?php if (isset($_REQUEST['corte'])){?><?php if ($_REQUEST['corte']==$_smarty_tpl->tpl_vars['item']->value['uid']){?>selected="selected"<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</option>
							  <?php } ?>
							</select>
						  </div>
						  
						  <div class="form-group">
						    <label>Por marca:</label>
						    <select class="form-control" name="marca">
							  <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value['marcas']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item']->iteration++;
?>
							  	<option value="<?php echo $_smarty_tpl->tpl_vars['item']->value['uid'];?>
" <?php if (isset($_REQUEST['marca'])){?><?php if ($_REQUEST['marca']==$_smarty_tpl->tpl_vars['item']->value['uid']){?>selected="selected"<?php }?><?php }?>><?php echo $_smarty_tpl->tpl_vars['item']->value['name'];?>
</option>
							  <?php } ?>						    
							</select>
						  </div>

						  <div class="form-group">
						    <label>Ordenar por</label>
						    <select class="form-control" name="orden">
							  <option value="asc" <?php if (isset($_REQUEST['orden'])){?><?php if ($_REQUEST['orden']=='asc'){?>selected="selected"<?php }?><?php }?>>Menor precio</option>
							  <option value="desc" <?php if (isset($_REQUEST['orden'])){?><?php if ($_REQUEST['orden']=='desc'){?>selected="selected"<?php }?><?php }?>>Mayor precio</option>
							</select>
						  </div>
						  <div class="form-group">
						  <div class="form-submit">
							<input type="submit"  class="btn btn-orange btn-lg " value="Filtrar"/>
						  </div>
						  </div>
						  <div class="clearfix"></div>
					</form>
				</div>
				</div>
				</div>
				<div class="listing row">

					<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
 $_smarty_tpl->tpl_vars['item']->iteration=0;
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['item']->iteration++;
?>
					<div class= " col-xs-12 col-md-6 "  data-aos="fade-up" >
					<div class="item <?php if ($_smarty_tpl->tpl_vars['item']->iteration%2){?>left<?php }?>">
						<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Imagenes'])){?>
						<a class="" href="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
/productos/<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
"><img class="photo <?php if (isset($_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua'])&&$_smarty_tpl->tpl_vars['item']->value['Marca_de_Agua']==1){?> watermark<?php }?>" src="<?php echo $_smarty_tpl->tpl_vars['item']->value['Imagenes'][0]['src']['Thumbnail'];?>
" /></a>
						<?php }else{ ?>
						<a class="" href="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
/productos/<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
"><img class="photo" src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/images/no-image.jpg" /></a>
						<?php }?>
						<div class="info ">
							<div class="title"><a class="" href="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
/productos/<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['name'];?>
</a></div>
							<strong>Tipo: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Tipo'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Marca: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Marca'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Tránsito: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Transito'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Corte: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Corte'][0]['name'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<strong>Medida: </strong><?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Medida'])===null||$tmp==='' ? "N/D" : $tmp);?>
<br/>
							<?php if ($_smarty_tpl->tpl_vars['item']->value['Precio_primera']>0){?>
							<strong>Precio 1ra calidad: </strong><br/><span class="price">$ <?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Precio_primera'])===null||$tmp==='' ? "N/D" : $tmp);?>
 /<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad'])&&$_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad']==1){?>unidad<?php }else{ ?>m<sup>2</sup><?php }?></span><br/>
							<?php }?>
							<?php if ($_smarty_tpl->tpl_vars['item']->value['Precio_segunda']>0){?>
							<strong>Precio 2da calidad: </strong><br/><span class="price">$ <?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Precio_segunda'])===null||$tmp==='' ? "N/D" : $tmp);?>
 /<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad'])&&$_smarty_tpl->tpl_vars['item']->value['Precio_por_Unidad']==1){?>unidad<?php }else{ ?>m<sup>2</sup><?php }?></span><br/>
							<?php }?>
							<div class="botones">
								<a class="btn btn-gray btn-cart" data-uid="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['uid'];?>
"><span class="glyphicon glyphicon-shopping-cart"></span> Cotizar</a>
								<a class="btn btn-orange btn-info" href="<?php echo $_smarty_tpl->tpl_vars['URL']->value;?>
/productos/<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
">+ info</a>
							</div>
						</div>
					</div>
					</div>
					<?php }
if (!$_smarty_tpl->tpl_vars['item']->_loop) {
?>
					 <div class="alert alert-warning"><h3>No se encuentran productos con estas especificaciones.</h3></div>
					<?php } ?>

				</div>
	            <div class="clearfix"></div>

				<?php $_smarty_tpl->tpl_vars['paginfo'] = new Smarty_variable($_smarty_tpl->tpl_vars['contents']->value['paginfo'], null, 0);?>
				<?php if ($_smarty_tpl->tpl_vars['paginfo']->value){?>
					<?php if ($_smarty_tpl->tpl_vars['paginfo']->value['pages']>1){?>
						<div class="paging pull-right">
							<nav aria-label="">
							<ul class="pagination">
							<?php if ($_smarty_tpl->tpl_vars['paginfo']->value['prev']){?> <li class="page-item"><a  class="page-link" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['QS'][0][0]->QS(array('key'=>"pag",'value'=>$_smarty_tpl->tpl_vars['paginfo']->value['prev']),$_smarty_tpl);?>
">&laquo;</a></li> <?php }?>
							 <?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['pag'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['pag']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['name'] = 'pag';
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['paginfo']->value['pages']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['loop'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['step'] = 1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['start'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['step'] > 0 ? 0 : $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['loop']-1;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['total'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['loop'];
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['pag']['total']);
?>
							  	<li class="<?php if ($_smarty_tpl->tpl_vars['paginfo']->value['pag']==$_smarty_tpl->getVariable('smarty')->value['section']['pag']['iteration']){?>active<?php }?> page-item"><a  class="page-link" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['QS'][0][0]->QS(array('key'=>"pag",'value'=>$_smarty_tpl->getVariable('smarty')->value['section']['pag']['iteration']),$_smarty_tpl);?>
"><?php echo $_smarty_tpl->getVariable('smarty')->value['section']['pag']['iteration'];?>
</a></li>
							  <?php endfor; endif; ?>
							<?php if ($_smarty_tpl->tpl_vars['paginfo']->value['next']){?> <li class="page-item"><a  class="page-link" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['QS'][0][0]->QS(array('key'=>"pag",'value'=>$_smarty_tpl->tpl_vars['paginfo']->value['next']),$_smarty_tpl);?>
">&raquo;</a></li> <?php }?>
							</ul>
							</nav>
						</div>
					<?php }?>
				<?php }?>

<?php echo $_smarty_tpl->getSubTemplate ("./modal-add-to-cart.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php }} ?>