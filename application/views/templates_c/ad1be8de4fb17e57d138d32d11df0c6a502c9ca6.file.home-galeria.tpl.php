<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:29:29
         compiled from "application/_clients/default/templates/home-galeria.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17019367795efa1efa19ac49-55994799%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ad1be8de4fb17e57d138d32d11df0c6a502c9ca6' => 
    array (
      0 => 'application/_clients/default/templates/home-galeria.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17019367795efa1efa19ac49-55994799',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5efa1efa1af739_17041501',
  'variables' => 
  array (
    'contents' => 0,
    'item' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5efa1efa1af739_17041501')) {function content_5efa1efa1af739_17041501($_smarty_tpl) {?>	<div class="galeria"  data-aos="fade-up">
		<div class="galeria-fotos">

			    <div id="" class="carousel slide">

			      <div class="owl-carousel owl-theme owl-carousel-galeria">
				<?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>
			        <div class="item ">
			          <a href="<?php echo $_smarty_tpl->tpl_vars['item']->value['Link'];?>
" target="_blank"><img class=" img-rounded img-fluid watermark " src="<?php echo $_smarty_tpl->tpl_vars['item']->value['Imagen'][0]['src']['Galeria'];?>
" alt=""></a>
			            <div class="carousel-caption"> </div>
			        </div>
				<?php } ?>

			      </div>

			 <!-- Controls -->
			  <a class="carousel-control-prev" href="#" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>

		    </div><!-- /.carousel -->


		</div>
	</div><!-- /.galeria -->
	<script type="text/javascript">
		$(document).ready(function(){
		  var owl = $(".owl-carousel-galeria").owlCarousel({
		  	items:1,
		  	dots: true,
		  	loop: true,
		  	mouseDrag: false
		  });
		  $('.carousel-control-prev').on('click', function(){
			owl.trigger('prev.owl.carousel');
		  })
		  $('.carousel-control-next').on('click', function(){
			owl.trigger('next.owl.carousel');
		  })
		});		
	</script>
<?php }} ?>