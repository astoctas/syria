<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:29:30
         compiled from "application/_clients/default/templates/header-menu.tpl" */ ?>
<?php /*%%SmartyHeaderCode:4665314345efa1efa2c3d03-28836150%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbcc19fa7e921051a1aaf7babc42bff69e1e26a1' => 
    array (
      0 => 'application/_clients/default/templates/header-menu.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '4665314345efa1efa2c3d03-28836150',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5efa1efa31fb36_29340743',
  'variables' => 
  array (
    'ASSETS' => 0,
    'VERSION' => 0,
    'contents' => 0,
    'item' => 0,
    'child' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5efa1efa31fb36_29340743')) {function content_5efa1efa31fb36_29340743($_smarty_tpl) {?><div class="row">
    <div class="logo col">
        <a href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['URL'][0][0]->URL(array(),$_smarty_tpl);?>
">
        <img src="<?php echo $_smarty_tpl->tpl_vars['ASSETS']->value;?>
/css/images/Syria_Logo_Header2.png?<?php echo $_smarty_tpl->tpl_vars['VERSION']->value;?>
" alt="Logo" class="hvr-grow"/> 
        </a>
    </div>    
    <div class="col">
        <nav class="navbar navbar-orange navbar-toggleable-md navbar-light" >
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="true" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
                    <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_smarty_tpl->tpl_vars['uid'] = new Smarty_Variable;
 $_from = $_smarty_tpl->tpl_vars['contents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
 $_smarty_tpl->tpl_vars['uid']->value = $_smarty_tpl->tpl_vars['item']->key;
?>
                        <li class="nav-item <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['act'][0][0]->act(array('s'=>$_smarty_tpl->tpl_vars['item']->value['_data']['slug']),$_smarty_tpl);?>
 <?php if ($_smarty_tpl->tpl_vars['item']->value['_children']){?>dropdown<?php }?>">
                            <a class="nav-link" href="<?php if ($_smarty_tpl->tpl_vars['item']->value['_children']){?>#<?php }else{ ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['URL'][0][0]->URL(array(),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
<?php }?>" <?php if ($_smarty_tpl->tpl_vars['item']->value['_children']){?> data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"<?php }?> id="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
"><?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['name'];?>
</a>
                         <?php if ($_smarty_tpl->tpl_vars['item']->value['_children']){?>
                            <div class="dropdown-menu list-unstyled"  aria-labelledby="<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
">
                                <?php  $_smarty_tpl->tpl_vars['child'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['child']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['item']->value['_children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['child']->key => $_smarty_tpl->tpl_vars['child']->value){
$_smarty_tpl->tpl_vars['child']->_loop = true;
?>
                                    <a  class="dropdown-item" href="<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['URL'][0][0]->URL(array(),$_smarty_tpl);?>
<?php echo $_smarty_tpl->tpl_vars['item']->value['_data']['slug'];?>
/<?php echo $_smarty_tpl->tpl_vars['child']->value['_data']['slug'];?>
"><?php echo $_smarty_tpl->tpl_vars['child']->value['_data']['name'];?>
</a>
                                <?php } ?>
                            </div>
                            <?php }?>
                        </li>
                    <?php } ?>
            </ul>

            <div class="sub-nav">
                <ul class="nav navbar-nav">
                </ul>
            </div>       

          </div>
        </nav>
    <div class="clearfix"></div>
    </div>
</div>
<?php }} ?>