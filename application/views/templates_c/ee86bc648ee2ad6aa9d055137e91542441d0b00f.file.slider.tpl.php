<?php /* Smarty version Smarty-3.1.8, created on 2020-07-07 22:29:30
         compiled from "application/_clients/default/templates/slider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:11129709275efa1efa273da3-06424753%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'ee86bc648ee2ad6aa9d055137e91542441d0b00f' => 
    array (
      0 => 'application/_clients/default/templates/slider.tpl',
      1 => 1594160678,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '11129709275efa1efa273da3-06424753',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.8',
  'unifunc' => 'content_5efa1efa2b37c2_32024384',
  'variables' => 
  array (
    'contents' => 0,
    'section' => 0,
    'item' => 0,
    'img' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5efa1efa2b37c2_32024384')) {function content_5efa1efa2b37c2_32024384($_smarty_tpl) {?><?php if ($_smarty_tpl->tpl_vars['contents']->value){?>
    <div class="container">
    <div class="row">
            <!--featured products-->
            <div class="section-marcas p_bottom_0 m_bottom_5 col-sm-12"  data-aos="fade-up">
                    <div class="d_table m_bottom_5 w_full animated " data-animation="fadeInDown">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 v_align_m d_table_cell f_none">
                            <h3 class=" m_top_0 color_dark tt_uppercase fw_light d_inline_m m_bottom_4">Nuestras Marcas</h3>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 t_align_r d_table_cell f_none">
                            <!--carousel navigation-->
                            <div class="clearfix d_inline_b buttons">
                                <button class="<?php echo $_smarty_tpl->tpl_vars['section']->value['uid'];?>
_prev black_hover button_type_4  state_2 tr_all d_block f_left vc_child m_right_5"> <i class="fa fa-chevron-left  d_inline_m"></i></button>
                                <button class="<?php echo $_smarty_tpl->tpl_vars['section']->value['uid'];?>
_next black_hover button_type_4  state_2 tr_all d_block f_left vc_child"><i class="fa fa-chevron-right  d_inline_m"></i></button>
                            </div>
                        </div>
                    </div>
                    <hr class="divider_bg m_bottom_15 animated hidden" data-animation="fadeInDown" data-animation-delay="100">
                    <div class="row">
                        <div class="owl-carousel owl-theme owl-carousel-marcas" data-nav="<?php echo $_smarty_tpl->tpl_vars['section']->value['uid'];?>
_">

                        <?php  $_smarty_tpl->tpl_vars['item'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['item']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['contents']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['item']->key => $_smarty_tpl->tpl_vars['item']->value){
$_smarty_tpl->tpl_vars['item']->_loop = true;
?>

                                <a href="<?php echo (($tmp = @$_smarty_tpl->tpl_vars['item']->value['Link'])===null||$tmp==='' ? 'javascript:;' : $tmp);?>
" class="">
                                	<?php if (isset($_smarty_tpl->tpl_vars['item']->value['Logo'][0])){?>
                                    <?php $_smarty_tpl->tpl_vars["img"] = new Smarty_variable($_smarty_tpl->tpl_vars['item']->value['Logo'][0], null, 0);?>
                                    <img class="marca" src="<?php echo $_smarty_tpl->tpl_vars['img']->value['src']['Original'];?>
" alt="" />
                                    <?php }?>
                                </a>

                        <?php } ?>

                        </div>
                    </div>
            </div>
    </div>
</div>
<script type="text/javascript">
        $(document).ready(function(){
          var owlMarcas = $(".owl-carousel-marcas").owlCarousel(
                            {
                                    "stagePadding" : 15,
                                    "margin" : 30,
                                    dots: false,
                                    loop: true,
                                    mouseDrag: false,
                                "responsive" : {
                                        "0" : {
                                            "items" : 6
                                        },
                                        "320" : {
                                            "items" : 2
                                        },
                                        "550" : {
                                            "items" : 2
                                        },
                                        "768" : {
                                            "items" : 4
                                        },
                                        "992" : {
                                            "items" : 6
                                        },
                                        "1200" : {
                                            "items" : 6
                                        }
                                    }
                                })
          $('.<?php echo $_smarty_tpl->tpl_vars['section']->value['uid'];?>
_prev').on('click', function(){
            owlMarcas.trigger('prev.owl.carousel');
          })
          $('.<?php echo $_smarty_tpl->tpl_vars['section']->value['uid'];?>
_next').on('click', function(){
            owlMarcas.trigger('next.owl.carousel');
          })
        });     
</script>
<?php }?><?php }} ?>