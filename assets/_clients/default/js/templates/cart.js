function refreshCart(data) {
	data = JSON.parse(data);
	$('#cart-items').html(data.items);
	jQuery.each(data.cart,function(k,v){
		e = $('.btn-cart[data-uid='+v+']');
		e.attr('disabled', 'disabled');
	})
}

$(function(){
	jQuery.ajax(_const.URL+'/ajax/get/cart/get',{
		success: refreshCart
	})
})

$('#cart-q').on('click', function(a){
	window.location.href =_const.URL + '/cotizar';
})

$('.btn-cart').on('click', function(a){
	uid = $(a.currentTarget).data('uid');
	jQuery.ajax(_const.URL+'/ajax/get/cart/add',{
		data: {uid: uid},
		success: function(data){
			refreshCart(data);
			$('#modal-add-to-cart').modal('show');			
		}
	})
})

$('.btn-cart-update').on('click', function(a){
	uid = $(a.currentTarget).data('uid');
	jQuery.ajax(_const.URL+'/ajax/get/cart/update',{
		data: {
			uid: uid,
			q: $('input[data-uid='+uid+']').val()
		},
		success: function(data){
			
		}
	})
})

$('.btn-cart-confirm').on('click', function(a){
	uid = $(a.currentTarget).data('uid');
	jQuery.ajax(_const.URL+'/ajax/get/cart/delete',{
		data: {
			uid: uid
		},
		success: function(data){
			window.location.reload();
		}
	})
})

// FORM CARRITO
var cartForm = $('#cart-send-form');
$("#cart-send-form").validate({
    submitHandler : function(form) {
        $.ajax(_const.URL+'/ajax/get/cart/send', {
            type : "POST",
            data : cartForm.serialize(),
            success : function(data) {
                $('#alert-success').removeClass('hidden-xs-up');
                $('#form-content').hide();
            },
            error : function() {
                $('#alert-failure').removeClass('hidden-xs-up');
            }
        });
    }
}); 


