
$(document).ready(function(){

            $('a.gallery').swipebox({
                afterMedia: function(index) {
                    var config = {
                        "path": _const.ASSETS+"/css/images/Syria_Watermark.png",
                        "gravity": "sw",
                        "opacity": 0
                    };
                    var $img = $.swipebox.extend().target[0];
                    if($($img).hasClass('watermark'))
                        $('.slide img').watermark(config);
                }
            })
})

            /*
            $('a.gallery').colorbox({
                rel: 'gal',
                current: '{current} / {total}',
                onComplete: function() {
                    var config = {
                        "path": _const.ASSETS+"/css/images/Syria_Watermark.png",
                        "position": "bottom-left",
                        "opacity": 0,
                        "className": "cboxPhoto img-fluid"
                    };
                    if($(this).hasClass('watermark'))
                        $(document).watermark(config);
                }
            });
            */

/*
$(document).bind('cbox_complete', function(){
            var config = {
                "path": _const.ASSETS+"/css/images/Syria_Watermark.png",
                "position": "bottom-left",
                "opacity": 0,
                "className": "cboxPhoto"
            };
            $(document).watermark(config);

});
*/