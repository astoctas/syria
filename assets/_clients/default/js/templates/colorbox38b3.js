			$('a.gallery').colorbox({
			    rel: 'gal',
			    current: '{current} / {total}',
			    onComplete: function() {
                    var config = {
                        "path": _const.ASSETS+"/css/images/Syria_Watermark.png",
                        "position": "bottom-left",
                        "opacity": 0,
                        "className": "cboxPhoto"
                    };
			        if($(this).hasClass('watermark'))
                        $(document).watermark(config);
			    }
			});

/*
$(document).bind('cbox_complete', function(){
            var config = {
                "path": _const.ASSETS+"/css/images/Syria_Watermark.png",
                "position": "bottom-left",
                "opacity": 0,
                "className": "cboxPhoto"
            };
            $(document).watermark(config);

});
*/