            $('.owl-carousel').each(function(){

                var _this = $(this),
                    options = _this.data('owl-carousel-options') ? _this.data('owl-carousel-options') : {},
                    buttons = _this.data('nav'),
                    config = $.extend(options,{
                        dragEndSpeed : 500,
                        smartSpeed : 500
                    });

                var owl = _this.owlCarousel(config);

                $('.' + buttons + 'prev').on('click',function(){
                    owl.trigger('prev.owl.carousel');
                });
                $('.' + buttons + 'next').on('click',function(){
                    owl.trigger('next.owl.carousel');
                });

            });