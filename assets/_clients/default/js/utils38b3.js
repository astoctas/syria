$(document).ready(function(){
  $(window).bind('scroll', function() {
    var navHeight = 50; // custom nav height
    ($(window).scrollTop() > navHeight) ? $('#fixed-header').addClass('fixed') : $('#fixed-header').removeClass('fixed');
  });
});