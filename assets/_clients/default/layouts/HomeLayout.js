Ext.define('CMS.client.layouts.HomeLayout',{
	extend: CMS.view.layout.Layout,
    items: [
		{
			xtype: 'vcontainer',
			items:[{
				xtype: 'region',
				itemId: '6'
			},	{
				xtype: 'region',
				itemId: '1'
			},	{
				xtype: 'region',
				itemId: '2'
			}, {
				xtype: 'hcontainer',
				items: [{
					xtype: 'region',
					itemId: '3'
				},{
					xtype: 'region',
					itemId: '4'
				}]
			}, {
				xtype: 'region',
				itemId: '7'
			}, {
				xtype: 'region',
				itemId: '5'
			}
			]
		}
	]

})