<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Cache Class
 *
 * @package	CodeIgniter
 * @subpackage	Libraries
 * @category	Cache
 *  
 */

class Cache  {
	
	private $_ci;
	private $_default_expires;

	function __construct()
	{

		$this->_ci =& get_instance();
		$this->_ci->load->config('cache');
		$this->_default_expires = $this->_ci->config->item('cache_expires');
		
		log_message('debug', "Cache Class Initialized");
	}

	public function query($query="",$values=array(),$key="", $expires = 0) {
		$this->_ci->load->driver('cache',array('adapter' => 'file'));
		if(!$res = $this->_ci->cache->get(md5($query.$key))) {
			$res =  $this->_ci->db->query($query, $values);
			if(!$res) return FALSE; 
			$expires = ($expires) ? $expires : $this->_default_expires;
			$this->_ci->cache->save(md5($query.$key),$res->result_array(),$expires);
			return $res->result_array();
		}
		return $res;
	}

}
// END Cache Class
